<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- Set latest rendering mode for IE -->
<%
	String path = request.getContextPath();
%>

<title>${title}</title>
<link rel="icon" type="png/ico"  href="<%=path %>/img/favicon.ico">
			
			 <!-- CSS -->
    <link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="all" href="<%=path %>/css/styles.css"> 
    <link rel="stylesheet" href="<%=path %>/css/slicknav_style.css">
    <link rel="stylesheet" href="<%=path %>/css/slicknav.css">
       <link href="<%=path %>/css/popup_style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<%=path %>/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/css/responsive_style.css" rel="stylesheet" type="text/css" />
    
    	<!-- Java Script -->
       <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<%=path %>/js/modernizr.js"></script>
        <script src="<%=path %>/js/bootstrap.min.js" type="text/javascript"></script>
  		<script src="<%=path %>/js/jquery.cycle.all.latest.js"></script>
       <script src="<%=path %>/js/funcations.js"></script>
		<script src="<%=path %>/js/jquery.slicknav.min.js"></script>
		 <script src="<%=path %>/js/popup_script.js" type="text/javascript"></script>
       <script type="text/javascript" src="<%=path %>/js/responsiveCarousel.min.js"></script>
       <script src="<%=path%>/js/spin.min.js"></script>
        <script src="<%=path%>/js/constants.js"></script>
		 <script src="<%=path%>/js/ajaxCaller.js"></script>
		<script src="<%=path%>/js/common.functions.js"></script>
	   
    
</head>
<body>

<tiles:insertAttribute name="header-content"></tiles:insertAttribute>
<tiles:insertAttribute name="body-content"></tiles:insertAttribute>
<tiles:insertAttribute name="footer-content"></tiles:insertAttribute>

 
</body>
</html>
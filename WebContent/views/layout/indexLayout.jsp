<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!-- Set latest rendering mode for IE -->
<%
	String path = request.getContextPath();
%>
<%
	response.setHeader("Cache-Control","max-age=100000");
%>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Gloomy | Home</title>
<link rel="icon" type="png/ico"  href="<%=path %>/img/favicon.ico">
 
  <!-- CSS -->
    
   		<!-- Java Script -->
        
     <link href="<%=path %>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/css/popup_style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<%=path %>/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<%=path %>/css/responsive_style.css" rel="stylesheet" type="text/css" />
  
     <link rel="stylesheet" type="text/css" media="all" href="<%=path %>/css/styles.css">
    <link rel="stylesheet" href="<%=path %>/css/slicknav_style.css">
    <link rel="stylesheet" href="<%=path %>/css/slicknav.css">
   		 <!-- Java Script -->
   		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
</head>

<body class="index">
<tiles:insertAttribute name="body-content"></tiles:insertAttribute>
<tiles:insertAttribute name="footer-content"></tiles:insertAttribute>
</body>

          <script src="<%=path %>/js/modernizr.js"></script>
        <script src="<%=path %>/js/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	    <script src="<%=path %>/js/bootstrap.min.js" type="text/javascript"></script>
  		<script src="<%=path %>/js/jquery.cycle.all.latest.js"></script>
        <script src="<%=path %>/js/popup_script.js" type="text/javascript"></script>
        <script src="<%=path%>/js/constants.js"></script>
        <script src="<%=path%>/js/spin.min.js"></script>
		<script src="<%=path%>/js/common.functions.js"></script>
		<script src="<%=path %>/js/index.js"></script>
		<script src="<%=path %>/js/ajaxCaller.js"></script>    
</html>
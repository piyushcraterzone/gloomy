<%
	String path = request.getContextPath();
%>
<script>
function subcsribe() {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var email = $("#subscribe-email").val();
	if(email==null || email=="" ||  !reg.test(email)) {
		$("#subscribe-email").css("background-color","red");
		return false;
	}
	data ={
			emailId:email
	};
	ajaxCallingGET(METHOD.POST,data,URL.SUBSCRIBE, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SUBSCRIBE);
	$("#subscribe-email").val("");
	$("#subscribe-email").css("background-color","white");
}
</script>
 <!-- Footer -->
    <footer>
    <div class="container">
        	
            <div class="col-lg-4  text-center col footer_desktop">
            	<ul style="border:none;">
                    <div class="footer_logo"><a href="dashboard"><img src="<%=path %>/images/fetaboo_logo.png" alt="Logo" /></a></div>
                    <p>2014 � Gloomy</p>
                    
                    <!-- Subscribe -->
                    <div class="subscribe">
                        <div class="subscribe_link">
                            <input name="" type="text" class="sub_email" id="subscribe-email" placeholder="Enter Email Id"><br>
                            <button class="subscribe_btn" onclick="subcsribe();">Subscribe</button>
                        </div>
                    </div>
                <!-- /Subscribe -->
                </ul>
            </div>
            
            <div class="col-lg-4">
                <ul>
                	<h1>Gloomy</h1>
                	<li><a href="aboutus.htm" id="about-us">About</a></li>
                    <li><a href="privacypolicy.htm" id="privacy">Privacy</a> and <a href="terms.htm" id="terms">terms</a></li>
                    <li><a href="howwork.htm" id="how-does-it-works">How dose it work</a></li>
                    <li><a href="faq.htm" id="faq">FAQ</a></li>
                </ul>
            </div>
            
            <div class="col-lg-4">
                <ul>
                	<h1>GET IN TOUCH</h1>
                	<li><a href="contact.htm" id="contact-us">Contact us</a></li>
                	<li><a style="cursor:pointer;" onclick="sharingOnFb('http://www.gloomy.com');">Facebook</a></li>
                    <li><a style="cursor:pointer;" onclick="sharingOnGooglePlus('http://www.gloomy.com');">Google+</a></li>
                    <li><a href="http://www.tumblr.com/share/link?url=www.gloomy.com&name=Express your desires and more one&description=Hii Tumbler" title="Share on Tumblr">Tumblr</a></li>
                </ul>
            </div>
            
            <div class="col-lg-4  text-center col footer_mobile">
            	<div class="footer_logo"><img src="<%=path %>/images/fetaboo_logo.png" alt="Logo" /></div>
                <p>2014 � Gloomy</p>
                
            </div>
            
    </div>
    </footer>
    <!-- /Footer -->
    
   
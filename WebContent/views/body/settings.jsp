 <%@ page language="java" contentType="text/html; charset=utf-8"%>

    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 <%
	String path = request.getContextPath();
%>
  <!-- Page Title -->
    <div class="content" style="margin-bottom:0px;">
        <div class="page_title">
            <div class="container">
                <div class="tri-down">
                     Settings
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title -->
  
	<!-- Content -->
	<div class="white_content_box">
    	<div class="container">
		<div class="inner_content setting">
        
        	<!-- Notification Setting Section -->
            <div class="row">
			<h3 style="margin-left:10px;">Notification Settings</h3>
            <form:form action="updatesetting" method="POST" commandName="settings">
            <form:input type="hidden" path="userId" value="${usermodel.userId }" />
            	<table width="100%" >
                	<tr>
                    	<td width="20%"></td>
                        <td width="25%" colspan="2"><h4 style="margin:0;">Email</h4></td>
                        <td width="25%" colspan="2"><h4 style="margin:0 auto; width:0px;">Mobile</h4></td>
                    </tr>
                    <tr>
                   	  	<td style="text-align:left;"><h4>New Messages and comments</h4></td>
                        <td><input type="radio" name="RadioGroup1" value="1" checked /> ON</td>
                        <td style="border-right:1px solid #fff;"><input type="radio" name="RadioGroup1" value="0" /> OFF</td>
                        <td style="text-align:right;"><form:input type="radio" name="RadioGroup2" path="notificationSettings.newComment" value="true" checked="true" /> ON</td>
                    	<td style="text-align:right;"><form:input type="radio" name="RadioGroup2" path="notificationSettings.newComment" value="false" /> OFF</td>
                    </tr>
                    <tr>
                   	  <td style="text-align:left;"><h4>Profile visitors</h4></td>
                        <td><input type="radio" name="RadioGroup3" value="1" checked /> ON</td>
                        <td style="border-right:1px solid #fff;"><input type="radio" name="RadioGroup3" value="0" /> OFF</td>
                        <td style="text-align:right;"><form:input type="radio" name="RadioGroup4" path="notificationSettings.pushNotification" value="true" checked="true"/> ON</td>
                        <td style="text-align:right;"><form:input type="radio" name="RadioGroup4" path="notificationSettings.pushNotification" value="false" /> OFF</td>
                    </tr>
                    <tr>
                   	  <td style="text-align:left;"><h4>Alert</h4></td>
                        <td><input type="radio" name="RadioGroup5" value="true" checked> ON</td>
                        <td style="border-right:1px solid #fff;"><input type="radio" name="RadioGroup5" value="false"> OFF</td>
                        <td style="text-align:right;"><form:input type="radio" name="RadioGroup6" path="notificationSettings.pushNotification" value="true" checked="true"/> ON</td>
                        <td style="text-align:right;"><form:input type="radio" name="RadioGroup6" path="notificationSettings.pushNotification" value="false" /> OFF</td>
                    </tr>
                    <tr>
                    	<td colspan="10" style="text-align:right;"><input name="" type="submit" value="Save" class="story_form_send" onclick="saveSettings();"></td>
                    </tr>
                </table>
                </form:form>
            </div>
            <!-- /Notification Setting Section -->
            
            
            <div style="width:100%; height:1px; background:#fff; margin:25px 0;"></div>
            
            
            <!-- Feedback Section -->
            <div class="row">
                <div class="col-lg-4"><h3>Support</h3><br>
                <h4>Feedback</h4>
                </div>
                
                    <div class="col-lg-8" style="margin-top:50px;">
                        <p><h4>Leave a message</h4></p><br>
                        <p>Choose your Subject</p>
                        <select class="feedback_select">
                            <option>Errors/Bugs</option>
                            <option>Idias</option>
                        </select>
                        
                        <br><br>
                        <p>Your Feedback</p>
                        <textarea name="" cols="" rows="" class="feedback_textarea"></textarea>
                        <input name="" type="submit" value="Send" class="story_form_send">
                    </div>
            
            <div class="clearfix"></div> 
        	</div>
            <!-- Feedback Section -->
            
            <div style="width:100%; height:1px; background:#fff; margin:25px 0;"></div>
			
            
            <!-- Recommended Section -->
			<div class="row">
            	<div class="col-lg-4"><h3>Recommended</h3></div>
            		
                    <div class="col-lg-8">
                    	<p>Download the free Gloomy desktop app, it's free and only takes a minute to install.<br><br></p>
                        	<div class="app_download">
                                <a href="#"><img src="<%=path %>/images/android_app_btn.png"></a>
                                <a href="#"><img src="<%=path %>/images/itune_app_btn.png"></a>
                                <a href="#"><img src="<%=path %>/images/window_app_btn.png"></a>
                            </div>
                    </div>
                    
            </div>
            <!-- /Recommended Section -->
            
            
            
           
         
		</div>
    </div>
	<!-- /Content -->
	<a href="#" class="back-to-top"></a>
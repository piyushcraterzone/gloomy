 <!-- Page Title -->
    <div class="content" style="margin-bottom:0px;">
        <div class="page_title">
            <div class="container">
                <div class="tri-down">
                     Frequently Asked Questions
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title -->

  
	<!-- Content -->
	<div class="container">
		<div class="inner_content">
			<p class="qus">Q1. What changes have you made to your Terms Of Use?</p>
			<p class="ans">As part of the introduction of our new Pro and Pro Unlimited accounts, we are making a few small changes to our Terms of Use. You can read the new Terms here, but to make things a bit easier, we have summarized the main changes below. Some of these changes.</p>
			
			<div class="red_divider"></div>
			
			<p class="qus">Q2. What is copyright?</p>
			<p class="ans">We've set up a page with more information on copyright here.</p>
			
			<div class="red_divider"></div>
			
			<p class="qus">Q3. What is copyright infringement and how can I avoid it?</p>
			<p class="ans">Copyright infringement occurs when someone uses a part of another person's copyrighted work without permission or licensing in place. If someone were to duplicate a copyrighted work, publish it online, adapt it, story it, or use it in any other way without rights holder permission, this would be considered copyright.</p>
			
			<div class="red_divider"></div>
			
			<p class="qus">Q4. Why should I officially copyright my own content?</p>
			<p class="ans">As a creator, copyright is automatically granted to you at the time your work is created. You don't have to file any paperwork or publish the content online to own the copyright of your story. It is an automatic right that you have as the content creator. However, it can.</p>
			
			<div class="red_divider"></div>
			
			<p class="qus">Q5. My original track was blocked, how do I get it back on my profile?</p>
			<p class="ans">If your gloomy is blocked, it means that our system has recognized something in your story that is under copyright protection. If the system has prevented your own story from being published on your profile, it's likely that your label or distributor submitted your sounds for fingerprinting in order to.</p>
			
			<div class="red_divider"></div>
			
			<p class="qus">Q6. Why was my story blocked for copyright by your automatic system? What are my options in this situation?</p>
			<p class="ans">gloomy is a community of sound creators, and for that community to thrive, everyone needs to respect copyright. For this reason, we use an automated content identification system to identify known copyrighted works. We do this at the time of upload and also on a regular basis thereafter. If your.</p>
			
			<div class="red_divider"></div>
			
			<p class="qus">Q7. Why is my DJ mix being blocked for copyright reasons?</p>
			<p class="ans">If you use parts of a track in a DJ set, please bear in mind that the original creator owns the copyright for that part in your track and, based on current copyright legislation, you will need their explicit permission to publish your mix on SoundCloud. Some producers and publishers.</p>
			
			<div class="red_divider"></div>
			
			<p class="qus">Q8. Why is my story being blocked for copyright?</p>
			<p class="ans">Since your remix is based on another artist's track, you'll need permission from the original artist and any other relevant rights holders before you can publish your remix online. If you do have permission, or if your remix was taken down in error, let us know here and we'll take.</p>
			
        </div>
	</div>
	<!-- /Content -->
<a href="#" class="back-to-top"></a>
<a href="submitstory.htm"  onclick="return isValidSession('${usermodel}');"><div class="circle"><img src="images/submit_fetaboo.png" /></div></a>
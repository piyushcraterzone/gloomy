<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<jsp:useBean id="dateValue" class="java.util.Date" />
  <%
	String path = request.getContextPath();
%>
 <script src="<%=path %>/js/jquery.sticky-kit.min.js"></script>
 <script>
  $(function() {
	  $("#sidebar , #effect-1").stick_in_parent();
  });
  </script>

<!-- Start Posts content -->
  <!-- Content -->
    <div class="container content">
    <div id="content-Spinner" style="float:left;display:none;margin-top:-15px;"></div>
    <!-- Story Box Area -->
    <!-- Left Private and Public Button -->
        <div class="mypost_btn" style="display:none;">
        	<ul>
        	<li ><a href="#" onclick="showPublicPost();" class="active" id="PUB">Public Story</a></li>
            	<li ><a href="#" onclick="showPrivatePost();" id="PRI">Private Story</a></li>
            </ul>
        <div class="clearfix"></div>
        </div>
        <!-- /Left Private and Public Button -->
        	<div class="col-lg-9 story_box effects clearfix" id="effect-1">
            
    			<c:forEach items="${stories}" var="datafeed" varStatus="loopCounter" >
			<c:set var="color" value="${(loopCounter.index%8)+1}" scope="session" />
			 <jsp:setProperty name="dateValue" property="time" value="${datafeed.postDate}" />
   		<c:set var="colorcode" value="color_${color}" scope="session" />
            	<c:choose>
            	 <c:when test="${loopCounter.index == 0}">
            	     <div class="col-lg-6 story_box_1 scroll img fetaured_post" id="${datafeed.postId }">
                	<div class="post_date" ><fmt:formatDate type="date" value="${dateValue}" pattern="dd MMM,yyyy" /></div>
                	  <div class="bottom_like">
                         <div class="ic_like"><a class="icon_like "  id="${datafeed.postId }-like" onclick="likeAndShare('${datafeed.postId }','${usermodel.userId}','like')" ></a><label id="like-${datafeed.postId }"><c:choose><c:when test="${datafeed.likes.size()!=null }">${datafeed.likes.size() }</c:when><c:otherwise>0</c:otherwise></c:choose></label></div>
                        <div class="ic_comment"><a class="icon_comment"></a><label id="comment-${datafeed.postId }">${datafeed.comments }</label></div>
                    <div class="clearfix"></div>
                    </div>
                    <!-- OverLay -->
                    <div class="tran"></div>
                    <div class="overlay">
                    <h4><a onclick="showStory('${datafeed.postId }','fetaured_post')">${datafeed.caption}</a></h4>
                    <p class="ellipsis" >${datafeed.postDetail}</p>
                    <a  onclick="showStory('${datafeed.postId }','fetaured_post')" class="read_btn">Read more</a>
                        <div class="overlay_social">
                           <a onclick="sharingOnFb('${datafeed.postURL}')"> <div class="facebook_like_story"></div></a>
                            <a href="http://www.tumblr.com/share/link?url=${datafeed.postURL}"><div class="twitter_like_story"></div></a>
                            <a href="https://plus.google.com/share?url=${datafeed.postURL}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" id="share_gplus" title="Share on Google+">
                            <div class="google_like_story"></div></a>
                        </div>
                    </div>
                    <!-- /OverLay -->
                </div>
            	 </c:when>
            	 
 				 <c:when test="${loopCounter.index > 0}">
   <!-- Story Box -->
                <div class="col-lg-3 img ${colorcode }" id="${datafeed.postId }">
                <img src="${datafeed.lresURL}" class="story_box_img">
                <div class="post_date"><fmt:formatDate type="date"  value="${dateValue}" pattern="dd MMM,yyyy" /></div>
                <div class="bottom_like">
                        <div class="ic_like"><a class="icon_like "  id="${datafeed.postId }-like"   onclick="likeAndShare('${datafeed.postId }','${usermodel.userId}','like')" ></a><label id="like-${datafeed.postId }"><c:choose><c:when test="${datafeed.likes.size()!=null }">${datafeed.likes.size() }</c:when><c:otherwise>0</c:otherwise></c:choose></label></div>
                        <div class="ic_comment"><a class="icon_comment"></a><label id="comment-${datafeed.postId }">${datafeed.comments }</label></div>
                         <div class="clearfix"></div>
                   
                 </div> 
                   <!-- OverLay -->
                    <div class="tran"></div>
                    <div class="overlay">
                    <h4><a onclick="showStory('${datafeed.postId }','${colorcode }')">${datafeed.caption}</a></h4>
                    <p class="ellipsis" >${datafeed.postDetail}</p>
                    <a onclick="showStory('${datafeed.postId }','${colorcode }')" class="read_btn">Read more</a>
                       <div class="overlay_social">
                           <a onclick="sharingOnFb('${datafeed.postURL}')"> <div class="facebook_like_story"></div></a>
                            <a href="http://www.tumblr.com/share/link?url=${datafeed.postURL}"><div class="twitter_like_story"></div></a>
                            <a href="https://plus.google.com/share?url=${datafeed.postURL}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" id="share_gplus" title="Share on Google+">
                            <div class="google_like_story"></div></a>
                        </div>
                    </div>
                    <!-- /OverLay -->
                </div>
                <!-- /Story Box -->
  				 </c:when></c:choose>
  				 </c:forEach>
  				 
            </div>
        <!-- /Story Box Area -->
       <div id="sidebar">
       
       
       <!-- My Post Tab --
        <div class="col-lg-3 popular_tag mypost_tab" style="display:block">
        	<div style="width:100%; text-align:center;">
        		<h1>My Post</h1>
            </div>
            <ul>
            	<li><a href="#">All</a></li>
            	<li><a href="#" class="active">Private</a></li>
                <li><a href="#">Public</a></li>
            </ul>
        </div>
        <!-- /My Post Tab -->
       
       
        <!-- Categories -->
         <div class="col-lg-3 categories" style="display:none;">
            <div style="width:100%; text-align:center;">
        	<h1>All Categories</h1>
        	   </div>
            <ul >
            	<li><a href="#FETABOO" onclick="postByCategory('FETABOO');" id="FETABOO" >Fetaboo<span class="categories_like" id="fetaboo-type"></span></a></li>
                <li><a href="#FETISH" onclick="postByCategory('FETISH');" id="FETISH">Fetish<span class="categories_like" id="fetish-type"></span></a></li>
                <li><a href="#TABOO" onclick="postByCategory('TABOO');" id="TABOO">Taboo<span class="categories_like" id="taboo-type"></span></a></li>
            </ul>
        </div>
     
        <!--/ Categories -->
        
          <!-- Popular Tags -->
        	<div class="col-lg-3 popular_tag" style="display:block">
        	<div style="width:100%; text-align:center;">
        		<h1>Popular Tags</h1>
            </div>
            <div id="tagSpinner" style="float:left;margin-top:10px;padding-bottom:15px;display:none"></div>
            <ul id="popularTag">
            	
            </ul>
        </div>
        <!-- /Popular Tags -->
        
        
        <!-- Sidebar Tabs -->
        <div class="col-lg-3 tabs_section" style="display:block">
		<div id="viewSpinner" style="float:left;margin-top:75px;padding-bottom:21px;display:none"></div>
		<ul class='tabs' >
			<li><a href='#tab1' onclick="getMost('view','tab-1')">Most<br>Read</a></li>
			<li><a href='#tab2' onclick="getMost('comment','tab-2')">Most<br>Commented</a></li>
			<!-- <li><a href='#tab3' onclick="getMost('share')" style="display:none;">Most<br>Shared</a></li> -->
		</ul>
		<div id='tab1'>
			<ul class="most" id="tab-1">
              
            </ul>
		</div>
		<div id='tab2'>
			<ul class="most" id="tab-2">
            	
             
            </ul>
		</div>
		<div id='tab3'>
			<ul class="most" id="tab-3">
            	
            </ul>
		</div>
</div>
        </div> 
     <div class="clearfix"></div>
    
     <!-- /Sidebar Tabs -->   
    <div class="clearfix"></div>
  				 <div id="postSpinner" style="float:left;height:37px;display:none"></div>
  				 
		<!-- Responsive Categories -->
        <div class="col-lg-3 categories responsive_cat" onClick="dropdownhide();" style="display:none;">
        	<h1>All Categories</h1>
            <ul>
            	<li><a href="#">Fetaboos<span class="categories_like">(2,345)</span></a></li>
                <li><a href="#">Fetish<span class="categories_like">(686)</span></a></li>
                <li><a href="#">Taboo<span class="categories_like">(180)</span></a></li>
                <li><a href="#">Latest<span class="categories_like">(76)</span></a></li>
                <li><a href="#">Reviws<span class="categories_like">(18)</span></a></li>
                <li><a href="#">More<span class="categories_like">(6)</span></a></li>
            </ul>
        </div>
        <!-- /Responsive Categories -->
   
   <div class="clearfix"></div> 
  
    </div>
    <!-- /Content -->
    
<a href="#" class="back-to-top"></a>

 <!-- Story Read Popup -->
 <div id="story_popup" style="display:none;">
 <div class="story_share_popup story_popup_2">
 <div class="story_popup_close" onClick="close_popup();"><img src="img/ic_close.png"/></div>
 
 
  <div class="story_popup_scroll">
 	<div class="story_popup">
    	
        <div class="article_area ">
        
        	<!-- Article Header -->
        	<div class="article_title">
            	<h1 id="post-caption"></h1>
                
                <!-- Share Post -->
                <div class="post_share">
                   <!-- User Menu Hover -->
                    <div class="user_menu" style="display:none;">
                    	<div class="user_option_2">
                        	<ul>
                                <li><a href="#" onClick="abuseFetaboo('${usermodel.userId}','user_report','reported');">Report User</a></li>
                                <li><a href="#" onClick="abuseFetaboo('${usermodel.userId}','user_block','blocked');">Block User</a></li>
                                <li><a href="#" onClick="abuseFetaboo('${usermodel.userId}','content_report','content');">Hide a Content</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- User Menu Hover -->
                	<div class="hover_post_share share_post_btn">
                	<!-- <div class="post_text" onClick="post_share();"></div> -->
                	<!-- <button class="share_post_btn" onClick="post_share();"></button> -->
                        <div class="post_icon" >
                        <ul>
                        	<li><a onclick="shareFb()"><img src="images/ic_facebook.png"/></a></li>
                            <li><a onclick="shareGplus()"><img src="images/ic_gp.png"/></a></li>
                            <li><a onclick="shareTumbler()"><img src="images/ic_tumbler.png"/></a></li>
                            </ul>
                        </div>
                    
                    <button class="share_post_btn_minus" onClick="post_share_icon();" style="display:none;"></button>
                    </div>
                     <div class="story_delete" style="display:none;" onclick="showDeletePopup();" title="Delete Story"></div>
                     <div  class="story_edit" style="display:none;" onclick="editPost();" title="Edoit Story"></div>
                    
                     
                 
                    
                <div class="clearfix"></div>
                </div>
               
                <div class="clearfix"></div>
                </div>
                <!-- /Share Post -->
                <!-- /Article Header -->
                
                
            <!-- Article -->
            <div class="article_user">
            	<img src="" class="art_user_img" id="user-avatar">
               <!--  by <span class="art_user_name" id="written-by"></span> --> <br>
				on <span id="post-date"></span>
            <div class="clearfix"></div>
            </div>
            
            <div class="art_content" id="post-detail">
            
            </div>
            
            <div class="art_tag_area" id="post-tag">
            	
            </div>
            
            <!-- /Article -->
            
            	<div class="story_bottom_like">
                        <div class="ic_like">
                            <div style="float:left;"><a class="ic_story_like " id="story-like" onclick="likeStory();"></a>&nbsp;</a><span id="post-like">21</span></div>
                        </div>
                        <div class="ic_comment">
                            <div style="float:left;"><a class="ic_story_comment"></a><span id="post-comment">21</span></div>
                            <div style="float:left;"><a class="ic_story_view isviewed"></a><span id="post-view">21</span></div>
                        </div>
                    <div class="clearfix"></div>
                 </div>
            
            </div>
            
            
            
            <!-- Comments -->
            <div class="art_comment">
                <h1>Comments</h1>
                    <ul id="post-comments">
                       
                       
                         </ul>
                         <div>
                         <ul>
                        <li class="comment_area" style="border-bottom:2px solid #fff;">
                            <div class="comment_text" style="width:100%;">
                            <form onsubmit="return postComment();">
                            <div style="width:100%;text-align:center;">
                            <div style="display:inline-block;">
                              <div id="comments-container" style="width:30px; height:23px; position:relative;margin-top:10px;display:none;"></div>
                              </div></div>
                                <input name="" type="text" class="comment_field" id="commentMessage" placeholder="Write a comment..."/>
                                </form>
                            <div class="clearfix"></div>
                            </div>
                        </li>
                        </ul>
                   </div>
                     
             </div>
             <!-- /Comments -->
             <div class="clearfix"></div>
             <!-- Similar Story -->
        <%--      <div class="art_similar">
             <h1>Similar stories</h1>
        
        		<div class="clearfix"></div>
         <div class="story_box effects story_similar" >
       			<!-- Story Box -->
                <div class="col-lg-3 img color_1">
                <img src="<%=path %>/images/img/img01.png" class="story_box_img">
                <div class="post_date" style="z-index:3;">28 April, 2013 <!--<div class="ic_type"><img src="images/ic_cam.png"></div>--></div>
                <div class="bottom_like" style="z-index:2;">
                        <div class="ic_like"><a class="icon_like"></a>21</div>
                        <div class="ic_comment"><a class="icon_comment"></a>28</div>
                    <div class="clearfix"></div>
                 </div>
                    <!-- OverLay -->
                    <div class="tran"></div>
                    <div class="overlay clearfix">
                    <h4><a href="#">The standard chunk of</a></h4>
                    <p>The standard chunk of The standard chunk ofThe standard chunk of</p>
                    <a href="story.html" class="read_btn">Read more</a>
                        <div class="overlay_social">
                            <div class="facebook_like_story"><img src="<%=path %>/images/img/fb_like.jpg"></div>
                            <div class="twitter_like_story"><img src="<%=path %>/images/img/tw_like.png"></div>
                            <div class="google_like_story"><img src="<%=path %>/images/img/g_like.png"></div>
                        </div>
                    </div>
                    <!-- /OverLay -->
                </div>
                <!-- /Story Box -->
                
                
                
                <!-- Story Box -->
                <div class="col-lg-3 img color_2" >
                <div class="post_date">28 April, 2013 <!--<div class="ic_type"><img src="images/ic_list.png"></div>--></div>
                <div class="bottom_like">
                        <div class="ic_like"><a class="icon_like"></a>21</div>
                        <div class="ic_comment"><a class="icon_comment"></a>28</div>
                    <div class="clearfix"></div>
                 </div>
                    <!-- OverLay -->
                    <div class="tran"></div>
                    <div class="overlay">
                    <h4><a href="#">The point of using</a></h4>
                    <p>Lorem Ipsum as their default model text, and a search for 'lorem ipsum' </p>
                    <a href="story.html" class="read_btn">Read more</a>
                        <div class="overlay_social">
                            <div class="facebook_like_story"><img src="<%=path %>/images/img/fb_like.jpg"></div>
                            <div class="twitter_like_story"><img src="<%=path %>/images/img/tw_like.png"></div>
                            <div class="google_like_story"><img src="<%=path %>/images/img/g_like.png"></div>
                        </div>
                    </div>
                    <!-- /OverLay -->
                </div>
                <!-- /Story Box -->
                
                
                <!-- Story Box -->
                <div class="col-lg-3 img color_3">
                <img src="<%=path %>/images/img/img02.png" class="story_box_img">
                <div class="post_date">28 April, 2013 <!--<div class="ic_type"><img src="images/ic_cam.png"></div>--></div>
                <div class="bottom_like">
                        <div class="ic_like"><a class="icon_like"></a>21</div>
                        <div class="ic_comment"><a class="icon_comment"></a>28</div>
                    <div class="clearfix"></div>
                 </div>
                    <!-- OverLay -->
                    <div class="tran"></div>
                    <div class="overlay">
                    <h4><a href="#">There are many variations</a></h4>
                    <p>The generated Lorem Ipsum is therefore always free from repetition, injected humour</p>
                    <a href="story.html" class="read_btn">Read more</a>
                        <div class="overlay_social">
                            <div class="facebook_like_story"><img src="<%=path %>/images/img/fb_like.jpg"></div>
                            <div class="twitter_like_story"><img src="<%=path %>/images/img/tw_like.png"></div>
                            <div class="google_like_story"><img src="<%=path %>/images/img/g_like.png"></div>
                        </div>
                    </div>
                    <!-- /OverLay -->
                </div>
                <!-- /Story Box -->
                
                
                <!-- Story Box -->
                <div class="col-lg-3 img color_4">
                <img src="<%=path %>/images/img/img03.png" class="story_box_img">
                <div class="post_date">28 April, 2013 <!--<div class="ic_type"><img src="images/ic_cam.png"></div>--></div>
                <div class="bottom_like">
                        <div class="ic_like"><a class="icon_like"></a>21</div>
                        <div class="ic_comment"><a class="icon_comment"></a>28</div>
                    <div class="clearfix"></div>
                 </div>
                    <!-- OverLay -->
                    <div class="tran"></div>
                    <div class="overlay">
                    <h4><a href="#">Neque porro quisquam est</a></h4>
                    <p>vel illum qui dolorem eum fugiat quo voluptas</p>
                    <a href="story.html" class="read_btn">Read more</a>
                        <div class="overlay_social">
                            <div class="facebook_like_story"><img src="<%=path %>/images/img/fb_like.jpg"></div>
                            <div class="twitter_like_story"><img src="<%=path %>/images/img/tw_like.png"></div>
                            <div class="google_like_story"><img src="<%=path %>/images/img/g_like.png"></div>
                        </div>
                    </div>
                    <!-- /OverLay -->
                </div>
                <!-- /Story Box -->
                
                </div>
              
            
        </div> --%>
          <!-- /Similar Story -->
        </div>
        </div>
        
        
    </div>
     <a href="#" class="back-to-top"></a> 
     
  <div style="background:rgba(0, 0, 0, 0.8); width:100%; height:100%; position:fixed; top:0; left:0; z-index:99;"></div>
  
 </div>
 <a href="submitstory.htm"  onclick="return isValidSession('${usermodel}');"><div class="circle"><img src="images/submit_fetaboo.png" /></div></a>
  <!-- Block User Popup -->
    <div id="user_block" class="story_share_popup" style="display:none;" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path %>/img/closebox.png"></a></div>
        	 <div >Block this User and his Posts!</div>
            <div class="title_popup" id="blocked"></div>
            
            <br>    		
            <p>Are you sure you want to block this user!</p><br>
            
            <a href="#" class="yes_btn" onclick="blockUser();">Yes</a>
            <a href="#" class="no_btn" onclick="share_popup_close();">No</a>
           <br><br><br>         
                   
    </div> 
    </div>
    <!-- /Delete Story Popup -->
    <div id="delete-story" class="story_share_popup" style="display:none;" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path %>/img/closebox.png"></a></div>
            <div class="title_popup" id="blocked">Delete Post</div>
            <br>    		
            <p>Are you sure you want to Delete this Story?</p><br>
            
            <a href="#" class="yes_btn" onclick="deletePost();">Yes</a>
            <a href="#" class="no_btn" onclick="share_popup_close();">No</a>
           <br><br><br>         
                   
    </div> 
    </div>
    <!-- /Delete story Popup -->
    
    
    <!-- Report User Popup -->
    <div id="user_report" class="story_share_popup" style="display:none;" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path %>/img/closebox.png"></a></div>
        	<div >Report User !</div>
            <div class="title_popup" id="reported"></div>
            <h4>Select a reason</h4>
            <table>
            	<tr>
                	<td width="20"><input name="option1" id="cause-1" type="radio" value="" checked></td>
                    <td class="cause-1">User has inappropriate content</td>
                </tr>
                <tr>
                	<td><input name="option1" id="cause-2" type="radio" value="" /></td>
                    <td class="cause-2">User is sending spam</td>
                </tr>
                <tr>
                	<td><input name="option1" type="radio" id="cause-3" value="" /></td>
                    <td class="cause-3">User is rude or abusive</td>
                </tr>
            </table>
            
            <h4>Add a comment</h4>
            <textarea name="" cols="" rows="" class="report_textarea"></textarea>
          <div class="title_popup" id="CZ_messageAfterprocessed"></div>   <div class="report_submit"><input name="" class="CZ_messageAfterprocessed" type="submit" value="Submit" onclick="reportUser();" /></div>
    
    <div class="clearfix"></div>  
    </div>    
    </div>
    <!-- /Report User Popup -->
    
    <!-- Report Content Popup -->
    <div id="content_report" class="story_share_popup" style="display:none;" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path %>/img/closebox.png"></a></div>
        	<div >Hide Content!</div>
            <div class="title_popup" id="content"></div>
            <h4>Select a reason</h4>
            <table>
            	<tr>
                	<td width="20"><input name="option2" type="radio" value="" checked></td>
                    <td>I'm in this post and I dont like it</td>
                </tr>
                <tr>
                	<td><input name="option2" type="radio" value=""></td>
                    <td>It's annoyig or not interesting</td>
                </tr>
                <tr>
                	<td><input name="option2" type="radio" value=""></td>
                    <td>It's spam</td>
                </tr>
            </table>
            
            <h4>Add a comment</h4>
            <textarea name="" cols="" rows="" class="report_textarea"></textarea>
           <div class="title_popup" id="CZ_messageAfterprocessedContentReport"></div> <div class="report_submit"><input name="" type="submit" class="CZ_messageAfterprocessedContentReport" value="Submit" id="CZ_contentReportButton" onclick="hidePost();"></div>
    
    <div class="clearfix"></div> 
    </div>     
    </div>
    <!-- /Report Content Popup -->  

 <script>
 //$("#effect-1").hide().fadeIn(1000);
 /* check for url comming for public post or any other */
 if('${url}' == "myPost.htm") {
	 currentURL = URL.MY_POST;
	 $(".mypost_btn").show();
 }
 $(function(){
	   var data = ${storiesJson};
	  $.each(data.postList,function(index,val){
		   stories[val.postId] = val;
		   postCount = val.postDate;
		  
		   if(isLoggedIn('${usermodel}')) {
			   if(val.likes!=null && val.likes.length > 0) {
				   for(var i=0;i<val.likes.length;i++) {
					   if(val.likes[i].userId=='${usermodel.userId}') {
						   $("#"+val.postId+"-like").toggleClass("isliked");
					   }
				   }
			   }
		   } 
	   });
	 
	   $(window).scroll(function() {
			  if($(window).scrollTop() >= ($(document).height() - $(window).height())*0.7) {
		    	  if(isNextCall && isRequestComplete){
		    		  isRequestComplete = false;
		        	  var target = document.getElementById("postSpinner");
		              var spinner = spinnerShow(target,opts);
		              if(currentURL== URL.PUBLIC_POST) {
		              ajaxCallingGET(METHOD.GET,'',currentURL+"?count="+postCount,WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DASHBOARD_DATA_PAYLOAD,target,spinner);
		              } else if(currentURL== URL.MY_POST) {
		            	  ajaxCallingGET(METHOD.GET,"",URL.MY_POST+"?userId="+'${usermodel.userId}'+"&&count="+postCount+"&&storytype="+storyType, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DASHBOARD_DATA_PAYLOAD,target,spinner);
		              } else if(currentURL== URL.SEARCH_CATEGORY) {
		            	  ajaxCallingGET(METHOD.GET,'',URL.SEARCH_CATEGORY+"?type="+defaultCategory+"&&count="+postCount,WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DASHBOARD_DATA_PAYLOAD,target,spinner);
		              } /* else if(currentURL== URL.POST_BY_TAG) {
		            	  ajaxCallingGET(METHOD.GET,'',URL.POST_BY_TAG+"?tag="+currentTag+"&&count="+count,WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DASHBOARD_DATA_PAYLOAD,target,spinner);
		              } */
 				}
		      }
		});
	   $(".art_comment").bind('scroll', function() {
		    if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
		    	  if(isNextCall && isRequestComplete) {
		    		  var target1 = document.getElementById("comments-container");
			    	  var spinner1 = spinnerShow(target1,opts); 
		    		  isRequestComplete = false;
		    		  ajaxCallingGET( METHOD.GET,'', URL.GET_COMMENT+"?postId="+currentPostId+"&&count="+commentCount, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.GET_COMMENT, target1, spinner1);
		    	  }
		      }
		  });
       $(window).keyup(function(event) {
           if(event.which === 27) {
        	   if(currentPopupId=="") {
                   $('#story_popup').fadeOut(100);
        	   } else {
        		   $(currentPopupId).fadeOut(300);
        		   currentPopupId = "";
        	   }
           }
   });
});
	   
   function onScrollPostDataPayload(data,target,spinner) {
	   spinnerHide(target,spinner);
		if(data!="" ) {
			  data = JSON.parse(data);
			  var html="";
			if(data.postList.length == 100) {
			isNextCall = true;
		} else {
			isNextCall = false;
		}
			$.each(data,function(index,val) {
				var color=1;
				$.each(val,function(index1,val1) {
					var isLiked="";
					postCount = val1.postDate;
					 stories[val1.postId] = val1;
					 if(isLoggedIn('${usermodel}')) {
						   if(val1.likes!=null && val1.likes.length > 0) {
							   for(var i=0;i<val1.likes.length;i++) {
								   if(val1.likes[i].userId=='${usermodel.userId}') {
									   $("#"+val1.postId+"-like").toggleClass("isliked");
								   }
							   }
						   }
					   }
					if(color==9) {
						color = 1;
					}
					var colorCode="color_"+color;
					color = color+1;
					var lres="";
					 var date=new Date(val1.postDate);
					 var likes = 0;
					 if(val1.likes != null) {
						 likes = val1.likes.length;
					 }
					 var ic_type="<%=path %>/images/ic_cam.png";
					if(val1.lresURL!=null && val1.lresURL!="") {
						lres=val1.lresURL;
						is_type="<%=path %>/images/ic_list.png";
					}
					html=html+'<div class="col-lg-3 img '+colorCode+' " id="'+val1.postId+'">'
              +'<img src="'+lres+'" class="story_box_img">'
                +'<div class="post_date" style="z-index:3;">'+date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990)
                +'</div>'
                +' <div class="bottom_like" style="z-index:2;">'
                +"<div class='ic_like'><a class='icon_like "+isLiked+"'id='"+val1.postId+"-like'  onclick=likeAndShare('"+val1.postId+"','${usermodel.userId}','like') ></a><label id=like-"+val1.postId+">"+likes+"</label></div>"
                +'<div class="ic_comment"><a class="icon_comment"></a><label id="comment-'+val1.postId+'">'+val1.comments+'</label></div>'
                +'<div class="clearfix"></div>'
                +'</div>'
               +'<div class="tran"></div>'
                +'<div class="overlay clearfix">'
                +'<h4><a onclick=showStory("'+val1.postId+'","'+colorCode+'")>'+val1.caption+'</a></h4>'
                +'<p class="ellipsis">'+val1.postDetail+'</p>'
                +'<a onclick=showStory("'+val1.postId+'","'+colorCode+'") class="read_btn">Read more</a>'
                +'<div class="overlay_social">'
                +"<a onclick=sharingOnFb('"+val1.podstURL+"')><div class='facebook_like_story'><img src='<%=path %>/images/img/fb_like.jpg'></div></a>"
                +'<a href="http://www.tumblr.com/share/link?url="'+val1.postURL+'"><div class="twitter_like_story"><img src="<%=path %>/images/ic_tm_check.png"></div></a>'
                +"<a onclick=sharingOnGooglePlus('"+val1.postURL+"')"
                +'<div class="google_like_story"></div></a>'
                +' </div>'
                +'</div>';
					
			});
			});
			if(html!="") {
				$("#effect-1").append(html);
				//$("#effect-1").hide().fadeIn(300);
				
			}		isRequestComplete = true;
			 //$(document.body).trigger("sticky_kit:recalc");
		} else {
			isNextCall = false;
		}
	}
  
	   
	   
  
  //ajaxCallingGET(METHOD.GET,'',URL.CATEGORY_COUNT,WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POST_CATEGORY_COUNT_DATA_PAYLOAD);
  function categoryCount(data) {
	  $("#fetaboo-type").html("("+data.fetabooCount+")");
		$("#fetish-type").html("("+data.fetishCount+")");
		$("#taboo-type").html("("+data.tabooCount+")");
		$(".fetaboo-typeM").html("("+data.fetabooCount+")");
		$(".fetish-typeM").html("("+data.fetishCount+")");
		$(".taboo-typeM").html("("+data.tabooCount+")");
		 
  }
  
  function likeAndShare(postId,userId,type) {
		  if(isValidSession('${usermodel}')) {
			  var data={"postId":postId,"userId":userId,"type":type};
			  if($("#"+postId+"-like").hasClass("isliked")) {
				  $("#like"+"-"+data.postId).html(parseInt($("#like"+"-"+data.postId).html())-1);
			  } else {
				  $("#like"+"-"+data.postId).html(parseInt($("#like"+"-"+data.postId).html())+1);
			  }
				  $("#post-like").html( $("#like"+"-"+data.postId).html());
			  $("#"+postId+"-like").toggleClass("isliked");
			  $("#story-like").toggleClass("storyliked");
				ajaxCallingGET(METHOD.POST,JSON.stringify(data),URL.LIKE_SHARE_POST, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.LIKE_SHARE);

				return true;		
				}
				return false;
		  }
	 var target2 = document.getElementById("tagSpinner");
  var spinner2 = spinnerShow(target2,opts);
  /* Getting popular Tags */
 ajaxCallingGET(METHOD.GET,'',URL.POPULAR_TAGS, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POPULAR_TAG,target2,spinner2);
  
  function popularTag(data,target,spinner) {
	  $("#popularTag").html("");
	  $(".popularTag-resp").html("");
	  data = JSON.parse(data);
	  $.each(data.tag,function(index,val) {
	  $("#popularTag").append('<li><a href="#'+val+'" id="'+val+'" onclick=postByTagName("'+val+'") style="text-decorn:none;">'+val+'</a></li>');
	  $(".popularTag-resp").append('<li><a onclick=postByTagName("'+val+'")>#'+val+'</a></li>');
	  });
	  $("#"+'${tagName}').addClass("active");
  }
  var target1 = document.getElementById("viewSpinner");
  var spinner1 = spinnerShow(target1,opts);
  /* Ajax call for MOst Viewed Posts */
  ajaxCallingGET(METHOD.GET,'',URL.MOST_VIEWED+"?count=0", WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_VIEWED,target1,spinner1);
  ajaxCallingGET(METHOD.GET,'',URL.MOST_COMMENTED+"?count=0", WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_COMMENTED);

  
  function mostAccessedPost(data,tab) {
	  data = JSON.parse(data);
	  $("#"+tab).html("");
	  $.each(data.postList,function(index,val) {
		  if(tab=="tab-1") {
			  mostRead[val.postId] = val;
			
		  } else if(tab == "tab-2") {
			  mostCommented[val.postId] = val;
			 
		  }
		  if(index <= 4) {
			stories[val.postId] = val;
			  var date = new Date(val.postDate);
			  var image = "<%=path%>/images/fetaboo_ico.png";
			  if(val.lresURL!="" && val.lresURL!=null) {
				  image = val.lresURL;
			  } else {
				 if(val.categoryType=="FETISH") {
					 image = "<%=path%>/images/fetish_ico.png";
				 } else if(val.categoryType=="TABOO") {
					 image = "<%=path%>/images/taboo_ico.png";
				 }
			  }
			  $("#"+tab).append('<li>'
					  	+'<div class="most_img"><img src="'+image+'"></div>'
				      	+'<div class="most_cont">'
				             +'<h1><a onclick=showStory("'+val.postId+'") style="cursor:pointer;">'+val.caption+'</a></h1>'
				              +'<div class="most_time">'+date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990)+'</div>'
				              +'<div class="most_comment">'+val.comments+'</div>'
				      	+'</div>'
				  	+'<div class="clearfix"></div>'
				  +'</li>')
		  } else {
			  return false;
		  }
		  
	  });
	  
  }
  function getMost(type,tab) {
	  $("#"+tab).html("");
	if(type=="view") {
		 
		  var spinner = spinnerShow(target1,opts);
		 ajaxCallingGET(METHOD.GET,'',URL.MOST_VIEWED+"?count=0", WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_VIEWED,target1,spinner);
	} else if(type=="comment") {
		  var spinner = spinnerShow(target1,opts);
		ajaxCallingGET(METHOD.GET,'',URL.MOST_COMMENTED+"?count=0", WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_COMMENTED,target1,spinner);
	} /* else if(type=="share") {
		  var spinner = spinnerShow(target1,opts);
		ajaxCallingGET(METHOD.GET,'',URL.MOST_SHARED+"?count=0", WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_SHARED,target1,spinner);
	} */
  }
  
  function story_popup() {
		$("#story_popup").show();	
	}
	function close_popup() {
		$("#story_popup").fadeOut(300);	
		$("#post-comments").html("");
		$(".article_area").removeClass(colorClass);
	}
	var colorClass = "fetaured_post";
	
	function showStory(postId,colorCode) {
		makingAvatar();
		$("#user-avatar").attr("src",currentAvatar);
		if(colorCode==undefined || colorCode=="") {
			$(".article_area").addClass("fetaured_post");
			colorClass = "fetaured_post";
		} else {
		$(".article_area").addClass(colorCode);
		colorClass = colorCode;
		}
		$("#story_popup").show();
		postURL = stories[postId].postURL;
		currentPostId = postId;
		/* Making View on story*/
		ajaxCallingGET(METHOD.GET,'',URL.VIEW+"?postId="+postId, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.VIEW);
		$(".story_edit").hide();
		$(".story_delete").hide();
		$(".user_menu").hide();
	$("#post-caption").html(stories[postId].caption);
	$("#written-by").html(stories[postId].userThumbInfo.userDisplayName);
	var date = new Date(stories[postId].postDate);
	$("#post-date").html(date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990));
	$("#post-detail").html(stories[postId].postDetail);
	//if(stories[postId].userThumbInfo.lresURL != null ) { 
		
	//} else {
		
//	}
	$("#post-tag").html("");
	$.each(stories[postId].tags,function(index,val){
		$("#post-tag").append('<a href="#'+val+'" onclick=postByTagName("'+val+'")><div class="art_tag">#'+val+'</div></a>');
	});
	$("#post-tag").append('<div class="clearfix"></div>'); 
	$("#post-like").html($("#like-"+currentPostId).html());
	if(isLoggedIn('${usermodel}')) {
		if(stories[currentPostId].userThumbInfo.userId == '${usermodel.userId}' ) {
			if(storyType=="PUB") {
			$(".story_edit").show();
			$(".story_delete").show();
			}
			if(stories[postId].likes!=null && stories[postId].likes.length > 0) {
				   for(var i=0;i<stories[postId].likes.length;i++) {
					   if(stories[postId].likes[i].userId=='${usermodel.userId}') {
						   $("#story-like").addClass("storyliked");
					   }
				   }
			   } 
		}else {
			$(".user_menu").show();
		}
	   }
	
	
	if($("#"+currentPostId+"-like").hasClass("isliked")) {
		$("#story-like").addClass("storyliked");
	} else {
		$("#story-like").removeClass("storyliked");
	}
	
	$("#post-comment").html(stories[postId].comments);
	$("#post-view").html(stories[postId].viewCounter);
	
	/* getting Comments on this post */
	
	  commentCount = 0;
	  $("#post-comments").html("");
	  $("#commentMessage").val("");
	  if(stories[postId].comments!=0) {
		  var target1 = document.getElementById("comments-container");
		  var spinner1 = spinnerShow(target1,opts); 
		getComments(commentCount,postId,target1,spinner1);
	  }
	}
	
	/* Calling web service and getting comment for this post id */
	
	function getComments(commentcount,postId,target,spinner) {
	  ajaxCallingGET( METHOD.GET,'', URL.GET_COMMENT+"?postId="+postId+"&&count="+commentcount, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.GET_COMMENT, target, spinner);
}
	function loadComment(data,target,spinner) {
		spinnerHide(target,spinner);
		if(data!="") {
			data =JSON.parse(data);
		}
		$.each(data,function(index,val) {
			if(data.commentList.length == 20) {
				isNextCall = true;
			} else {
				isNextCall = false;
			}
			$.each(val,function(index1,val1) {
				makingAvatar();
				commentCount = val1.commentDate;
			var delete_comment ="";
			if(isLoggedIn('${usermodel}') && '${usermodel.userId}'==val1.userThumbInfo.userId) {
				delete_comment = '<div class="comment_delete" id="delete-'+val1.commentDate+'"  onclick=deleteComment("'+val1.commentDate+'","'+currentPostId+'","'+val1.userThumbInfo.userId+'") title="delete"></div>';
			}
			
			$("#post-comments").append('<li id="'+val1.commentDate+'" class="comment_area">'
					+'<div class="comment_img"><img src="'+currentAvatar+'" alt=""></div>'
	                +'<div class="comment_text">'
	             /*    +'<h2>'+val1.userThumbInfo.userDisplayName+':</h2>' */+val1.message
	                +'<div class="comment_date">'+new Date(val1.commentDate).toDateString()+'</div>'
	                +'<div class="clearfix"></div>'
	              	+'</div><div class="clearfix"></div>'
	              	+delete_comment
	            	+'</li>');
		});
		});
		
		isRequestComplete = true;
	}
	  function shareFb() {
		  sharingOnFb(postURL);
	  }	
	  function shareGplus() {
		  sharingOnGooglePlus(postURL);
	  } 
	  function shareTumbler() {
		  sharingOnTumbler(postURL);
	  }
	  
	  function abuseFetaboo(userId,id,divId) {
		 
	 		if(!isValidSession('${usermodel}')) {
	 		return false;
	 		}
	 		var userDisplayName = stories[currentPostId].userThumbInfo.userDisplayName;
			 $("#"+divId).html(userDisplayName);
			$("#"+id).show();	
			currentPopupId = "#"+id; 
			$(".report_textarea").val("");
			$(".CZ_messageAfterprocessed").show();
			$(".CZ_messageAfterprocessedContentReport").show();
			$("#CZ_messageAfterprocessed").html("");
			$("#CZ_messageAfterprocessedContentReport").html("");
			reportingUser.abuseUserId=stories[currentPostId].userThumbInfo.userId;
			reportingUser.userId=userId;
					hide.userId=userId;
					hide.postId=currentPostId;
		}
	  
	  function reportUser() {
			var id=$("input[name=option1]:checked").attr('id');
			reportingUser.cause=$("."+id).html();
			if(reportingUser.abuseUserId!=reportingUser.userId) {
			ajaxCallingGET(METHOD.POST,JSON.stringify(reportingUser), URL.REPORT_USER, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.REPORT_USER);
			}
		}

		function blockUser() {
			if(reportingUser.abuseUserId!=reportingUser.userId) {
			ajaxCallingGET(METHOD.POST,JSON.stringify(reportingUser), URL.BLOCK_USER, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.BLOCK_USER);
			} 
		}
		function hidePost() {
			var id=$("input[name=option2]:checked").attr('id');
			var cause = $("."+id).html();
			ajaxCallingGET(METHOD.POST,JSON.stringify(hide), URL.HIDE_POST, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.HIDE_POST);
			
		}
		
		function postComment() {
			var message= $("#commentMessage").val().trim();
			if(!isValidSession('${usermodel}')) {
				return false;
			}
			if(message!= "") {
				 var target1 = document.getElementById("comments-container");
				  var spinner1 = spinnerShow(target1,opts); 
				var  data={"comment":message,"userId":'${usermodel.userId}',"postId":currentPostId};
				 ajaxCallingGET(METHOD.POST,JSON.stringify(data),URL.ADD_COMMENT, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.ADD_COMMENT,target1,spinner1);
			}
			return false;
			
		}
		
		function addComment(response,message,target,spinner) {
			if(response!="") {
				makingAvatar();
				response = JSON.parse(response);
				stories[currentPostId].comments  = stories[currentPostId].comments +1;
				$("#post-comment").html(stories[currentPostId].comments);
				$("#comment-"+currentPostId).html(stories[currentPostId].comments);
				var lres = "<%=path %>/images/avater_img.png";
				var delete_comment="";
				if(isLoggedIn('${usermodel}')) {
					delete_comment = '<div class="comment_delete" id="delete-'+response.commentDate+'" onclick=deleteComment("'+response.commentDate+'","'+currentPostId+'","${usermodel.userId}'+'") alt="delete"></div>';
				}
				
				$("#post-comments").append('<li id="'+response.commentDate+'"class="comment_area">'
						+'<div class="comment_img"><img src="'+currentAvatar+'" alt=""></div>'
		                +'<div class="comment_text">'
		                /* +'<h2>'+stories[currentPostId].userThumbInfo.userDisplayName*/+'</h2>'+message
		                +'<div class="comment_date">'+new Date(parseInt(response.commentDate)).toDateString()+'</div>'
		                +'<div class="clearfix"></div>'
		              	+'</div><div class="clearfix"></div>'
		            	+delete_comment
		            	+'</li>');
				$("#commentMessage").val("");
				}
		}
		function addView(viewCount) {
			stories[currentPostId].viewCounter = viewCount; 
			$("#post-view").html(stories[currentPostId].viewCounter);
		}
		function likeStory() {
			likeAndShare(currentPostId,'${usermodel.userId}',"like");
			
		}
		function deleteComment(commentId,postId,userId) {
			var data = {"postId":postId,"commentDate":commentId,"userId":userId};
			$("#delete-"+commentId).hide();
			ajaxCallingGET(METHOD.POST,JSON.stringify(data),URL.DELETE_COMMENT, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DELETE_COMMENT);
		}
		/* This function bind with My post Link */
		function getMyPost(userId) {
			$("#"+$(".active").attr("id")).removeClass("active");
			$("#"+userId).addClass("active");
			$("#effect-1").html("");
			 var target = document.getElementById("content-Spinner");
				var spinner = spinnerShow(target,contentSpinnerOpts);
				currentURL = URL.MY_POST;
				isRequestComplete = true;
				isNextCall = true;
			ajaxCallingGET(METHOD.GET,"",URL.MY_POST+"?userId="+userId+"&&count=0", WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MY_POST,target,spinner);
		}
		/* this function is Bind with Category wise post  */
		function postByCategory(category) {
			 $(".mypost_btn").hide();
			$("#"+$(".active").attr("id")).removeClass("active");
			$("#"+category).addClass("active");
			defaultCategory = category;
			 var target = document.getElementById("content-Spinner");
				var spinner = spinnerShow(target,contentSpinnerOpts);
				currentURL = URL.SEARCH_CATEGORY;
				isRequestComplete = true;
				isNextCall = true;
   	 	 ajaxCallingGET(METHOD.GET,'',URL.SEARCH_CATEGORY+"?type="+category+"&&count=0",WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SEARCH_CATEGORY,target,spinner);

		}
		/* this function is bind with post by tag name  */
		function postByTagName(tagName) {
			 $(".mypost_btn").hide();
			$("#story_popup").hide();
			$("#"+$(".active").attr("id")).removeClass("active");
			$("#"+tagName).addClass("active");
			var target = document.getElementById("content-Spinner");
			var spinner = spinnerShow(target,contentSpinnerOpts);
			currentURL = URL.POST_BY_TAG;
			isRequestComplete = true;
			isNextCall = true;
			currentTag = tagName;
			ajaxCallingGET(METHOD.GET,'',URL.POST_BY_TAG+"?tag="+tagName+"&&count=0",WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POST_BY_TAG,target,spinner);
		}
		
		function onRequestChangeDataReplicateOnDashBoard(data) {
			if(data!="" ) {
				var html = "";
				  data = JSON.parse(data);
				  stories = new Array();
				if(data.postList.length == 100) {
				isNextCall = true;
				
			} else {
				isNextCall = false;
			}
				$.each(data,function(index,val) {
					var color=1;
					
					$.each(val,function(index1,val1) {
						var isLiked="";
						postCount=val1.postDate;
						 stories[val1.postId] = val1;
						 if(isLoggedIn('${usermodel}')) {
							   if(val1.likes!=null && val1.likes.length > 0) {
								   for(var i=0;i<val1.likes.length;i++) {
									   if(val1.likes[i].userId=='${usermodel.userId}') {
										   isLiked ="isliked";
									   }
								   }
							   }
						   }
						if(color==9) {
							color = 1;
						}
						var colorCode="color_"+color;
						color = color+1;
						var lres="";
						 var date=new Date(val1.postDate);
						 var likes = 0;
						 if(val1.likes != null) {
							 likes = val1.likes.length;
						 }
						 var ic_type="<%=path %>/images/ic_cam.png";
						if(val1.lresURL!=null && val1.lresURL!="") {
							lres=val1.lresURL;
							is_type="<%=path %>/images/ic_list.png";
						}
						if(index1==0) {
							html=html+'<div class="col-lg-6 story_box_1 scroll img fetaured_post" id="'+val1.postId+'">'
						              +'<img src="'+lres+'" class="story_box_img">'
						                +'<div class="post_date" style="z-index:3;">'+date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990)
						                +'</div>'
						                +' <div class="bottom_like" style="z-index:2;">'
						                +"<div class='ic_like'><a class='icon_like "+isLiked+"' id='"+val1.postId+"-like' onclick=likeAndShare('"+val1.postId+"','${usermodel.userId}','like') ></a><label id=like-"+val1.postId+">"+likes+"</label></div>"
						                +'<div class="ic_comment"><a class="icon_comment"></a><label id="comment-'+val1.postId+'">'+val1.comments+'</label></div>'
						                +'<div class="clearfix"></div>'
						                +'</div>'
						               +'<div class="tran"></div>'
						                +'<div class="overlay clearfix">'
						                +'<h4><a onclick=showStory("'+val1.postId+'","fetaured_post")>'+val1.caption+'</a></h4>'
						                +'<p class="ellipsis">'+val1.postDetail+'</p>'
						                +'<a onclick=showStory("'+val1.postId+'","fetaured_post") class="read_btn">Read more</a>'
						                +'<div class="overlay_social">'
						                +"<a onclick=sharingOnFb('"+val1.podstURL+"')><div class='facebook_like_story'><img src='<%=path %>/images/img/fb_like.jpg'></div></a>"
						                +'<a href="http://www.tumblr.com/share/link?url="'+val1.postURL+'"><div class="twitter_like_story"><img src="<%=path %>/images/ic_tm_check.png"></div></a>'
						                +"<a onclick=sharingOnGooglePlus('"+val1.postURL+"')"
						                +'<div class="google_like_story"></div></a>'
						                +' </div>'
						                +'</div>';	
						} else {
							html=html+'<div class="col-lg-3 img '+colorCode+' " id="'+val1.postId+'">'
	              +'<img src="'+lres+'" class="story_box_img">'
	                +'<div class="post_date" style="z-index:3;">'+date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990)
	                +'</div>'
	                +' <div class="bottom_like" style="z-index:2;">'
	                +"<div class='ic_like'><a class='icon_like "+isLiked+"'id='"+val1.postId+"-like'  onclick=likeAndShare('"+val1.postId+"','${usermodel.userId}','like') ></a><label id=like-"+val1.postId+">"+likes+"</label></div>"
	                +'<div class="ic_comment"><a class="icon_comment"></a><label id="comment-'+val1.postId+'">'+val1.comments+'</label></div>'
	                +'<div class="clearfix"></div>'
	                +'</div>'
	               +'<div class="tran"></div>'
	                +'<div class="overlay clearfix">'
	                +'<h4><a onclick=showStory("'+val1.postId+'","'+colorCode+'")>'+val1.caption+'</a></h4>'
	                +'<p class="ellipsis">'+val1.postDetail+'</p>'
	                +'<a onclick=showStory("'+val1.postId+'","'+colorCode+'") class="read_btn">Read more</a>'
	                +'<div class="overlay_social">'
	                +"<a onclick=sharingOnFb('"+val1.podstURL+"')><div class='facebook_like_story'><img src='<%=path %>/images/img/fb_like.jpg'></div></a>"
	                +'<a href="http://www.tumblr.com/share/link?url="'+val1.postURL+'"><div class="twitter_like_story"><img src="<%=path %>/images/ic_tm_check.png"></div></a>'
	                +"<a onclick=sharingOnGooglePlus('"+val1.postURL+"')"
	                +'<div class="google_like_story"></div></a>'
	                +' </div>'
	                +'</div>';
				}
						
				});
				});
				if(html!="") {
					$("#effect-1").html(html);
					$("#effect-1").hide().fadeIn(300);
					
				}
				isRequestComplete = true;
				 $(document.body).trigger("sticky_kit:recalc");
			} else {
				isNextCall = false;
			}
		}
		
		function editPost() {
       //var data = {"jsonData":JSON.stringify(stories[currentPostId])};
       ajaxCallingGET(METHOD.POST,JSON.stringify(stories[currentPostId]),URL.UPDATE_POST , WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.UPDATE_POST);
		}
		function showDeletePopup() {
			$("#delete-story").show();
			currentPopupId = "#delete-story";
		}
		function deletePost() {
			var data = JSON.stringify({"postId":currentPostId,"userId":'${usermodel.userId}'});
			ajaxCallingGET(METHOD.POST,data,URL.DELETE_POST , WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DELETE_POST);
			currentPopupId = "";
		}
		function reportUserCallBack(data) {
			//$("#user_report").hide();
			$("#CZ_messageAfterprocessed").html("User reported successfully, Mail has been sent to User");
			$(".CZ_messageAfterprocessed").hide();
			//currentPopupId = "";
		}
		function blockUserCallBack(data) {
			$("#user_block").fadeOut(100);
			 $('#story_popup').fadeOut(100);
			currentPopupId = "";
			var blockUserId = reportingUser.abuseUserId;
			for(key in stories) {
				if(stories[key].userId == blockUserId) {
					$("#"+stories[key].postId).fadeOut(100);
					$("#"+stories[key].postId).remove(100);
				}
			}
			
		}
		function hidePostCallback(data) {
			//$("#content_report").hide();
			$("#"+hide.postId).fadeOut(200);
			  $('#story_popup').fadeOut(100);
			  $("#"+hide.postId).remove();
			delete stories[hide.postId];
			$("#CZ_messageAfterprocessedContentReport ").html("you will no longer see his posts");
			$(".CZ_messageAfterprocessedContentReport ").hide();
			//currentPopupId = "";
		}
		function share_popup_close() {
			if(currentPopupId=="") {
                $('#story_popup').fadeOut(100);
     	   } else {
     		   $(currentPopupId).fadeOut(300);
     		   currentPopupId = "";
     	   }
		}
		
		function showMostRead() {
			mostDataPayLoad(mostRead); 
		}
		function showMostCommented() {
			
			mostDataPayLoad(mostCommented);
			
		}
		function mostDataPayLoad(data) {
			var html = "";
			var color=1;
		for(var key in data) {
			var val1 = data[key];
			var isLiked="";
			var colorCode="color_"+color;
			var lres = "";
			
			if(val1.lresURL!=null && val1.lresURL!="") {
				lres=val1.lresURL;
			}
			 var likes = 0;
			 if(val1.likes != null) {
				 likes = val1.likes.length;
			 }
			 if(isLoggedIn('${usermodel}')) {
				   if(val1.likes!=null && val1.likes.length > 0) {
					   for(var i=0;i<val1.likes.length;i++) {
						   if(val1.likes[i].userId=='${usermodel.userId}') {
							   $("#"+val1.postId+"-like").toggleClass("isliked");
						   }
					   }
				   }
			   }
			 var date=new Date(val1.postDate);
			if(color==1) {
				html=html+'<div class="col-lg-6 story_box_1 scroll img fetaured_post" id="'+mostRead[key].postId+'">'
			              +'<img src="'+lres+'" class="story_box_img">'
			                +'<div class="post_date" style="z-index:3;">'+date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990)
			                +'</div>'
			                +' <div class="bottom_like" style="z-index:2;">'
			                +"<div class='ic_like'><a class='icon_like "+isLiked+"' id='"+val1.postId+"-like' onclick=likeAndShare('"+val1.postId+"','${usermodel.userId}','like') ></a><label id=like-"+val1.postId+">"+likes+"</label></div>"
			                +'<div class="ic_comment"><a class="icon_comment"></a><label id="comment-'+val1.postId+'">'+val1.comments+'</label></div>'
			                +'<div class="clearfix"></div>'
			                +'</div>'
			               +'<div class="tran"></div>'
			                +'<div class="overlay clearfix">'
			                +'<h4><a onclick=showStory("'+val1.postId+'","fetaured_post")>'+val1.caption+'</a></h4>'
			                +'<p class="ellipsis">'+val1.postDetail+'</p>'
			                +'<a onclick=showStory("'+val1.postId+'","fetaured_post") class="read_btn">Read more</a>'
			                +'<div class="overlay_social">'
			                +"<a onclick=sharingOnFb('"+val1.podstURL+"')><div class='facebook_like_story'><img src='<%=path %>/images/img/fb_like.jpg'></div></a>"
			                +'<a href="http://www.tumblr.com/share/link?url="'+val1.postURL+'"><div class="twitter_like_story"><img src="<%=path %>/images/ic_tm_check.png"></div></a>'
			                +"<a onclick=sharingOnGooglePlus('"+val1.postURL+"')"
			                +'<div class="google_like_story"></div></a>'
			                +' </div>'
			                +'</div>';	
			} else {
				html=html+'<div class="col-lg-3 img '+colorCode+' " id="'+val1.postId+'">'
      +'<img src="'+lres+'" class="story_box_img">'
        +'<div class="post_date" style="z-index:3;">'+date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990)
        +'</div>'
        +' <div class="bottom_like" style="z-index:2;">'
        +"<div class='ic_like'><a class='icon_like "+isLiked+"'id='"+val1.postId+"-like'  onclick=likeAndShare('"+val1.postId+"','${usermodel.userId}','like') ></a><label id=like-"+val1.postId+">"+likes+"</label></div>"
        +'<div class="ic_comment"><a class="icon_comment"></a><label id="comment-'+val1.postId+'">'+val1.comments+'</label></div>'
        +'<div class="clearfix"></div>'
        +'</div>'
       +'<div class="tran"></div>'
        +'<div class="overlay clearfix">'
        +'<h4><a onclick=showStory("'+val1.postId+'","'+colorCode+'")>'+val1.caption+'</a></h4>'
        +'<p class="ellipsis">'+val1.postDetail+'</p>'
        +'<a onclick=showStory("'+val1.postId+'","'+colorCode+'") class="read_btn">Read more</a>'
        +'<div class="overlay_social">'
        +"<a onclick=sharingOnFb('"+val1.podstURL+"')><div class='facebook_like_story'><img src='<%=path %>/images/img/fb_like.jpg'></div></a>"
        +'<a href="http://www.tumblr.com/share/link?url="'+val1.postURL+'"><div class="twitter_like_story"><img src="<%=path %>/images/ic_tm_check.png"></div></a>'
        +"<a onclick=sharingOnGooglePlus('"+val1.postURL+"')"
        +'<div class="google_like_story"></div></a>'
        +' </div>'
        +'</div>';
       
	}
			 color = color+1;
		}
		if(html!="") {
			$("#effect-1").html(html);
			$("#effect-1").hide().fadeIn(300);
			
		}
		isRequestComplete = false;
		}
		
		function showPrivatePost() {
			storyType = "PRI";
			postCount = 0;
			var target = document.getElementById("content-Spinner");
			var spinner = spinnerShow(target,contentSpinnerOpts);
			ajaxCallingGET(METHOD.GET,"",URL.MY_POST+"?userId="+'${usermodel.userId}'+"&&count="+postCount+"&&storytype="+storyType, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MY_POST,target,spinner);
			$("#"+$(".active").attr("id")).removeClass("active");
			$("#PRI").addClass("active");
		}
		function showPublicPost() {
			storyType = "PUB";
			postCount = 0;
			var target = document.getElementById("content-Spinner");
			var spinner = spinnerShow(target,contentSpinnerOpts);
			ajaxCallingGET(METHOD.GET,"",URL.MY_POST+"?userId="+'${usermodel.userId}'+"&&count="+postCount+"&&storytype="+storyType, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MY_POST,target,spinner);
			$("#"+$(".active").attr("id")).removeClass("active");
			$("#PUB").addClass("active");
		}
  </script>
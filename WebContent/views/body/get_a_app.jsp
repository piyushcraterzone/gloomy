<%
	String path = request.getContextPath();
%>

    <!-- Page Title -->
    <div class="content" style="margin-bottom:0px;">
        <div class="page_title">
            <div class="container">
                <div class="tri-down">
                     Get a App
                </div>
            </div>
        </div>
    </div>
    <!-- Page Title -->

	<!-- Content Section --> 
    <div class="content_grey">
    	<div class="container">
    	<div class="get_app_img"><img src="<%= path %>/images/get-app-img.png" alt="Image" /></div>
        
        <div class="get_app_content">
        	<img src="<%= path %>/images/fetaboo_logo.png" alt="Logo" /><br><br>
            <h3>Share with your friends, <br>Gloomy and much more...</h3>
            <div class="tringle"><img src="<%= path %>/images/tringle.png" /></div>
            <div class="download_cir_box">
            	<div class="download_cir" style="float:left;"><img src="<%= path %>/images/ic_itune.png"></div>
                <div class="download_cir" style="float:right;"><img src="<%= path %>/images/ic_window.png"></div>
                <div class="download_cir" style="display:inline-table;"><img src="<%= path %>/images/ic_android.png"></div>
            <div class="clearfix"></div>
            </div>
        </div>
        </div>
    <div class="clearfix"></div>
    </div>
    <!-- /Content Section --> 
    
     
    

	<div class="white_bar">
    	<div class="container">
        	<p>We will never post anything on Social Sites without your permission.
            <span style="background:#7d8081; border-radius:100%; color:#fff; font-size:12px; font-family:Georgia; padding:3px 7px;">i</span>
            </p>
        </div>
    </div>
<a href="#" class="back-to-top"></a>
<a href="submitstory.htm"  onclick="return isValidSession('${usermodel}');"><div class="circle"><img src="images/submit_fetaboo.png" /></div></a>
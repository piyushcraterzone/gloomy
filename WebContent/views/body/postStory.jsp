<%@ page language="java" contentType="text/html; charset=utf-8"%>

    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
%>
 <script src="<%=path %>/js/tumblershare.js"></script>
 <link href="<%=path %>/css/dd.css" rel="stylesheet" type="text/css" />
  <script src="<%=path %>/js/jquery.dd.min.js"></script>
  <link href="<%=path %>/css/editor.css" rel="stylesheet" type="text/css" />

  <link href="<%=path %>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<script src="<%=path %>/js/editor.js"></script>
  <style>
@font-face {
  font-family: 'FontAwesome';
  src: url('<%=path %>/fonts/fontawesome-webfont.eot?v=4.1.0');
  src: url('<%=path %>/fonts/fontawesome-webfont.eot?#iefix&v=4.1.0') format('embedded-opentype'), url('<%=path %>/fonts/fontawesome-webfont.woff?v=4.1.0') format('woff'), url('<%=path %>/fonts/fontawesome-webfont.ttf?v=4.1.0') format('truetype'), url('<%=path %>/fonts/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular') format('svg');
  font-weight: normal;
  font-style: normal;
}
</style>
<script>
var tagCount = 0;
var count=0;
var emailCount=0;
var countEmail = 0;
$(function() {
	 $(".txtEditor").Editor();  
	$("#sample").change(function(){
		var id = $(this).val();
		var path =$("."+id).attr('id');
		$('#previewHolder').attr('src',path );
		
	});
	$("#countries").msDropdown(); //image can have css class; Please check source code.
	$("#sample").msDropdown(); // this one already has data-usesprite in SELECT
	$("#visibilty").msDropdown(); // this one already has data-usesprite in SELECT

				$("#add-tag").click(function() {
									var text = $("#tag-text").val();
									if (tagCount < 3) {
										if (text != "" && text != null && text.length < 20) {
											id="tag-"+count;
											$("#tags").prepend('<div class="tag" id="'+id+'" ><span>'+ text + '</span><a style="cursor:pointer;" class="btn_tag_del" onclick=tagDelete("'+count+'")>X</a></div>').hide().fadeIn(500);
											$("#storypost").append('<input type="hidden" id="'+id+'-tag" value="'+text+'" name="tags['+tagCount+']" />');
											$("#tag-text").val("");
											tagCount = tagCount + 1;
											count=count+1;
										}
										else {
											errorMessage("Tag can not be blank or must contains maximum 20 characters");
										}
									}
								});
	
	/* Adding email address */
				$("#add-email").click(function() {
					 var regex=/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+.([A-Za-z]{2,3})(,\n*[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+.([A-Za-z]{2,3}))*,?$/;
					var text = $("#email-text").val();
						if (text != "" && text != null && regex.test(text)) {
							id="email-"+emailCount;
							$("#emails").prepend('<div class="tag" id="'+id+'" ><span>'+ text + '</span><a style="cursor:pointer;" class="btn_tag_del" onclick=emailDelete("'+emailCount+'")>X</a></div>').hide().fadeIn(500);
							$("#emailpost").append('<input type="hidden" id="'+id+'-email" value="'+text+'" name="emailId['+countEmail+']" />');
							$("#email-text").val("");
							emailCount = emailCount+1;
							countEmail = countEmail +1;
						}
						else {
							errorMessage("Email can not be blank or must be valid");
						}
				});
				$("#visibilty").change(function() {
					if($(this).val() == "PUBLIC"){
						$(".email-area").css("display","none");
					} else {
						$(".email-area").css("display","block");
					} 
					
				});
	
});
function shareViaEmail() {
	 var regex=/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+.([A-Za-z]{2,3})(,\n*[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+.([A-Za-z]{2,3}))*,?$/;
	var email = $("#emailIds").val();
	if(email!="" || email.length>0) {
		var emailId = email.split(",");
		var varifiedEmailId =new Array();
		var j=0;
		for(var i=0 ; i<emailId.length;i++) {
			if(regex.test(emailId[i])) {
				varifiedEmailId[j] = emailId[i];
				j=j+1;
			} 
			
		}
		var data = {"emailIds":emailId,"postURL": $("#private-url").val(),"postId": $("#postId").val(),"userId":'${usermodel.userId}'};
		ajaxCallingGET(METHOD.POST,JSON.stringify(data),URL.SHARE_PRIVATE_POST, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SHARE_PRIVATE_POST);
		$("#private-share").hide();	
	} else {
		errorMessage("Please enter email id for sharing the post");
	}
	}
	function postFormValidation() {
		if(!isValidSession('${usermodel}')) {
		  return false;
		  }
		var tag = 0;
		$("#tags .tag").each(function(index,val){
			tag=tag+1;
						
		});
		
		if($("#caption").val() == null || $("#caption").val()=="" || $("#caption").val().length < 6 || $("#caption").val().length >30 ) {
			errorMessage("Story Title must not blank and should have 6-30 characters");
			return false;
		}
		if($("#contentarea").html()== null || $("#contentarea").html()=="" || $("#contentarea").html().length <= 0) {
			errorMessage("Story must not blank");
			return false;
		}
		if(tag < 1) {
			errorMessage("A Story must have tags at least one tag");
			return false;
		}
		$("#postDetail").val($("#contentarea").html());
		return true;
	}

		
			

						
	function tagDelete(id) {
	$("div").remove("#tag-"+id);
	$("input").remove("#tag-"+id+"-tag");
	tagCount=tagCount-1;
	}
	function emailDelete(id) {
		$("div").remove("#email-"+id);
		$("input").remove("#email-"+id+"-email");
		emailCount = emailCount-1;
		countEmail = countEmail -1;
		}
	
	function validateInputBox(evt) {
		var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode ==32)
           return false;

        return true;
     }
	function validateEmailId(evt) {
		 var regex=/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+.([A-Za-z]{2,3})(,\n*[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+.([A-Za-z]{2,3}))*,?$/;
		    if($("#emailIds").val()!="" && regex.test($("#emailIds").val())) {
		    	$("#share-via-email").show();
		    } else {
		    	$("#share-via-email").hide();
		    }
     }
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "http://connect.facebook.net/en_US/sdk.js#xfbml=1&appId=263648493818981&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Page Title -->
<div class="content" style="margin-bottom: 0px;">
	<div class="page_title">
		<div class="container">
			<div class="tri-down">Share Your Story</div>
		</div>
	</div>
</div>
<!-- Page Title -->
<!-- Content Section -->
    <div class="content_grey">
    	<div class="container inner_content">
    	<p>What is Gloomy? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's 
standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen 
book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>


		<!-- Story Submit Form -->
		<div class="story_submit_form">
        
			<form:form id="submitPostform" commandName="postRequest" method="POST" enctype="multipart/form-data" action="submitpost.htm" onsubmit="return postFormValidation();">
			<form:input type="hidden" path="userId" value="${usermodel.userId }"/>
			<form:input type="hidden" path="postId" value="${postRequest.postId }"/>
				<div>
				<!-- Form Right -->
				<div class="form_left">
					                    
                    <div class="upload_img">
					<div class="story_img" id="upload_img">
                    	<!--<img src="images/img/img01.png" alt="" >-->
						<img id="previewHolder" src="<%=path %>/images/fetish_img.png" alt="" />
                        <!--<img id="previewHolder img2" src="<%=path %>/images/img/img03.png" alt="" style="display:none;" />
                        <img id="previewHolder img3" src="<%=path %>/images/img/img04.png" alt="" style="display:none;" />-->
					</div>
					
					<div class="image_upload_btn">
							<div class='upload_btn'>Upload image
							<input type="file" name="multipartFile" id="filePhoto" class="file_browse required borrowerImageFile" data-errormsg="PhotoUploadErrorMsg" />
							</div>
						</div>
                    </div>
                    
				</div>
				<!-- /Form Right -->
				<script>
				function readURL(input) {
  if (input.files && input.files[0]) {
	   var reader = new FileReader();
	   reader.onload = function(e) {
		   isDefaultImageUpload = true;
		   $('#previewHolder').attr('src', e.target.result);
	   }

	   reader.readAsDataURL(input.files[0]);
  }
}
$("#filePhoto").change(function() {
  readURL(this);
});
</script>
				<!-- Form Right -->
				<div class="form_right">
					<div class="select_col">
						Choose your category 
						<form:select name="sample" id="sample" data-usesprite="smallIcons" path="categoryType">
						 <option class="fetaboo FETABOO" id="<%=path %>/images/fetaboo.png" value="GLOOMY">Gloomy</option>
						 <%--  <option class="fetish FETISH" id="<%=path %>/images/fetish_img.png" value="FETISH">Fetish</option>
						  <option class="taboo TABOO" id="<%=path %>/images/taboo.png" value="TABOO">Taboo</option> --%>

						</form:select>				
						
					</div>

					<div class="select_col">
                    	Visibilty 
                    	<select name="postType" id="visibilty" data-usesprite="visibilty">
                              <option class="public" value="PUBLIC" id="public">Public</option>
                              <option class="private" value="PRIVATE" id="private" >Private</option>
						</select>
                    </div>
                    
					<div id="location" style="display:none;">
					<div class="select_col">
						Location 
							<select name="countries" id="countries" >
      <option value='ad' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ad" data-title="Andorra">Andorra</option>
      <option value='ae' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ae" data-title="United Arab Emirates">United Arab Emirates</option>
      <option value='af' data-image="<%=path%>/images/blank.gif" data-imagecss="flag af" data-title="Afghanistan">Afghanistan</option>
      <option value='ag' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ag" data-title="Antigua and Barbuda">Antigua and Barbuda</option>
      <option value='ai' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ai" data-title="Anguilla">Anguilla</option>
      <option value='al' data-image="<%=path%>/images/blank.gif" data-imagecss="flag al" data-title="Albania">Albania</option>
      <option value='am' data-image="<%=path%>/images/blank.gif" data-imagecss="flag am" data-title="Armenia">Armenia</option>
      <option value='an' data-image="<%=path%>/images/blank.gif" data-imagecss="flag an" data-title="Netherlands Antilles">Netherlands Antilles</option>
      <option value='ao' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ao" data-title="Angola">Angola</option>
      <option value='aq' data-image="<%=path%>/images/blank.gif" data-imagecss="flag aq" data-title="Antarctica">Antarctica</option>
      <option value='ar' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ar" data-title="Argentina">Argentina</option>
      <option value='as' data-image="<%=path%>/images/blank.gif" data-imagecss="flag as" data-title="American Samoa">American Samoa</option>
      <option value='at' data-image="<%=path%>/images/blank.gif" data-imagecss="flag at" data-title="Austria">Austria</option>
      <option value='au' data-image="<%=path%>/images/blank.gif" data-imagecss="flag au" data-title="Australia">Australia</option>
      <option value='aw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag aw" data-title="Aruba">Aruba</option>
      <option value='ax' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ax" data-title="Aland Islands">Aland Islands</option>
      <option value='az' data-image="<%=path%>/images/blank.gif" data-imagecss="flag az" data-title="Azerbaijan">Azerbaijan</option>
      <option value='ba' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ba" data-title="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
      <option value='bb' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bb" data-title="Barbados">Barbados</option>
      <option value='bd' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bd" data-title="Bangladesh">Bangladesh</option>
      <option value='be' data-image="<%=path%>/images/blank.gif" data-imagecss="flag be" data-title="Belgium">Belgium</option>
      <option value='bf' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bf" data-title="Burkina Faso">Burkina Faso</option>
      <option value='bg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bg" data-title="Bulgaria">Bulgaria</option>
      <option value='bh' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bh" data-title="Bahrain">Bahrain</option>
      <option value='bi' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bi" data-title="Burundi">Burundi</option>
      <option value='bj' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bj" data-title="Benin">Benin</option>
      <option value='bm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bm" data-title="Bermuda">Bermuda</option>
      <option value='bn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bn" data-title="Brunei Darussalam">Brunei Darussalam</option>
      <option value='bo' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bo" data-title="Bolivia">Bolivia</option>
      <option value='br' data-image="<%=path%>/images/blank.gif" data-imagecss="flag br" data-title="Brazil">Brazil</option>
      <option value='bs' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bs" data-title="Bahamas">Bahamas</option>
      <option value='bt' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bt" data-title="Bhutan">Bhutan</option>
      <option value='bv' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bv" data-title="Bouvet Island">Bouvet Island</option>
      <option value='bw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bw" data-title="Botswana">Botswana</option>
      <option value='by' data-image="<%=path%>/images/blank.gif" data-imagecss="flag by" data-title="Belarus">Belarus</option>
      <option value='bz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag bz" data-title="Belize">Belize</option>
      <option value='ca' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ca" data-title="Canada">Canada</option>
      <option value='cc' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cc" data-title="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
      <option value='cd' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cd" data-title="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
      <option value='cf' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cf" data-title="Central African Republic">Central African Republic</option>
      <option value='cg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cg" data-title="Congo">Congo</option>
      <option value='ch' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ch" data-title="Switzerland">Switzerland</option>
      <option value='ci' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ci" data-title="Cote D'Ivoire (Ivory Coast)">Cote D'Ivoire (Ivory Coast)</option>
      <option value='ck' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ck" data-title="Cook Islands">Cook Islands</option>
      <option value='cl' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cl" data-title="Chile">Chile</option>
      <option value='cm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cm" data-title="Cameroon">Cameroon</option>
      <option value='cn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cn" data-title="China">China</option>
      <option value='co' data-image="<%=path%>/images/blank.gif" data-imagecss="flag co" data-title="Colombia">Colombia</option>
      <option value='cr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cr" data-title="Costa Rica">Costa Rica</option>
      <option value='cs' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cs" data-title="Serbia and Montenegro">Serbia and Montenegro</option>
      <option value='cu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cu" data-title="Cuba">Cuba</option>
      <option value='cv' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cv" data-title="Cape Verde">Cape Verde</option>
      <option value='cx' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cx" data-title="Christmas Island">Christmas Island</option>
      <option value='cy' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cy" data-title="Cyprus">Cyprus</option>
      <option value='cz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag cz" data-title="Czech Republic">Czech Republic</option>
      <option value='de' data-image="<%=path%>/images/blank.gif" data-imagecss="flag de" data-title="Germany">Germany</option>
      <option value='dj' data-image="<%=path%>/images/blank.gif" data-imagecss="flag dj" data-title="Djibouti">Djibouti</option>
      <option value='dk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag dk" data-title="Denmark">Denmark</option>
      <option value='dm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag dm" data-title="Dominica">Dominica</option>
      <option value='do' data-image="<%=path%>/images/blank.gif" data-imagecss="flag do" data-title="Dominican Republic">Dominican Republic</option>
      <option value='dz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag dz" data-title="Algeria">Algeria</option>
      <option value='ec' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ec" data-title="Ecuador">Ecuador</option>
      <option value='ee' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ee" data-title="Estonia">Estonia</option>
      <option value='eg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag eg" data-title="Egypt">Egypt</option>
      <option value='eh' data-image="<%=path%>/images/blank.gif" data-imagecss="flag eh" data-title="Western Sahara">Western Sahara</option>
      <option value='er' data-image="<%=path%>/images/blank.gif" data-imagecss="flag er" data-title="Eritrea">Eritrea</option>
      <option value='es' data-image="<%=path%>/images/blank.gif" data-imagecss="flag es" data-title="Spain">Spain</option>
      <option value='et' data-image="<%=path%>/images/blank.gif" data-imagecss="flag et" data-title="Ethiopia">Ethiopia</option>
      <option value='fi' data-image="<%=path%>/images/blank.gif" data-imagecss="flag fi" data-title="Finland">Finland</option>
      <option value='fj' data-image="<%=path%>/images/blank.gif" data-imagecss="flag fj" data-title="Fiji">Fiji</option>
      <option value='fk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag fk" data-title="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
      <option value='fm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag fm" data-title="Federated States of Micronesia">Federated States of Micronesia</option>
      <option value='fo' data-image="<%=path%>/images/blank.gif" data-imagecss="flag fo" data-title="Faroe Islands">Faroe Islands</option>
      <option value='fr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag fr" data-title="France">France</option>
      <option value='fx' data-image="<%=path%>/images/blank.gif" data-imagecss="flag fx" data-title="France, Metropolitan">France, Metropolitan</option>
      <option value='ga' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ga" data-title="Gabon">Gabon</option>
      <option value='gb' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gb" data-title="Great Britain (UK)">Great Britain (UK)</option>
      <option value='gd' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gd" data-title="Grenada">Grenada</option>
      <option value='ge' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ge" data-title="Georgia">Georgia</option>
      <option value='gf' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gf" data-title="French Guiana">French Guiana</option>
      <option value='gh' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gh" data-title="Ghana">Ghana</option>
      <option value='gi' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gi" data-title="Gibraltar">Gibraltar</option>
      <option value='gl' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gl" data-title="Greenland">Greenland</option>
      <option value='gm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gm" data-title="Gambia">Gambia</option>
      <option value='gn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gn" data-title="Guinea">Guinea</option>
      <option value='gp' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gp" data-title="Guadeloupe">Guadeloupe</option>
      <option value='gq' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gq" data-title="Equatorial Guinea">Equatorial Guinea</option>
      <option value='gr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gr" data-title="Greece">Greece</option>
      <option value='gs' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gs" data-title="S. Georgia and S. Sandwich Islands">S. Georgia and S. Sandwich Islands</option>
      <option value='gt' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gt" data-title="Guatemala">Guatemala</option>
      <option value='gu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gu" data-title="Guam">Guam</option>
      <option value='gw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gw" data-title="Guinea-Bissau">Guinea-Bissau</option>
      <option value='gy' data-image="<%=path%>/images/blank.gif" data-imagecss="flag gy" data-title="Guyana">Guyana</option>
      <option value='hk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag hk" data-title="Hong Kong">Hong Kong</option>
      <option value='hm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag hm" data-title="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
      <option value='hn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag hn" data-title="Honduras">Honduras</option>
      <option value='hr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag hr" data-title="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
      <option value='ht' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ht" data-title="Haiti">Haiti</option>
      <option value='hu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag hu" data-title="Hungary">Hungary</option>
      <option value='id' data-image="<%=path%>/images/blank.gif" data-imagecss="flag id" data-title="Indonesia">Indonesia</option>
      <option value='ie' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ie" data-title="Ireland">Ireland</option>
      <option value='il' data-image="<%=path%>/images/blank.gif" data-imagecss="flag il" data-title="Israel">Israel</option>
      <option value='in' data-image="<%=path%>/images/blank.gif" data-imagecss="flag in" data-title="India" selected>India</option>
      <option value='io' data-image="<%=path%>/images/blank.gif" data-imagecss="flag io" data-title="British Indian Ocean Territory">British Indian Ocean Territory</option>
      <option value='iq' data-image="<%=path%>/images/blank.gif" data-imagecss="flag iq" data-title="Iraq">Iraq</option>
      <option value='ir' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ir" data-title="Iran">Iran</option>
      <option value='is' data-image="<%=path%>/images/blank.gif" data-imagecss="flag is" data-title="Iceland">Iceland</option>
      <option value='it' data-image="<%=path%>/images/blank.gif" data-imagecss="flag it" data-title="Italy">Italy</option>
      <option value='jm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag jm" data-title="Jamaica">Jamaica</option>
      <option value='jo' data-image="<%=path%>/images/blank.gif" data-imagecss="flag jo" data-title="Jordan">Jordan</option>
      <option value='jp' data-image="<%=path%>/images/blank.gif" data-imagecss="flag jp" data-title="Japan">Japan</option>
      <option value='ke' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ke" data-title="Kenya">Kenya</option>
      <option value='kg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag kg" data-title="Kyrgyzstan">Kyrgyzstan</option>
      <option value='kh' data-image="<%=path%>/images/blank.gif" data-imagecss="flag kh" data-title="Cambodia">Cambodia</option>
      <option value='ki' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ki" data-title="Kiribati">Kiribati</option>
      <option value='km' data-image="<%=path%>/images/blank.gif" data-imagecss="flag km" data-title="Comoros">Comoros</option>
      <option value='kn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag kn" data-title="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
      <option value='kp' data-image="<%=path%>/images/blank.gif" data-imagecss="flag kp" data-title="Korea (North)">Korea (North)</option>
      <option value='kr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag kr" data-title="Korea (South)">Korea (South)</option>
      <option value='kw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag kw" data-title="Kuwait">Kuwait</option>
      <option value='ky' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ky" data-title="Cayman Islands">Cayman Islands</option>
      <option value='kz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag kz" data-title="Kazakhstan">Kazakhstan</option>
      <option value='la' data-image="<%=path%>/images/blank.gif" data-imagecss="flag la" data-title="Laos">Laos</option>
      <option value='lb' data-image="<%=path%>/images/blank.gif" data-imagecss="flag lb" data-title="Lebanon">Lebanon</option>
      <option value='lc' data-image="<%=path%>/images/blank.gif" data-imagecss="flag lc" data-title="Saint Lucia">Saint Lucia</option>
      <option value='li' data-image="<%=path%>/images/blank.gif" data-imagecss="flag li" data-title="Liechtenstein">Liechtenstein</option>
      <option value='lk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag lk" data-title="Sri Lanka">Sri Lanka</option>
      <option value='lr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag lr" data-title="Liberia">Liberia</option>
      <option value='ls' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ls" data-title="Lesotho">Lesotho</option>
      <option value='lt' data-image="<%=path%>/images/blank.gif" data-imagecss="flag lt" data-title="Lithuania">Lithuania</option>
      <option value='lu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag lu" data-title="Luxembourg">Luxembourg</option>
      <option value='lv' data-image="<%=path%>/images/blank.gif" data-imagecss="flag lv" data-title="Latvia">Latvia</option>
      <option value='ly' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ly" data-title="Libya">Libya</option>
      <option value='ma' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ma" data-title="Morocco">Morocco</option>
      <option value='mc' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mc" data-title="Monaco">Monaco</option>
      <option value='md' data-image="<%=path%>/images/blank.gif" data-imagecss="flag md" data-title="Moldova">Moldova</option>
      <option value='mg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mg" data-title="Madagascar">Madagascar</option>
      <option value='mh' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mh" data-title="Marshall Islands">Marshall Islands</option>
      <option value='mk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mk" data-title="Macedonia">Macedonia</option>
      <option value='ml' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ml" data-title="Mali">Mali</option>
      <option value='mm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mm" data-title="Myanmar">Myanmar</option>
      <option value='mn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mn" data-title="Mongolia">Mongolia</option>
      <option value='mo' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mo" data-title="Macao">Macao</option>
      <option value='mp' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mp" data-title="Northern Mariana Islands">Northern Mariana Islands</option>
      <option value='mq' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mq" data-title="Martinique">Martinique</option>
      <option value='mr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mr" data-title="Mauritania">Mauritania</option>
      <option value='ms' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ms" data-title="Montserrat">Montserrat</option>
      <option value='mt' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mt" data-title="Malta">Malta</option>
      <option value='mu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mu" data-title="Mauritius">Mauritius</option>
      <option value='mv' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mv" data-title="Maldives">Maldives</option>
      <option value='mw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mw" data-title="Malawi">Malawi</option>
      <option value='mx' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mx" data-title="Mexico">Mexico</option>
      <option value='my' data-image="<%=path%>/images/blank.gif" data-imagecss="flag my" data-title="Malaysia">Malaysia</option>
      <option value='mz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag mz" data-title="Mozambique">Mozambique</option>
      <option value='na' data-image="<%=path%>/images/blank.gif" data-imagecss="flag na" data-title="Namibia">Namibia</option>
      <option value='nc' data-image="<%=path%>/images/blank.gif" data-imagecss="flag nc" data-title="New Caledonia">New Caledonia</option>
      <option value='ne' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ne" data-title="Niger">Niger</option>
      <option value='nf' data-image="<%=path%>/images/blank.gif" data-imagecss="flag nf" data-title="Norfolk Island">Norfolk Island</option>
      <option value='ng' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ng" data-title="Nigeria">Nigeria</option>
      <option value='ni' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ni" data-title="Nicaragua">Nicaragua</option>
      <option value='nl' data-image="<%=path%>/images/blank.gif" data-imagecss="flag nl" data-title="Netherlands">Netherlands</option>
      <option value='no' data-image="<%=path%>/images/blank.gif" data-imagecss="flag no" data-title="Norway">Norway</option>
      <option value='np' data-image="<%=path%>/images/blank.gif" data-imagecss="flag np" data-title="Nepal">Nepal</option>
      <option value='nr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag nr" data-title="Nauru">Nauru</option>
      <option value='nu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag nu" data-title="Niue">Niue</option>
      <option value='nz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag nz" data-title="New Zealand (Aotearoa)">New Zealand (Aotearoa)</option>
      <option value='om' data-image="<%=path%>/images/blank.gif" data-imagecss="flag om" data-title="Oman">Oman</option>
      <option value='pa' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pa" data-title="Panama">Panama</option>
      <option value='pe' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pe" data-title="Peru">Peru</option>
      <option value='pf' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pf" data-title="French Polynesia">French Polynesia</option>
      <option value='pg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pg" data-title="Papua New Guinea">Papua New Guinea</option>
      <option value='ph' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ph" data-title="Philippines">Philippines</option>
      <option value='pk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pk" data-title="Pakistan">Pakistan</option>
      <option value='pl' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pl" data-title="Poland">Poland</option>
      <option value='pm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pm" data-title="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
      <option value='pn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pn" data-title="Pitcairn">Pitcairn</option>
      <option value='pr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pr" data-title="Puerto Rico">Puerto Rico</option>
      <option value='ps' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ps" data-title="Palestinian Territory">Palestinian Territory</option>
      <option value='pt' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pt" data-title="Portugal">Portugal</option>
      <option value='pw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag pw" data-title="Palau">Palau</option>
      <option value='py' data-image="<%=path%>/images/blank.gif" data-imagecss="flag py" data-title="Paraguay">Paraguay</option>
      <option value='qa' data-image="<%=path%>/images/blank.gif" data-imagecss="flag qa" data-title="Qatar">Qatar</option>
      <option value='re' data-image="<%=path%>/images/blank.gif" data-imagecss="flag re" data-title="Reunion">Reunion</option>
      <option value='ro' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ro" data-title="Romania">Romania</option>
      <option value='ru' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ru" data-title="Russian Federation">Russian Federation</option>
      <option value='rw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag rw" data-title="Rwanda">Rwanda</option>
      <option value='sa' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sa" data-title="Saudi Arabia">Saudi Arabia</option>
      <option value='sb' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sb" data-title="Solomon Islands">Solomon Islands</option>
      <option value='sc' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sc" data-title="Seychelles">Seychelles</option>
      <option value='sd' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sd" data-title="Sudan">Sudan</option>
      <option value='se' data-image="<%=path%>/images/blank.gif" data-imagecss="flag se" data-title="Sweden">Sweden</option>
      <option value='sg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sg" data-title="Singapore">Singapore</option>
      <option value='sh' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sh" data-title="Saint Helena">Saint Helena</option>
      <option value='si' data-image="<%=path%>/images/blank.gif" data-imagecss="flag si" data-title="Slovenia">Slovenia</option>
      <option value='sj' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sj" data-title="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
      <option value='sk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sk" data-title="Slovakia">Slovakia</option>
      <option value='sl' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sl" data-title="Sierra Leone">Sierra Leone</option>
      <option value='sm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sm" data-title="San Marino">San Marino</option>
      <option value='sn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sn" data-title="Senegal">Senegal</option>
      <option value='so' data-image="<%=path%>/images/blank.gif" data-imagecss="flag so" data-title="Somalia">Somalia</option>
      <option value='sr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sr" data-title="Suriname">Suriname</option>
      <option value='st' data-image="<%=path%>/images/blank.gif" data-imagecss="flag st" data-title="Sao Tome and Principe">Sao Tome and Principe</option>
      <option value='su' data-image="<%=path%>/images/blank.gif" data-imagecss="flag su" data-title="USSR (former)">USSR (former)</option>
      <option value='sv' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sv" data-title="El Salvador">El Salvador</option>
      <option value='sy' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sy" data-title="Syria">Syria</option>
      <option value='sz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag sz" data-title="Swaziland">Swaziland</option>
      <option value='tc' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tc" data-title="Turks and Caicos Islands">Turks and Caicos Islands</option>
      <option value='td' data-image="<%=path%>/images/blank.gif" data-imagecss="flag td" data-title="Chad">Chad</option>
      <option value='tf' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tf" data-title="French Southern Territories">French Southern Territories</option>
      <option value='tg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tg" data-title="Togo">Togo</option>
      <option value='th' data-image="<%=path%>/images/blank.gif" data-imagecss="flag th" data-title="Thailand">Thailand</option>
      <option value='tj' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tj" data-title="Tajikistan">Tajikistan</option>
      <option value='tk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tk" data-title="Tokelau">Tokelau</option>
      <option value='tl' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tl" data-title="Timor-Leste">Timor-Leste</option>
      <option value='tm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tm" data-title="Turkmenistan">Turkmenistan</option>
      <option value='tn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tn" data-title="Tunisia">Tunisia</option>
      <option value='to' data-image="<%=path%>/images/blank.gif" data-imagecss="flag to" data-title="Tonga">Tonga</option>
      <option value='tp' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tp" data-title="East Timor">East Timor</option>
      <option value='tr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tr" data-title="Turkey">Turkey</option>
      <option value='tt' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tt" data-title="Trinidad and Tobago">Trinidad and Tobago</option>
      <option value='tv' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tv" data-title="Tuvalu">Tuvalu</option>
      <option value='tw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tw" data-title="Taiwan">Taiwan</option>
      <option value='tz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag tz" data-title="Tanzania">Tanzania</option>
      <option value='ua' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ua" data-title="Ukraine">Ukraine</option>
      <option value='ug' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ug" data-title="Uganda">Uganda</option>
      <option value='uk' data-image="<%=path%>/images/blank.gif" data-imagecss="flag uk" data-title="United Kingdom">United Kingdom</option>
      <option value='um' data-image="<%=path%>/images/blank.gif" data-imagecss="flag um" data-title="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
      <option value='us' data-image="<%=path%>/images/blank.gif" data-imagecss="flag us" data-title="United States">United States</option>
      <option value='uy' data-image="<%=path%>/images/blank.gif" data-imagecss="flag uy" data-title="Uruguay">Uruguay</option>
      <option value='uz' data-image="<%=path%>/images/blank.gif" data-imagecss="flag uz" data-title="Uzbekistan">Uzbekistan</option>
      <option value='va' data-image="<%=path%>/images/blank.gif" data-imagecss="flag va" data-title="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
      <option value='vc' data-image="<%=path%>/images/blank.gif" data-imagecss="flag vc" data-title="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
      <option value='ve' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ve" data-title="Venezuela">Venezuela</option>
      <option value='vg' data-image="<%=path%>/images/blank.gif" data-imagecss="flag vg" data-title="Virgin Islands (British)">Virgin Islands (British)</option>
      <option value='vi' data-image="<%=path%>/images/blank.gif" data-imagecss="flag vi" data-title="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
      <option value='vn' data-image="<%=path%>/images/blank.gif" data-imagecss="flag vn" data-title="Viet Nam">Viet Nam</option>
      <option value='vu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag vu" data-title="Vanuatu">Vanuatu</option>
      <option value='wf' data-image="<%=path%>/images/blank.gif" data-imagecss="flag wf" data-title="Wallis and Futuna">Wallis and Futuna</option>
      <option value='ws' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ws" data-title="Samoa">Samoa</option>
      <option value='ye' data-image="<%=path%>/images/blank.gif" data-imagecss="flag ye" data-title="Yemen">Yemen</option>
      <option value='yt' data-image="<%=path%>/images/blank.gif" data-imagecss="flag yt" data-title="Mayotte">Mayotte</option>
      <option value='yu' data-image="<%=path%>/images/blank.gif" data-imagecss="flag yu" data-title="Yugoslavia (former)">Yugoslavia (former)</option>
      <option value='za' data-image="<%=path%>/images/blank.gif" data-imagecss="flag za" data-title="South Africa">South Africa</option>
      <option value='zm' data-image="<%=path%>/images/blank.gif" data-imagecss="flag zm" data-title="Zambia">Zambia</option>
      <option value='zr' data-image="<%=path%>/images/blank.gif" data-imagecss="flag zr" data-title="Zaire (former)">Zaire (former)</option>
      <option value='zw' data-image="<%=path%>/images/blank.gif" data-imagecss="flag zw" data-title="Zimbabwe">Zimbabwe</option>
    </select>
							
					</div>
					</div>
					<div class="select_col" style="clear:both;">
						Title 
						<form:input name="" type="text" maxlength="30" placeholder="6-15 char is the magic headline!" path="caption" id="caption"/>
					</div>
					<div class="select_col tag_select">
						Tags
						<div style="display:none" id="storypost">
						</div>
						<div class="tag_area">
							<div id="tags"></div>
							<div class="tag tag_add">
						<input name="" type="text" placeholder="# Add Tags" id="tag-text" onkeypress="return validateInputBox(event);" /><a href="#" class="btn_tag_add" id="add-tag">+</a>	
							</div>				
					<div class="clearfix;"></div>
					</div>
					</div>
					<!-- Sharing story -->
					<div class="select_col tag_select email-area" style="display:none;">
						Share Your Story With
						<div style="display:none" id="emailpost">
						</div>
						<div class="tag_area">
							<div id="emails"></div>
							<div class="tag email_add">
						<input name="" type="text" placeholder="emailId" id="email-text" onkeypress="return validateInputBox(event);" />
						<a href="#" class="btn_tag_add" id="add-email">+</a>	
							</div>				
					<div class="clearfix;"></div>
					</div>
					</div>
					<div style="margin-left:5px; clear:both;">
						Your Story
						<form:textarea name="" cols="" rows="" class="type_story txtEditor" placeholder="Write a Story..." path="postDetail" id="postDetail"></form:textarea>
						
						<!--<div style="margin-top:30px;">
							<input name="" type="submit" value="Submit" class="story_form_send"  />
							<div style="border-left:1px solid #d6d6d6; height:30px; float:right;"></div>
							<input name="" type="reset" value="Cancel" class="story_form_reset" />
						</div>-->
						
					</div>
				</div>
				<!-- /Form Right -->
               
               <div class="clearfix"></div> 
                </div>
                
                	<div style="margin-top:30px; border-top:1px solid #fff; padding-top:20px;">
							<input name="" type="submit" value="Share" class="story_form_send"  id="share-update"/>
							<div style="border-left:1px solid #d6d6d6; height:30px; float:right;"></div>
							<div class="story_form_send" onclick="loadStoryData();" >Preview</div>
						<div class="clearfix"></div>
                        </div>
                
			</form:form>
		<div class="clearfix"></div>
		</div>
		<!-- /Story Submit Form -->
		
        </div>
    <div class="clearfix"></div>
    </div>
    <!-- /Content Section -->     
    
     <!-- Share Popup -->
    <div class="story_share_popup" style="display:none;" id="private-share">
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path%>/img/closebox.png"></a></div>
        	<div class="title_popup">Sharing Story Privately</div>
        
        
			<br>    		
            Link to share<br>
            <input type="text" name="" id="private-url" placeholder="http://www.gloomy.com" readonly />
            
            <div class="invite_people">
            Invite People<br>
            <input type="text"  id="emailIds" name="" onkeyup="return validateEmailId();"  placeholder="email@gmail.com,email@yahoo.com" />
            <input type="hidden" id="postId" />
            </div>   
            
            <input type="submit" value="Send" id="share-via-email" onclick="shareViaEmail();" style="display:none;"/>
        
        	      
        </div>
        
        
    </div>
    <!-- /Share Popup -->
    
    
    
    
    
    
    
    <!-- Share Popup -->
    <div class="story_share_popup" style="display:none;" id="public-share" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="publicSharePopup();"><img src="<%=path%>/img/closebox.png"></a></div>
        	
            <div class="title_popup">Sharing Story With Social Friends</div>
            
            <br>    		
            Link to share<br>
            <input type="text" name="" id="public-url" placeholder="file:///C:/wamp/www/fetaboo/submit_story.html" readonly />
        
        	<div class="share_social_icon">
                <ul>
                	<li style="margin:0;">Share link via:</li>
                    <li><a href="#" id="fb-share-post" title="Share on Facebook"><img src="<%=path%>/images/share_facebook.png"/></a></li>
                    <li><a href="https://plus.google.com/share" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" id="share_gplus" title="Share on Google+"><img src="<%=path%>/images/share_gplus.png"/></a></li>
                    <li><a id="share_tumbler" href="http://www.tumblr.com/share/link" title="Share on Tumblr"><img src="<%=path%>/images/share_tumblr.png"/></a></li>
                    
                </ul>
            <div class="clearfix"></div>
            </div>        
        </div>
        
        
    </div>
    <!-- /Share Popup -->
    
    <!----- Story Preview Popup -->
     <div style="display:none;" id="story-preview">
 <div class="story_share_popup story_popup_2">
 <div class="story_popup_close tick"><img src="images/StoryPopshare.png"/></div>
 <div class="story_popup_close" onClick="close_popup();"><img src="images/StoryPopclose.png"/></div>
 
 <div class="story_popup_scroll">
 	<div class="story_popup">
    	
        <div class="article_area fetaured_post story_preview">
        
        	<!-- Article Header -->
        	<div class="article_title">
            	<h1 id="post-caption"></h1>
                
                <!-- Share Post --
                <div class="post_share">
                	<div class="hover_post_share">
                	<!-- <div class="post_text" onClick="post_share();"></div> --
                	<button class="share_post_btn" onClick="post_share();"></button>
                        <div class="post_icon" style="display:none;">
                        	<a ><img src="images/ic_facebook.png"/></a>
                            <a ><img src="images/ic_gp.png"/></a>
                            <a ><img src="images/ic_tumbler.png"/></a>
                        </div>
                    
                    <button class="share_post_btn_minus" onClick="post_share_icon();" style="display:none;"></button>
                    </div>
                    
                     <div  class="story_edit" ></div>
                     <div class="story_delete" ></div>
                     
                    <!-- User Menu Hover --
                    <div class="user_menu">
                    	<div class="user_option_2">
                        	<ul>
                                <li><a href="#" >Report User</a></li>
                                <li><a href="#" >Block User</a></li>
                                <li><a href="#" >Hide a Content</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- User Menu Hover --
                    
                <div class="clearfix"></div>
                </div>
                
                
                
                
                
                
                
                -->
                
                
                
                
                <div class="post_share">
                   <!-- User Menu Hover -->
                    <div class="user_menu">
                    	<div class="user_option_2">
                        	<ul>
                                <li><a href="#" >Report User</a></li>
                                <li><a href="#" >Block User</a></li>
                                <li><a href="#" >Hide a Content</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- User Menu Hover -->
                	<div class="hover_post_share share_post_btn Hello">
                	<!-- <div class="post_text" onClick="post_share();"></div> -->
                	<!-- <button class="share_post_btn" onClick="post_share();"></button> -->
                        <div class="post_icon" >
                        <ul>
                        	<li><a ><img src="images/ic_facebook.png"/></a></li>
                            <li><a ><img src="images/ic_gp.png"/></a></li>
                            <li><a ><img src="images/ic_tumbler.png"/></a></li>
                            </ul>
                        </div>
                    
                    <button class="share_post_btn_minus" onClick="post_share_icon();" style="display:none;"></button>
                    </div>
                     <div class="story_delete" style="display:none;" onclick="showDeletePopup();"></div>
                     <div  class="story_edit" style="display:none;" onclick="editPost();"></div>
                    
                     
                 
                    
                <div class="clearfix"></div>
                </div>
                
                
                <div class="clearfix"></div>
                </div>
                <!-- /Share Post -->
                <!-- /Article Header -->
                
                
            <!-- Article -->
            <div class="article_user">
            	<img src="" class="art_user_img" id="dummy-avatar">
                <!-- by <span class="art_user_name" id="written-by"></span> --> <br>
				on <span id="post-date"></span>
            <div class="clearfix"></div>
            </div>
            
            <div class="art_content" id="post-detail">
            
            </div>
            
            <div class="art_tag_area" id="post-tag">
        	<div class="art_tag">#erjhgfj</div>
               	
            </div>
            <div style="clear:both;"></div> 
            <!-- /Article -->
            
            	<div class="story_bottom_like">
                        <div class="ic_like">
                            <div style="float:left;"><a class="ic_story_like " ></a>&nbsp;</a><span >0</span></div>
                        </div>
                        <div class="ic_comment">
                            <div style="float:left;"><a class="ic_story_comment"></a><span >0</span></div>
                            <div style="float:left;"><a class="ic_story_view"></a><span >0</span></div>
                        </div>
                    <div class="clearfix"></div>
                 </div>
            
            </div>
        </div>
        
        
        
        
 </div>
        
        
        
    </div>
     <a href="#" class="back-to-top"></a> 
     
  <div style="background:rgba(0, 0, 0, 0.8); width:100%; height:100%; position:fixed; top:0; left:0; z-index:99;"></div>
  
 </div>
    
    <!-- Story popup end -->
    
    
    <a href="#" class="back-to-top"></a>
     <script>
     $(function(){
    	 $(".tick").click(function(){
    			$("#share-update").click();
    		});
    	 
     });
     $("#fb-share-post").click(function(){
    	 
    	 sharingOnFb($("#public-url").val());
     });
     
     var url = $("#share_tumbler").attr('href');
     var Gurl = $("#share_gplus").attr('href');
    
		if('${postType}'!='' ) {
			if('${submitStoryError}' !='') {
				var data = ${postRequestJson};
				var html = data.postDetail;
				setTimeout(function(){$("#contentarea").html(html);}, 1000);
				$.each(data.tags , function(index,val){
					id="tag-"+count;
					$("#tags").prepend('<div class="tag" id="'+id+'" ><span>'+ val + '</span><a style="cursor:pointer;" class="btn_tag_del" onclick=tagDelete("'+count+'")>X</a></div>').hide().fadeIn(500);
					$("#storypost").append('<input type="hidden" id="'+id+'-tag" value="'+val+'" name="tags['+tagCount+']" />');
					tagCount = tagCount + 1;
					count=count+1;
			});
				errorMessage('${postType}'+" "+'${submitStoryError}');
			} else if('${submitStorySuccess}' !='') {
				$("#share_tumbler").attr('href',url+"?url=${URL}"+"&name="+name+"&description=hello");
				$("#share_gplus").attr('href',Gurl+"?url=${URL}");
				successMessage('${postType}'+" "+'${submitStorySuccess}');
				if('${postType}' == 'PUBLIC') {
					if('${URL}'!="") {
					$("#public-share").show();	
					$("#public-url").val('${URL}');
					} else {
						redirectOnLoginSuccess();
					} 
				}
			}
		}
		if('${edit}' == "true") {
			$("#submitPostform").attr("action" ,"updatepost.htm");
			$("#share-update").val("Update");
			var data = ${postRequestJson};
			var html = data.postDetail;
			setTimeout(function(){$("#contentarea").html(html);}, 1000);
			$("."+data.categoryType).attr("selected",true);
			$("#sample").attr("disabled" , true);
			$("#filePhoto").hide();
			if(data.lresURL != null && data.lresURL !="" ) {
				$('#previewHolder').attr('src','${postRequest.lresURL}');	
			} else {
				$('#previewHolder').attr('src',$("."+data.categoryType).attr("id"));
			}
			
			$.each(data.tags , function(index,val){
					id="tag-"+count;
					$("#tags").prepend('<div class="tag" id="'+id+'" ><span>'+ val + '</span><a style="cursor:pointer;" class="btn_tag_del" onclick=tagDelete("'+count+'")>X</a></div>').hide().fadeIn(500);
					$("#storypost").append('<input type="hidden" id="'+id+'-tag" value="'+val+'" name="tags['+tagCount+']" />');
					tagCount = tagCount + 1;
					count=count+1;
			});
		}
		
		function close_popup() {
			$("#story-preview").fadeOut(500);	
			//redirectOnLoginSuccess();
		}
		function loadStoryData() {
			if($(".Editor-editor").html() !="") {
			 makingAvatar();
			 $("#dummy-avatar").attr("src" , currentAvatar);
			 $("#written-by").html('${usermodel.userDisplayName}');
			 $("#post-caption").html($("#caption").val());
			 var date = new Date();
				$("#post-date").html(date.getDate()+' '+months[date.getMonth()]+','+(date.getYear()+1990));
				$("#post-detail").html($(".Editor-editor").html());
				$("#post-tag").html("");
				$("#tags .tag").each(function(index,val){
				$("#post-tag").append('<div class="art_tag">#'+$(val).text()+'</div>');
				});
				//$("#post-tag").append('<div class="art_tag">#YourTag</div><div class="clearfix"></div>');
			 $("#story-preview").fadeIn(300);
		}
		 else {
			errorMessage("Write Story for previewing");
		}
		}
		   $(window).keyup(function(event) {
	           if(event.which === 27) {
	        	   $("#story-preview").fadeOut(200);
	        	   
	           }
	   });
		   function publicSharePopup() {
			   redirectOnLoginSuccess();
		   }
    </script>


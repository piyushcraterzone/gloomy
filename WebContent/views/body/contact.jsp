<%@ page language="java" contentType="text/html; charset=utf-8"%>

    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
%>
<script>
var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
function validateform() {
	if($("#name").val() =="" || $("#name").val().length <=0) {
		errorMessage("please enter Your Name");
		return false;
	} if($("#email").val()=="" ||reg.test($("#email").val())==false) {
		errorMessage("Email id is Invalid");user_menu
		return false;
	}
	return true;
}
</script>
<!-- Content Section --> 
    <div class="content_grey content">
    	<div class="container contact">
        
        	<div class="col-lg-4">
            	<p style="font-size:16px;">Feel free to write anything you like , we usually reply to all emails within 24 hours or so.</p>
                <h3>We Also Do Support Through Socials</h3>
                <div style="text-align:center;">
                    <div class="contact_social_link"><a href="#"><img src="<%=path %>/images/contact_fetaboo.png" alt="Image"/></a></div>
                    <div class="contact_social_link"><a href="#"><img src="<%=path %>/images/contact_facebook.png" alt="Image"/></a></div>
                    <div class="contact_social_link"><a href="#"><img src="<%=path %>/images/contact_tumblr.png" alt="Image"/></a></div>
                    <div class="contact_social_link"><a href="#"><img src="<%=path %>/images/contact_google.png" alt="Image"/></a></div>
                </div>
            </div>
            
            <div class="col-lg-7 contact_form">
            	<h3 style="margin:0;">Contact Form</h3>
                <form:form action="submitcontact" method="POST" commandName="contact" onsubmit="return validateform();">
                	<label>Your Name (required)</label>
                    <form:input type="text" name="" path="name" id="name"/>
                    <label>Your Email (required)</label>
                    <form:input type="email" name="" path="emailId" id="email"/>
                    <label>Subject</label>
                    <form:input type="text" name="" path="subject"/>
                    <label>Your Message</label>
                    <form:textarea name="" cols="" rows="" path="message"></form:textarea>
                    <input name="" type="submit" value="Send Message">
                </form:form>
            </div>
    	
    <div class="clearfix"></div>
    </div>
    <!-- /Content Section --> 
   <a href="#" class="back-to-top"></a>
  <a href="submitstory.htm"  onclick="return isValidSession('${usermodel}');"><div class="circle"><img src="images/submit_fetaboo.png" /></div></a>
<!--end of circle--> 
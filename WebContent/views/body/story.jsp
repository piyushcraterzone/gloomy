<%@ taglib uri="http://www.springframework.org/tags/form" prefix="step"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
  <jsp:useBean id="dateValue" class="java.util.Date" />
 
   <%
	String path = request.getContextPath();
%>
<style>
.spinner {
margin-top: 23px;
}
</style>
 
<script type="text/javascript">
var commentCount=0;
var similarStoriesCount = 0;
var isNextAjaxCall = true;
var catType = 'FETABOO';
var data = ${storiesJson};
makingAvatar();

/* function likeAndShare(postId,userId,type) {
	if(isValidSession('${usermodel}')) {
		 var data={"postId":postId,"userId":userId,"type":type};
		 ajaxCallingGET(METHOD.POST,JSON.stringify(data),URL.LIKE_SHARE_POST, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.LIKE_SHARE);
		 $(".ic_story_like").toggleClass("storyliked");
	  } 
	} */
function likeAndShare(postId,userId,type) {
	  if(isValidSession('${usermodel}')) {
		  var data={"postId":postId,"userId":userId,"type":type};
		  if($("#"+postId+"-like").hasClass("storyliked")) {
			  $("#"+data.postId+"-like").html(parseInt($("#"+data.postId+"-like").html())-1);
		  } else {
			  $("#"+data.postId+"-like").html(parseInt($("#"+data.postId+"-like").html())+1);
		  }
		  $("#"+postId+"-like").toggleClass("storyliked");
			ajaxCallingGET(METHOD.POST,JSON.stringify(data),URL.LIKE_SHARE_POST, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.LIKE_SHARE);
			return true;		
			}
			return false;
	  }

	
function postComment() {
	var message= $("#commentMessage").val().trim();
	if(!isValidSession('${usermodel}')) {
		return false;
	}
	if(message!= "") {
		target = document.getElementById("add-commentSpinner");
		var spinner = spinnerShow(target,opts);
		var  data={"comment":message,"userId":$("#userId").val(),"postId":$("#postId").val()};
		 ajaxCallingGET(METHOD.POST,JSON.stringify(data),URL.ADD_COMMENT, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.ADD_COMMENT,target,spinner);
	}
	return false;
	
}
function addComment(response,message,target,spinner) {
	spinnerHide(target,spinner);
	if(response!=WEB_SERVICE_CODE.NO_CONTENT && response!=WEB_SERVICE_CODE.ERROR && response!=WEB_SERVICE_CODE.BAD_REQUEST && response !="") {
		response = JSON.parse(response);
		makingAvatar();
		var previousCountComment = $("#comment-"+$("#postId").val()).html();
		previousCountComment = parseInt(previousCountComment) + 1;
		$("#comment-"+$("#postId").val()).html(previousCountComment);
		
		$("#post-comments").append('<li class="comment_area"><div><img class="art_user_img" src="'+currentAvatar+'" alt=""></div>'
                +'<div class="comment_text">'
               +'</h2>'+message
                +'<div class="comment_date">'+new Date(parseInt(response.commentDate)).toDateString()+'</div>'
                +'<div class="clearfix"></div>'
              	+'</div>'
            	+'</li>');
		$("#commentMessage").val("");
		}
}


$(function(){
	$("#user-avatar").attr("src" , currentAvatar);
	$("#comment-avatar").attr("src" , currentAvatar);
	 target = document.getElementById("comments-container");
	var spinner = spinnerShow(target,opts);
	getComments(commentCount,'${stories.postId}',target,spinner);
	
});
function loadComment(data,target,spinner) {
	spinnerHide(target,spinner);
	 var comment=1;
	$("#post-comments").html("");
	if(data!="") {
		data =JSON.parse(data);
	}
	$.each(data,function(index,val) {
		$.each(val,function(index1,val1) {
			makingAvatar();
		comment=comment+1;
		$("#post-comments").append('<li class="comment_area"><div ><img id="user-avatar" class="art_user_img" src="'+currentAvatar+'" alt=""></div>'
                +'<div class="comment_text">'
                +'</h2>'+val1.message
                +'<div class="comment_date">'+new Date(val1.commentDate).toDateString()+'</div>'
                +'<div class="clearfix"></div>'
              	+'</div>'
              	+'<div class="clearfix"></div>'
            	+'</li>');
	});
	});
}

function getComments(count,postId,target,spinner) {
	  $("#postId").val(postId);
	  ajaxCallingGET( METHOD.GET,'', URL.GET_COMMENT+"?postId="+postId+"&&count="+count, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.GET_COMMENT, target, spinner);
}
</script>

 <!-- --------------------------OLD PART END NEW PART BEGIN -->
 <!-- Start Story Content -->
 <!-- Content -->
    <div style="clear:both;"></div>
    <div class="pub_pri">
    <div class="container content">
    
		<!-- Story Section -->
        <div class="col-lg-12 slide_btn" style="overflow:hidden; padding:0;">
       

        	
     <div class="col-lg-10 full_story">
     
     
        	<div class="article_area fetaured_post">
        
        	<!-- Article Header -->
        	<div class="article_title">
            	<h1 id="post-caption">${stories.caption }</h1>
                 <input type="hidden" id="userDisplayName-${stories.postId}" value="${stories.userThumbInfo.userDisplayName }" />
            <input type="hidden" id="postByUserId-${stories.postId}" value="${stories.userThumbInfo.userId }" />
            <input type="hidden" id="postByUserLresURL-${stories.postId}" value="${stories.userThumbInfo.lresURL }" />
            <input type="hidden" id="reporterUserId-${stories.postId }" value ="${usermodel.userId }" /> 
                    
                    <div class="post_share">
                   <!-- User Menu Hover -->
                    <div class="user_menu">
                    	<div class="user_option_2">
                        	<ul>
                                <li><a href="#" onClick="abuseFetaboo('${usermodel.userId}','user_report','reported');">Report User</a></li>
                                <li><a href="#" onClick="abuseFetaboo('${usermodel.userId}','user_block','blocked');">Block User</a></li>
                                <li><a href="#" onClick="abuseFetaboo('${usermodel.userId}','content_report','content');">Hide a Content</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- User Menu Hover -->
                	<div class="hover_post_share share_post_btn Hello">
                        <div class="post_icon" >
                        <ul>
                        	<li><a onclick="shareFb()"><img src="images/ic_facebook.png"/></a></li>
                            <li><a onclick="shareGplus()"><img src="images/ic_gp.png"/></a></li>
                            <li><a onclick="shareTumbler()"><img src="images/ic_tumbler.png"/></a></li>
                            </ul>
                        </div>
                    
                    </div>
                    
                <div class="clearfix"></div>
                </div>
                    
                   <!--  <div class="story_edit"></div> -->
                    
                   
               
                <div class="clearfix"></div>
                </div>
                <!-- /Share Post -->
                <!-- /Article Header -->
               <jsp:setProperty name="dateValue" property="time" value="${stories.postDate}" />
                
            <!-- Article -->
            <div class="article_user">
              <img src="" class="art_user_img" id="user-avatar">
                <br>
				on <span id="post-date"><fmt:formatDate type="date" value="${dateValue}" pattern="dd MMM,yyyy" />
            <div class="clearfix"></div>
            </div>
            
            <div class="art_content" id="post-detail">
            	${stories.postDetail }
            </div>
           
            <div class="art_tag_area" id="post-tag">
             <c:forEach items="${stories.tags}" var="tags" >
            	<div class="art_tag">#${tags }</div>
               </c:forEach>
            <div class="clearfix"></div>
            </div>
            
            <!-- /Article -->
            
            	 	 <div class="story_bottom_like">
                        <div class="ic_like">
                            <div style="float:left;"><a class="ic_story_like" id="${stories.postId }-like"   onclick="likeAndShare('${stories.postId }','${usermodel.userId}','like')" ></a>&nbsp;<label id="like-${stories.postId }"><c:choose><c:when test="${stories.likes.size()!=null }">${stories.likes.size() }</c:when><c:otherwise>0</c:otherwise></c:choose></label></div>
                        </div>
                        <div class="ic_comment">
                            <div style="float:left;"><a class="ic_story_comment"></a><span id="post-comment"><label id="comment-${stories.postId }">${stories.comments }</label></span></div>
                            <div style="float:left;"><a class="ic_story_view isviewed"></a><span id="post-view">${stories.viewCounter }</span></div>
                        </div>
                    <div class="clearfix"></div>
                 </div>  
                    <div class="clearfix"></div>
                 </div>
            
            </div>
     
    
           
 </div></div>
 <!-- /Story Section -->   
   
   <div class="clearfix"></div> 
    <!-- /Content -->
    
     <!-- Comments -->
     <div class="container comments" >
        <h1>Comments</h1>
            <ul id="post-comments">
               
             </ul>
            <div id="comments-container" style="text-align:center;height:37px;display:none"></div>
           <ul>

           <li class="comment_area" style="border-bottom:2px solid #fff;">
           <div class="comment_img">
        				<img src="<%=path %>/images/avater_img.png" class="art_user_img" id="comment-avatar">
    						
		                    </div>
		                    <div class="comment_text story_comm_2">
		                    <form onsubmit="return postComment();">
		                     <input id="commentMessage" type="text" class="comment_field" placeholder="Write a comment..."/>
		                    <input  type="hidden" id="postId" class="comment_post"/>
		                    <input type="hidden" id="userId" value="${usermodel.userId}" class="comment_post"/>
		                    <input type="submit" value="Post" class="comment_post"/>
		                     <div id="add-commentSpinner" style="float:left;height:37px;display:block"></div>
		                    </form>
		                    <div class="clearfix"></div>
		                    </div>
		                   <div class="clearfix"></div>
		                    </li>
           </ul> 
            
     </div>
     <!-- /Comments -->
 </div>
 
 <!--  NEW PART ENDS -->
      
<!-- Block User Popup -->
   <!-- Block User Popup -->
    <div id="user_block" class="story_share_popup" style="display:none;" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path %>/img/closebox.png"></a></div>
        	 <div >Block this User and his Posts!</div>
            <div class="title_popup" id="blocked"></div>
            
            <br>    		
            <p>Are you sure you want to block this user!</p><br>
            
            <a href="#" class="yes_btn" onclick="blockUser();">Yes</a>
            <a href="#" class="no_btn" onclick="share_popup_close();">No</a>
           <br><br><br>         
                   
    </div> 
    </div>
    <!-- /Block User Popup -->
    
    
   
    <!-- Report User Popup -->
    <div id="user_report" class="story_share_popup" style="display:none;" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path %>/img/closebox.png"></a></div>
        	<div >Report User !</div>
            <div class="title_popup" id="reported"></div>
            <h4>Select a reason</h4>
            <table>
            	<tr>
                	<td width="20"><input name="option1" id="cause-1" type="radio" value="" checked></td>
                    <td class="cause-1">User has inappropriate content</td>
                </tr>
                <tr>
                	<td><input name="option1" id="cause-2" type="radio" value="" /></td>
                    <td class="cause-2">User is sending spam</td>
                </tr>
                <tr>
                	<td><input name="option1" type="radio" id="cause-3" value="" /></td>
                    <td class="cause-3">User is rude or abusive</td>
                </tr>
            </table>
            
            <h4>Add a comment</h4>
            <textarea name="" cols="" rows="" class="report_textarea"></textarea>
          <div class="title_popup" id="CZ_messageAfterprocessed"></div>   <div class="report_submit"><input name="" class="CZ_messageAfterprocessed" type="submit" value="Submit" onclick="reportUser();" /></div>
    
    <div class="clearfix"></div>  
    </div>    
    </div>
    <!-- /Report User Popup -->
    
    <!-- Report Content Popup -->
    <div id="content_report" class="story_share_popup" style="display:none;" >
    
    	<div class="share_content">
        <div class="share_popup_close_btn"><a href="#" onClick="share_popup_close();"><img src="<%=path %>/img/closebox.png"></a></div>
        	<div >Hide Content!</div>
            <div class="title_popup" id="content"></div>
            <h4>Select a reason</h4>
            <table>
            	<tr>
                	<td width="20"><input name="option2" type="radio" value="" checked></td>
                    <td>I'm in this post and I dont like it</td>
                </tr>
                <tr>
                	<td><input name="option2" type="radio" value=""></td>
                    <td>It's annoyig or not interesting</td>
                </tr>
                <tr>
                	<td><input name="option2" type="radio" value=""></td>
                    <td>It's spam</td>
                </tr>
            </table>
            
            <h4>Add a comment</h4>
            <textarea name="" cols="" rows="" class="report_textarea"></textarea>
           <div class="title_popup" id="CZ_messageAfterprocessedContentReport"></div> <div class="report_submit"><input name="" type="submit" class="CZ_messageAfterprocessedContentReport" value="Submit" id="CZ_contentReportButton" onclick="hidePost();"></div>
    
    <div class="clearfix"></div> 
    </div>     
    </div>
    <!-- /Report Content Popup -->  
      <a href="#" class="back-to-top"></a>
     <a href="submitstory.htm"  onclick="return isValidSession('${usermodel}');"><div class="circle"><img src="images/submit_fetaboo.png" /></div></a>
  
     <script>
		if('${usermodel.userId}'=='${stories.userThumbInfo.userId}') {
			$(".user_menu").hide();
		}  
		 if(isLoggedIn('${usermodel}')) {
	     	   if(data.likes!=null && data.likes.length > 0) {
	     		   for(var i=0;i<data.likes.length;i++) {
	     			   if(data.likes[i].userId=='${usermodel.userId}') {
	     				   $("#"+data.postId+"-like").toggleClass("storyliked");
	     			   }
	     		   }
	     	   }
	     }
     function abuseFetaboo(userId,id,divId) {
		 //console.log('${storiesJson}');
	 		if(!isValidSession('${usermodel}')) {
	 		return false;
	 		}
	 		var userDisplayName = '${stories.userThumbInfo.userDisplayName}';
			 $("#"+divId).html(userDisplayName);
			$("#"+id).show();
			currentPopupId = "#"+id;
			$(".report_textarea").val("");
			$(".CZ_messageAfterprocessed").show();
			$(".CZ_messageAfterprocessedContentReport").show();
			$("#CZ_messageAfterprocessed").html("");
			$("#CZ_messageAfterprocessedContentReport").html("");
			reportingUser.abuseUserId='${stories.userThumbInfo.userId}';
			reportingUser.userId=userId;
					hide.userId=userId;
					hide.postId=currentPostId;
		}  

  function reportUser() {
			var id=$("input[name=option1]:checked").attr('id');
			reportingUser.cause=$("."+id).html();
			ajaxCallingGET(METHOD.POST,JSON.stringify(reportingUser), URL.REPORT_USER, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.REPORT_USER);
		}

		function blockUser() {
			ajaxCallingGET(METHOD.POST,JSON.stringify(reportingUser), URL.BLOCK_USER, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.BLOCK_USER);
		}
		function hidePost() {
			var id=$("input[name=option2]:checked").attr('id');
			var cause = $("."+id).html();
			ajaxCallingGET(METHOD.POST,JSON.stringify(hide), URL.HIDE_POST, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.HIDE_POST);
			
		}

		function reportUserCallBack(data) {
			$("#CZ_messageAfterprocessed").html("User reported successfully, Mail has been sent to User");
			$(".CZ_messageAfterprocessed").hide();
			currentPopupId = "#user_report";
		}
		function blockUserCallBack(data) {
			$("#user_block").fadeOut(100);
		}
		function hidePostCallback(hide) {
			//$("#content_report").hide();
			$("#"+hide.postId).fadeOut(500);
			$("#CZ_messageAfterprocessedContentReport ").html("you will no longer see his posts");
			$(".CZ_messageAfterprocessedContentReport ").hide();
			currentPopupId = "#content_report";
		}
		
function shareFb() {
	  sharingOnFb('${stories.postURL}');
}	
function shareGplus() {
	  sharingOnGooglePlus('${stories.postURL}');
} 
function shareTumbler() {
	  sharingOnTumbler('${stories.postURL}');
}

function share_popup_close() {
		$(currentPopupId).fadeOut(300);
	}
$(window).keyup(function(event) {
  if(event.which === 27) {
	  $(currentPopupId).fadeOut(300);
  }
}); 
     
     </script>
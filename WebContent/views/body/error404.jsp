<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%
	String path = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Page Not Found</title>
<link rel="icon" type="png/ico"  href="<%=path %>/img/favicon.ico">
<link rel="stylesheet" href="<%=path%>/css/error_style.css" type="text/css" />
</head>

<body>
<div class="wrapper">

	<div class="box">
    	<div class="danger1 fadeIn"><img src="<%=path%>/images/400rr.png"/></div>
        <div class="cont1 fadeIn">Page Not Found Error </div>
        
        <div class="msg">
        <h2>Problem Problem Problem!</h2>
    <p>The page that you re looking for does not exist, go back, friend , go back.</p>
    </div>
    <div class="blow">
    	<ul>
        	<li><a href="http://www.gloomy.com">Home</a></li>
            <li><a href="http://www.gloomy.com/contact.htm">Contact</a></li>
        </ul>
    </div>
        
    </div>

</div>
</body>
</html>

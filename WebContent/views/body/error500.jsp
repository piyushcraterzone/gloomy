<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
 <%
	String path = request.getContextPath();
%>
<link rel="stylesheet" href="<%=path%>/css/error_style.css" type="text/css" />
<link rel="icon" type="png/ico"  href="<%=path %>/img/favicon.ico">
<title>Gloomy Error</title>
</head>

<body>
<div class="wrapper">

<div class="box">
	
    <div class="danger fadeIn"><img src="<%=path%>/images/500rr.png" /></div>
    <div class="cont fadeIn">Internal Server Error </div>
    <div class="msg">
    <p>An error occurred while executing an external process: </p>
    </div>
    <div class="blow">
    	<ul>
        	<li><a href="http://www.fetaboo.com">Home</a></li>
            <li><a href="http://www.fetaboo.com/contact.htm">Contact</a></li>
        </ul>
    </div>

</div>

</div>
</body>
</html>

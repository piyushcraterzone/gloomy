var stories = new Array();
var mostRead = new Array();
var mostCommented = new Array();
/* this object is for Server Address */
var SERVER = {
		//LOCAL: "http://192.168.1.2:8080/gloomy/",
		LOCAL: "http://178.62.182.120:8080/",
};
/* This object is for Urls of hitting on web */
var URL = {
		REDIRECT_AFTER_LOGIN : SERVER.LOCAL+ "index1.htm",
		REDIRECT_ON_EDIT_POST : SERVER.LOCAL + "submitstory.htm",
		PUBLIC_POST : SERVER.LOCAL+"public.htm",
		SOCIAL_LOGIN:SERVER.LOCAL + "sociallogin.htm",
		CATEGORY_COUNT:SERVER.LOCAL + "categorycount.htm",
		LIKE_SHARE_POST :SERVER.LOCAL + "addlikeshare.htm",
		ADD_COMMENT :SERVER.LOCAL + "addcomment.htm",
		GET_COMMENT:SERVER.LOCAL + "comment.htm",
		SEARCH_CATEGORY:SERVER.LOCAL + "searchcategory.htm",
		SHARE_PRIVATE_POST:SERVER.LOCAL + "shareprivatly.htm",
		REPORT_USER: SERVER.LOCAL + "reportuser.htm",
		BLOCK_USER: SERVER.LOCAL + "blockuser.htm",
		HIDE_POST: SERVER.LOCAL + "hide.htm",
		SUBSCRIBE: SERVER.LOCAL + "subscribe.htm",
		POPULAR_TAGS: SERVER.LOCAL + "populartag.htm",
		MOST_COMMENTED: SERVER.LOCAL + "mostcommented.htm",
		MOST_VIEWED: SERVER.LOCAL + "mostviewed.htm",
		MOST_SHARED: SERVER.LOCAL + "mostshared.htm",
		SIMILAR_STORIES : SERVER.LOCAL + "similarpost.htm",
		VIEW : SERVER.LOCAL + "view.htm",
		DELETE_COMMENT : SERVER.LOCAL + "commentdelete.htm", 
		MY_POST : SERVER.LOCAL + "myPost.htm",
		POST_BY_TAG : SERVER.LOCAL + "postbytag.htm",
		UPDATE_POST : SERVER.LOCAL + "editpost.htm",
		DELETE_POST : SERVER.LOCAL + "deletepost.htm",
		
};
var METHOD = {
	POST:"POST",
	GET:"GET",
	PUT:"PUT"
};

var SOCIAL_APP_IDS = {
		FACEBOOK:"520477404731481",
		GOOGLE_PLUS:"478061008388-9boqqpca5s4lv22rg9p7itcld2elf9ps.apps.googleusercontent.com",
};
var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];


var opts = {
		  lines: 7, // The number of lines to draw
		  length: 4, // The length of each line
		  width: 2, // The line thickness
		  radius: 8, // The radius of the inner circle
		  corners: 0.1, // Corner roundness (0..1)
		  rotate: 11, // The rotation offset
		  direction: 1, // 1: clockwise, -1: counterclockwise
		  color: '#000', // #rgb or #rrggbb or array of colors
		  speed: 2.1, // Rounds per second
		  trail: 24, // Afterglow percentage
		  shadow: false, // Whether to render a shadow
		  hwaccel: true, // Whether to use hardware acceleration
		  className: 'spinner', // The CSS class to assign to the spinner
		  zIndex: 5, // The z-index (defaults to 2000000000)
		  top: 'auto', // Top position relative to parent
		  left: '50%', // Left position relative to parent
		
		};

var contentSpinnerOpts = {
		 lines: 11, // The number of lines to draw
		  length: 17, // The length of each line
		  width: 4, // The line thickness
		  radius: 30, // The radius of the inner circle
		  corners: 1, // Corner roundness (0..1)
		  rotate: 52, // The rotation offset
		  direction: 1, // 1: clockwise, -1: counterclockwise
		  color: '#000', // #rgb or #rrggbb or array of colors
		  speed: 2.1, // Rounds per second
		  trail: 51, // Afterglow percentage
		  shadow: true, // Whether to render a shadow
		  hwaccel: true, // Whether to use hardware acceleration
		  className: 'spinner', // The CSS class to assign to the spinner
		  zIndex: 2e9, // The z-index (defaults to 2000000000)
		  top: '40%', // Top position relative to parent
		  left: '40%' // Left position relative to parent
		};

var WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS = {
		DASHBOARD_DATA_PAYLOAD:'dashboard',
		POST_CATEGORY_COUNT_DATA_PAYLOAD:'categoryCount',
		SEARCH_CATEGORY:'searchCategory',
		PUBLIC_POST : 'publicPost',
		SHARE_PRIVATE_POST : 'sharePost',
		LIKE_SHARE : 'likeshare',
		VIEW : 'view',
		ADD_COMMENT : 'addComment',
		GET_COMMENT : 'getComment',
		GET_SIMILER_POST : 'getSimilerPost',
		REPORT_USER : 'reportUser',
		BLOCK_USER : 'blockUser',
		SOCIAL_LOGIN : "socialLogin",
		HIDE_POST : "hidePost",
		SUBSCRIBE : "subscribe",
		MOST_COMMENTED : "mostComment",
		MOST_SHARED : "mostShared",
		MOST_VIEWED : "mostViewed",
		POPULAR_TAG : "popularTag",
		DELETE_COMMENT : "deleteComment",
		MY_POST : "myPost",
		POST_BY_TAG : "postByTag",
		UPDATE_POST : "updatePost",
		DELETE_POST : "deletepost",
		
};
var WEB_SERVICE_CODE = {
		NO_CONTENT:"204",
		ERROR:"500",
		BAD_REQUEST:"400",
		SUCCESS:"200"
};


/* this object is for making users object when abusing the user */
var reportingUser = {
		
		
};
var socialLogin = {	
};
var hide = {
};

/* This variable is for checking the request is completed or not  */
var isRequestComplete = true;
var commentCount = 0;
var postURL ="";
var currentPostId ="";
var postCount = 0;
var currentURL = URL.PUBLIC_POST;
var defaultCategory = "FETABOO";
 var isNextCall = true;
 var currentTag = "";
 var avatarCount = 13;
 var currentAvatar = "./images/avater_img.png";
 var avatarArray = new Array();
 var currentPopupId = "";
 var storyType = "PUB";
 for(var i=1 ; i<=avatarCount ; i++) {
	 avatarArray[i]="./avatar/A"+i + ".png";
 }
 
 function makingAvatar() {
	    var x = Math.floor((Math.random() * avatarCount) + 1);
	    currentAvatar = avatarArray[x];
	}
 
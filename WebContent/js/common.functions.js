/* This Function for Hiding the Pop up Dialog Box  */
function share_popup_close() {
	$(".story_share_popup").hide();	
}
/* This Function is for Checking the session of user Loged in or not */
function isValidSession(session) {
	if(session == undefined || session =='' || session == "" || session ==null) {
		 $(".topopup").click();
		 $(".back-to-top").click();
		return false;
	} 
	return true;
}
function isLoggedIn(session) {
	if(session == undefined || session =='' || session == "" || session ==null) {
		return false;
	} 
	return true;
}
/* This function for Showing the Error message overlay  */
function errorMessage(message) {

	$("#error-message").html("");
	$("#error-message").html(message);
	$("#alert-error").slideDown(1000);
	setTimeout(function() {$("#alert-error").slideUp(1000)}, 2000);
}
/* This function for showing Success Message Overlay */
function successMessage(message) {
	$("#success-message").html("");
	$("#alert-success").slideDown(1000);
	$("#success-message").html(message);
	setTimeout(function() {$("#alert-success").fadeOut(1500)}, 2000);
}
/* This Function for show the Icons for sharing the Stories with social  *//* stories.jsp*/
function post_share(id) {
	
	$("#post_text-"+id).hide();
	$("#post_icon-"+id).show();
	$("#share_post_btn-"+id).hide();
	$("#share_post_btn_minus-"+id).show();
}

/* This function is for Hiding the Icons for sharing the Stories with social   *//* stories.jsp*/
function post_share_icon(id) {
	$("#post_text-"+id).show();
	$("#post_icon-"+id).hide();
	$("#share_post_btn-"+id).show();
	$("#share_post_btn_minus-"+id).hide();
}
/* This function for Showing the spinner on process */
function spinnerShow(target,opts) {
	$(target).fadeIn("slow");
	var spinner = new Spinner(opts).spin((target));
	return spinner;
	
}
/* This function hiding the spinner */
function spinnerHide(target,spinner) {
	if(spinner!=undefined) {
		$(target).fadeOut("slow");
		spinner.stop();
}
	
}
/* this function for Sharing the content on fb */
function sharingOnFb(href) {
	$.getScript('http://connect.facebook.net/en_UK/all.js',function() {
		FB.init({
			appId : SOCIAL_APP_IDS.FACEBOOK,
			frictionlessRequests : true,
			init : true,
			level : "debug",
			signedRequest : null,
			status : true,
			trace : false,
			viewMode : "website",
			autoRun : true,
			version : 'v2.0'
			
		});
		FB.ui({
			 display: 'popup',
			  method: 'share',
			  href: href,
			  
			}, function(response){});
	
});
}
/* This function for sending the app request through facebook */
function facebookAppRequest() {
	$.getScript('http://connect.facebook.net/en_UK/all.js',function() {
		FB.init({
			appId : SOCIAL_APP_IDS.FACEBOOK,
			frictionlessRequests : true,
			init : true,
			level : "debug",
			signedRequest : null,
			status : true,
			trace : false,
			viewMode : "website",
			autoRun : true,
			version : 'v2.0'
			
		});
	 FB.ui({
		    method: 'apprequests',
		    message: 'Sending App Request to Friends'
		  }, console.log.bind('send-to-many callback'));
	});
}


/* This function for loading the google script */
function googlePlusButtonScript() {
	(function() {
	    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	    po.src = 'https://apis.google.com/js/client:plusone.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  })();
}

/* This function for loading the facebook script  */
function fbLikeButtonScript() {
	(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=263648493818981&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
}
/* this function call when story slide and load comments of the current post*/
function onSlidePost() {
	target = document.getElementById("comments-container");
	  var spinner = spinnerShow(target,opts);
	 setTimeout(function() {
		 $("#post-comments").html("");
		 getComments(0,$(".crsl-active").attr('id'),target,spinner);
		 }, 1000);
}
function redirectOnLoginSuccess() {
	share_popup_close();
	window.location.href = URL.REDIRECT_AFTER_LOGIN;
}

$(function(){
	 googlePlusButtonScript();
	$(".g-invites").click(function(){
	var options = {
		    contenturl: 'www.fetaboo.com',
		    clientid:  SOCIAL_APP_IDS.GOOGLE_PLUS,
		    cookiepolicy: 'single_host_origin',
		    prefilltext: 'Sending invetation to User',
		    calltoactionlabel: 'INVITE',
		    calltoactionurl: '"http://developers.google.com/+/web/share/interactive?invite=true',
		  };
	gapi.interactivepost.render('g-div', options);

	});

$('#topMobileSlideshow, .bigSlideshow')
		.cycle({fx:'scrollLeft'});
		
			var offset = 220;
			var duration = 500;
			jQuery(window).scroll(function() {
				if (jQuery(this).scrollTop() > offset) {
					jQuery('.back-to-top').fadeIn(duration);
				} else {
					jQuery('.back-to-top').fadeOut(duration);
				}
			});
			
			jQuery('.back-to-top').click(function(event) {
				event.preventDefault();
				jQuery('html, body').animate({scrollTop: 0}, duration);
				return false;
			});
			
						
});

function gPlusLogin(id){
	 var target = document.getElementById(id);
		var spinner = spinnerShow(target,opts);
      gapi.auth.authorize({
          client_id: SOCIAL_APP_IDS.GOOGLE_PLUS,
          immediate: false,
          response_type: "token",
          scope: "https://www.googleapis.com/auth/plus.profile.emails.read",
          request_visible_actions: "https://schemas.google.com/AddActivity"
      }, function(e) {
          // callback
      	 gapi.client.load('plus','v1', function(){
          var request = gapi.client.plus.people.get( {'userId' : 'me'} );
          request.execute( function(profile) {
          	socialLogin.emailId =profile.emails[0].value;
				socialLogin.name =profile.displayName;
				socialLogin.socialGplusLink =profile.url;
				socialLogin.socialAvatar = profile.image.url;
				if(profile.gender == "male") {
					socialLogin.gender = "MALE";
					
				} else {
					socialLogin.gender = "FEMALE";
				}
				if(socialLogin.emailId!=undefined && socialLogin.name!=undefined) {
				ajaxCallingGET(METHOD.POST,JSON.stringify(socialLogin),URL.SOCIAL_LOGIN, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SOCIAL_LOGIN,target,spinner);
				}
				spinnerHide(target,spinner);
            });
      	 });
      });
}

function fbLogin(id) {
	 var target = document.getElementById(id);
		var spinner = spinnerShow(target,opts);
		$.ajaxSetup({
			cache : true
		});
		$.getScript('http://connect.facebook.net/en_UK/all.js',function() {
							FB.init({
								appId :  SOCIAL_APP_IDS.FACEBOOK,
								frictionlessRequests : true,
								init : true,
								level : "debug",
								signedRequest : null,
								status : true,
								trace : false,
								viewMode : "website",
								autoRun : true,
								version : 'v2.0'
							});
							
							 FB.login(function(response) {
									FB.api({
									      method: 'fql.query',
									      query: 'SELECT name, pic_square FROM user WHERE uid='+FB.getUserID()
									    },function(response) {
									    	socialLogin.socialAvatar = response[0].pic_square;
									    	
									    });
									
								FB.api('/me',function(response) {
									socialLogin.emailId = response.email;
									socialLogin.name = response.name;
									socialLogin.socialFBLink =response.link;
									if(response.gender == "male") {
										socialLogin.gender = "MALE";
										
									} else {
										socialLogin.gender = "FEMALE";
									}
									
									if(socialLogin.emailId!=undefined && socialLogin.name!=undefined) {
									ajaxCallingGET(METHOD.POST,JSON.stringify(socialLogin),URL.SOCIAL_LOGIN, WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SOCIAL_LOGIN,target,spinner);
									}
									spinnerHide(target,spinner);
								  });								
	},{ scope: 'email' });
  
});
}
function sharingOnGooglePlus(url) {
	javascript:window.open("https://plus.google.com/share?url="+url,
			  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;
}
function sharingOnTumbler(url) {
	javascript:window.open("http://www.tumblr.com/share/link?url=="+url,
			  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;
}
// JavaScript Document

// ------------- Header Show Hide ---------------------- //
var isMobileDevice = false;
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
					isMobileDevice = true;
				//	($(this).scrollTop()>1)
				} else {
					isMobileDevice = true
					$(window).scroll(function() {
					
						if ($(this).scrollTop()>0)
						 {
							$('header').fadeOut(40);
						 }
						else
						 {
						  $('header').fadeIn(40);
						 }
					});
				}
				
				
				
// -----  Index mobile image slide ----- //
 $(document).ready(function(){
	$('#topMobileSlideshow, .bigSlideshow')
		.cycle({fx:'scrollLeft'});
  });
  
  
	
	// -----  Story Slider ----- //
	$(function(){
  $('.crsl-items').carousel({
    visible: 1,
    itemMinWidth: 180,
    itemEqualHeight: 370,
    itemMargin: 9,
  });
  
  $("a[href=#]").on('click', function(e) {
    e.preventDefault();
  });
});






	// ----- Story List Responsive Categories ----- //

	$(document).ready(function(){
		$('#menu').slicknav();
	});
	
	
	function dropdown() {
		$(".responsive_cat") .toggle();	
	}
	function dropdownhide() {
		$(".responsive_cat") .hide();
	}
	

	

	
	// -------------- Story List Story Page Tab ------------------------ //			
		// Wait until the DOM has loaded before querying the document
			$(document).ready(function(){
				$('ul.tabs').each(function(){
					// For each set of tabs, we want to keep track of
					// which tab is active and it's associated content
					var $active, $content, $links = $(this).find('a');

					// If the location.hash matches one of the links, use that as the active tab.
					// If no match is found, use the first link as the initial active tab.
					$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
					$active.addClass('active');

					$content = $($active[0].hash);

					// Hide the remaining content
					$links.not($active).each(function () {
						$(this.hash).hide();
					});

					// Bind the click event handler
					$(this).on('click', 'a', function(e){
						// Make the old tab inactive.
						$active.removeClass('active');
						$content.hide();

						// Update the variables with the new link and content
						$active = $(this);
						$content = $(this.hash);

						// Make the tab active.
						$active.addClass('active');
						$content.show();

						// Prevent the anchor's default click action
						e.preventDefault();
					});
				});
			});

	


	

var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
function signInValidation() {
	if($(".signin-email").val()==null || $(".signin-email").val()=="" || reg.test($(".signin-email").val())==false) {
		$(".signin-email").css({background:"#FF6347"});
		$("#sign-error").html("Email is not valid");
		return false;
	} else if($("#sign-password").val()==null || $("#sign-password").val()=="") {
		$("#sign-password").css({background:"#FF6347"});
		$("#sign-error").html("Password is not valid");
		return false;
	} else if($("#sign-re-password").val()==null || $("#sign-re-password").val()=="") {
		$("#sign-re-password").css({background:"#FF6347"});
		$("#sign-error").html("Confirm-password is not valid");
		return false;
	} else if($("#sign-re-password").val()!= $("#sign-password").val()) {
		$("#sign-password").css({background:"#FF6347"});
		$("#sign-re-password").css({background:"#FF6347"});
		$("#sign-error").html("Password are not matched");
		return false;
	}
	
	return true;
}

function logInValidation() {
	if($(".login-email").val()==null || $(".login-email").val()=="" || reg.test($(".login-email").val())==false) {
		$(".login-email").css({background:"#FF6347"});
		$("#login-error").html("Email is not valid");
		return false;
	} else if($(".login-password").val()==null || $(".login-password").val()=="") {
		$(".login-password").css({background:"#FF6347"});
		$("#login-error").html("Password is not valid");
		return false;
	}
	return true;
}


	
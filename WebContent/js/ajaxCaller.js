/*
function ajaxCallingGET(method,data,url, type, target, spinner) {
	$.ajax({
				type :method,
				cache : false,
				url : url,
				data:data,
				dataType : "json",
				success : function(response) {
					
				},
				error : function(data) {
						
				},
				complete : function(response) {
					switch (type) {
					case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POST_CATEGORY_COUNT_DATA_PAYLOAD: {
						categoryCount(JSON.parse(response.responseText));
						break;
					}
					case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DASHBOARD_DATA_PAYLOAD: {
						onScrollPostDataPayload(response.responseText,target,spinner);
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SHARE_PRIVATE_POST: {
						
						if(response.responseText == "success") {
							successMessage("Your Post Has been shared privatly Through Email");
							} else {
								errorMessage("Your Post could not share Through email ");
							}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.LIKE_SHARE: {
						if(response.responseText != "") {
						if(data.type=='like') {
							$("#"+data.type+"-"+data.postId).html(JSON.parse(response.responseText).like);
							$("#post-like").html(JSON.parse(response.responseText).like);
							$("#"+data.postId+"-"+data.type).toggleClass("isliked");
						} else {
							$("#"+data.type+"-"+data.postId).html(JSON.parse(response.responseText).share);
							//successMessage("You Have Successfully Shared this Story ");
						}
						} 
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.ADD_COMMENT : {
						spinnerHide(target,spinner);
						addComment(response.responseText,data.comment,target,spinner);
					
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.GET_COMMENT : {
						loadComment(response.responseText,target,spinner);
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.GET_SIMILER_POST : {
						loadSimilerPost(response.responseText,target,spinner);
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.REPORT_USER : {
						reportUserCallBack(response.responseText);
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.BLOCK_USER : {
						blockUserCallBack(response.responseText);
						
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SOCIAL_LOGIN : {
						spinnerHide(target,spinner);
						if(response.responseText=="success") {
							redirectOnLoginSuccess();
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.HIDE_POST : {
						if(response.responseText!="") {
							hidePostCallback(data);
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_COMMENTED : {
						 spinnerHide(target,spinner);
						if(response.responseText!="") {
							mostAccessedPost(response.responseText,"tab-2");
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_SHARED : {
						 spinnerHide(target,spinner);
						if(response.responseText!="") {
							mostAccessedPost(response.responseText,"tab-3");
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_VIEWED : {
						 spinnerHide(target,spinner);
						if(response.responseText!="") {
							mostAccessedPost(response.responseText,"tab-1");
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POPULAR_TAG : {
						spinnerHide(target,spinner);
						if(response.responseText!="") {
							popularTag(response.responseText);
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.VIEW : {
						if(response.responseText !="") {
							addView(JSON.parse(response.responseText).view);
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DELETE_COMMENT : {
						if(response.responseText!="") {
							$("#"+data.commentId).fadeOut(500);
							stories[currentPostId].comments  = stories[currentPostId].comments -1;
							$("#post-comment").html(stories[currentPostId].comments);
							$("#comment-"+currentPostId).html(stories[currentPostId].comments);
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MY_POST : {
						spinnerHide(target,spinner);
						if(response.responseText!="") {
							onRequestChangeDataReplicateOnDashBoard(response.responseText);
						}
						break;
					} case  WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SEARCH_CATEGORY : {
						spinnerHide(target,spinner);
						if(response.responseText!="") {
							onRequestChangeDataReplicateOnDashBoard(response.responseText);
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POST_BY_TAG : {
						spinnerHide(target,spinner);
						if(response.responseText!="") {
							onRequestChangeDataReplicateOnDashBoard(response.responseText);
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.UPDATE_POST : {
						if(response.responseText!="") {
							window.location.href = URL.REDIRECT_ON_EDIT_POST;
						}
						break;
					} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DELETE_POST : {
						if(response.responseText!="") {
							$("#delete-story").hide(100);
							$("#story_popup").hide(200);
							$("#"+JSON.parse(data.jsonData).postId).fadeOut(500);
						}
					}
					}// Switch close
					
				}
			}); // end of $.ajax
}*/

function ajaxCallingGET(method,data,url, type, target, spinner) {
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  } else
	  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
	xmlhttp.timeout = 10000;
	xmlhttp.open(method,url,true);
	xmlhttp.setRequestHeader('Content-type','application/json; charset=utf-8');
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4) {
		  if(xmlhttp.status==200) {
			
				switch (type) {
				case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POST_CATEGORY_COUNT_DATA_PAYLOAD: {
					categoryCount(JSON.parse(xmlhttp.responseText));
					break;
				}
				case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DASHBOARD_DATA_PAYLOAD: {
					onScrollPostDataPayload(xmlhttp.responseText,target,spinner);
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SHARE_PRIVATE_POST: {
					if(xmlhttp.responseText == "success") {
						successMessage("Your Post Has been shared privatly Through Email");
						} else {
							errorMessage("Your Post could not share Through email ");
						}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.LIKE_SHARE: {
					if(xmlhttp.responseText != "") {
						data = JSON.parse(data);
					if(data.type=='like') {
						//$("#"+data.type+"-"+data.postId).html(JSON.parse(xmlhttp.responseText).like);
						//$("#post-like").html(JSON.parse(xmlhttp.responseText).like);
					} else {
						$("#"+data.type+"-"+data.postId).html(JSON.parse(xmlhttp.responseText).share);
						//successMessage("You Have Successfully Shared this Story ");
					}
					} 
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.ADD_COMMENT : {
					spinnerHide(target,spinner);
					addComment(xmlhttp.responseText,JSON.parse(data).comment,target,spinner);
				
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.GET_COMMENT : {
					loadComment(xmlhttp.responseText,target,spinner);
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.GET_SIMILER_POST : {
					loadSimilerPost(xmlhttp.responseText,target,spinner);
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.REPORT_USER : {
					reportUserCallBack(xmlhttp.responseText);
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.BLOCK_USER : {
					blockUserCallBack(xmlhttp.responseText);
					
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SOCIAL_LOGIN : {
					spinnerHide(target,spinner);
					if(xmlhttp.responseText=="success") {
						redirectOnLoginSuccess();
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.HIDE_POST : {
					if(xmlhttp.responseText!="") {
						hidePostCallback(data);
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_COMMENTED : {
					 spinnerHide(target,spinner);
					if(xmlhttp.responseText!="") {
						mostAccessedPost(xmlhttp.responseText,"tab-2");
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_SHARED : {
					 spinnerHide(target,spinner);
					if(xmlhttp.responseText!="") {
						mostAccessedPost(xmlhttp.responseText,"tab-3");
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MOST_VIEWED : {
					 spinnerHide(target,spinner);
					if(xmlhttp.responseText!="") {
						mostAccessedPost(xmlhttp.responseText,"tab-1");
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POPULAR_TAG : {
					spinnerHide(target,spinner);
					if(xmlhttp.responseText!="") {
						popularTag(xmlhttp.responseText);
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.VIEW : {
					if(xmlhttp.responseText !="") {
						addView(JSON.parse(xmlhttp.responseText).view);
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DELETE_COMMENT : {
					data = JSON.parse(data);
					if(xmlhttp.responseText!="") {
						$("#"+data.commentDate).fadeOut(500);
						$("#"+data.commentDate).remove();
						if(stories[currentPostId].comments !=0) {
						stories[currentPostId].comments  = stories[currentPostId].comments -1;
						$("#post-comment").html(stories[currentPostId].comments);
						$("#comment-"+currentPostId).html(stories[currentPostId].comments);
						}
					} else {
						$("#delete-"+data.commentDate).show();
					}
					
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.MY_POST : {
					spinnerHide(target,spinner);
					if(xmlhttp.responseText!="") {
						onRequestChangeDataReplicateOnDashBoard(xmlhttp.responseText);
					}
					break;
				} case  WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.SEARCH_CATEGORY : {
					spinnerHide(target,spinner);
					if(xmlhttp.responseText!="") {
						onRequestChangeDataReplicateOnDashBoard(xmlhttp.responseText);
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.POST_BY_TAG : {
					spinnerHide(target,spinner);
					if(xmlhttp.responseText!="") {
						onRequestChangeDataReplicateOnDashBoard(xmlhttp.responseText);
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.UPDATE_POST : {
					if(xmlhttp.responseText!="") {
						window.location.href = URL.REDIRECT_ON_EDIT_POST;
					}
					break;
				} case WEB_SERVICES_DATA_LOAD_METHOD_CONSTANTS.DELETE_POST : {
					if(xmlhttp.responseText!="") {
						$("#delete-story").hide(100);
						$("#story_popup").hide(200);
						$("#"+JSON.parse(data).postId).fadeOut(500);
						$("#"+JSON.parse(data).postId).remove();
					}
				}
				}// Switch close
		  }
	    }
	}
	xmlhttp.send(data);
}


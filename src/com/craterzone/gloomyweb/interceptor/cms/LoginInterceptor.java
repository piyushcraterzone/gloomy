package com.craterzone.gloomyweb.interceptor.cms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.gloomyweb.controller.util.CommonUtil;
import com.craterzone.gloomyweb.service.UserService;

public class LoginInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse response,
			Object arg2, ModelAndView arg3) throws Exception {
		response.setHeader("Cache-control", "no-store");
		//response.setHeader("Cache-control", "max-age=4492800, public");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", -1);
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2) throws Exception {
		HttpSession session = request.getSession(false);
		if (session == null) {
			response.sendRedirect("dashboard.htm");
			return false;
		}
		String str = (String) session.getAttribute("userName");
		if (str == null) {
			response.sendRedirect("dashboard.htm");
			return false;
		}
		return true;
	}
	
}

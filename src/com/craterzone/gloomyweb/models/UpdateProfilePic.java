package com.craterzone.gloomyweb.models;

import javax.xml.bind.annotation.XmlRootElement;

import com.craterzone.gloomyweb.models.Image;

@XmlRootElement
public class UpdateProfilePic {

	private String userId;
	private Image image;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

}

package com.craterzone.gloomyweb.models;

import com.craterzone.gloomyweb.enums.DeviceType;

public class UpdateDeviceToken {

	private String userId;
	private String deviceToken;
	private DeviceType deviceType;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public DeviceType getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
}

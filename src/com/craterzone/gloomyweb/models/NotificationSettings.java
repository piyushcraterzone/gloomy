package com.craterzone.gloomyweb.models;

import com.craterzone.gloomyweb.utils.common.ConstantsUtil;

public class NotificationSettings {

	private boolean didYouKnow = true;
	private boolean interestTaboo = true;
	private boolean nearBy = true;
	private boolean newComment = true;
	private String toneSetting = ConstantsUtil.DEFAULT_NOTIFICATION_RING_TYPE;
	private boolean pushNotification = true;

	public NotificationSettings() {

	}

	public NotificationSettings(boolean pushNotification) {

		if (pushNotification == false) {
			this.didYouKnow = false;
			this.interestTaboo = false;
			this.nearBy = false;
			this.newComment = false;
			this.pushNotification = false;
		}
	}

	public boolean isDidYouKnow() {
		return didYouKnow;
	}

	public void setDidYouKnow(boolean didYouKnow) {
		this.didYouKnow = didYouKnow;
	}

	public boolean isInterestTaboo() {
		return interestTaboo;
	}

	public void setInterestTaboo(boolean interestTaboo) {
		this.interestTaboo = interestTaboo;
	}

	public boolean isNearBy() {
		return nearBy;
	}

	public void setNearBy(boolean nearBy) {
		this.nearBy = nearBy;
	}

	public boolean isNewComment() {
		return newComment;
	}

	public void setNewComment(boolean newComment) {
		this.newComment = newComment;
	}

	public String getToneSetting() {
		return toneSetting;
	}

	public void setToneSetting(String toneSetting) {
		this.toneSetting = toneSetting;
	}

	public boolean isPushNotification() {
		return pushNotification;
	}

	public void setPushNotification(boolean pushNotification) {
		this.pushNotification = pushNotification;
	}
}

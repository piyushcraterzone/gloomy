package com.craterzone.gloomyweb.models;

public class ReportUser {

	private String userId;
	private String abuseUserId;
	private String cause;
	
	public ReportUser() {
		
	}
	
	public String getUserId() {
		return userId;
	}
	public String getAbuseUserId() {
		return abuseUserId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setAbuseUserId(String abuseUserId) {
		this.abuseUserId = abuseUserId;
	}
	
	public String getCause() {
		return cause;
	}
}

package com.craterzone.gloomyweb.models;

import com.craterzone.gloomyweb.enums.UserStatus;
import com.craterzone.gloomyweb.enums.constants.user.Gender;

public class UpdateProfile {

	private String userId;
	private Gender gender;
	private String userDisplayName;
	private long dob;
	private UserStatus userStatus = UserStatus.PUBLIC;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getUserDisplayName() {
		return userDisplayName;
	}

	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}

	public long getDob() {
		return dob;
	}
	
	public void setDob(long dob) {
		this.dob = dob;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}
	
}

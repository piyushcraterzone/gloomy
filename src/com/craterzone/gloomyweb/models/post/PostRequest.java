package com.craterzone.gloomyweb.models.post;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.craterzone.gloomyweb.enums.constants.post.CategoryType;
import com.craterzone.gloomyweb.models.Image;
import com.craterzone.gloomyweb.models.Location;

public class PostRequest {

	private String userId;
	private String caption;
	private String subHeading;
	private String postDetail;
	private CategoryType categoryType;
	private int colorCode;
	private Set<String> tags = new HashSet<String>();
	private Image image;
	private Location location;
	private List<String> emailId;
	
	public PostRequest(){}
	
	public PostRequest(String userId,String caption,String subHeading,String postDetail,CategoryType categoryType,int colorCode,Set<String> tags,Image image) {
		
		this.userId = userId;
		this.caption = caption;
		this.subHeading = subHeading;
		this.postDetail = postDetail;
		this.categoryType =categoryType;
		this.colorCode = colorCode;
		this.tags = tags;
		this.image = image;
	}
	
	public List<String> getEmailId() {
		return emailId;
	}

	public void setEmailId(List<String> emailId) {
		this.emailId = emailId;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getUserId() {
		return userId;
	}
	public String getCaption() {
		return caption;
	}
	public String getSubHeading() {
		return subHeading;
	}
	public String getPostDetail() {
		return postDetail;
	}
	public CategoryType getCategoryType() {
		return categoryType;
	}
	public int getColorCode() {
		return colorCode;
	}
	public Set<String> getTags() {
		return tags;
	}
	public Image getImage() {
		return image;
	}

	public Location getLocation() {
		return location;
	}
	
	
}

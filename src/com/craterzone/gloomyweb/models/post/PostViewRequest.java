package com.craterzone.gloomyweb.models.post;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Piyush gupta
 * 
 */
@XmlRootElement
public class PostViewRequest {

	private String postId;
	private String ipAddress;
	
	public PostViewRequest(String postId,String ipAddress) {
		this.postId = postId;
		this.ipAddress = ipAddress;
	}
	public String getPostId() {
		return postId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	
}

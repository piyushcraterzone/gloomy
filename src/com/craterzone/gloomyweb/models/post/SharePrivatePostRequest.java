package com.craterzone.gloomyweb.models.post;

import java.util.Set;

public class SharePrivatePostRequest {

	private String userId;
	private String postId;
	private Set<String> emailIds;
	private String postURL;
	
	public SharePrivatePostRequest(String userId,String postId,Set<String> emailIds,String postURL) {
		this.userId = userId;
		this.postId = postId;
		this.emailIds = emailIds;
		this.postURL = postURL;
	}
	public SharePrivatePostRequest() {
		
	}
	public String getPostURL() {
		return postURL;
	}
	public void setPostURL(String postURL) {
		this.postURL = postURL;
	}
	public String getUserId() {
		return userId;
	}
	public String getPostId() {
		return postId;
	}
	public Set<String> getEmailIds() {
		return emailIds;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public void setEmailIds(Set<String> emailIds) {
		this.emailIds = emailIds;
	}
}

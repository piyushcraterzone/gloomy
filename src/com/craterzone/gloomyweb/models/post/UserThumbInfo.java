package com.craterzone.gloomyweb.models.post;

public class UserThumbInfo {

	private String lresURL;
	private String userDisplayName;
	private String userId;
	
	public UserThumbInfo() {
		
	}
	public UserThumbInfo(String userDisplayName,String lresURL) {
		this.userDisplayName=userDisplayName;
		this.lresURL=lresURL;
	}
	public String getLresURL() {
		return lresURL;
	}
	public void setLresURL(String lresURL) {
		this.lresURL = lresURL;
	}
	public String getUserDisplayName() {
		return userDisplayName;
	}
	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}

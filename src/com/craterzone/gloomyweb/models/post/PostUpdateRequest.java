package com.craterzone.gloomyweb.models.post;

import java.util.Set;

public class PostUpdateRequest {

	private String postId;
	private String userId;
	private String caption;
	private String postDetail;
	private Set<String> tags;
	
	
	public PostUpdateRequest(String postId, String userId, String caption,
			String postDetail, Set<String> tags) {
		this.postId = postId;
		this.userId = userId;
		this.caption = caption;
		this.postDetail = postDetail;
		this.tags = tags;
	}
	public String getPostId() {
		return postId;
	}
	public String getUserId() {
		return userId;
	}
	public String getCaption() {
		return caption;
	}
	public String getPostDetail() {
		return postDetail;
	}
	public Set<String> getTags() {
		return tags;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public void setPostDetail(String postDetail) {
		this.postDetail = postDetail;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	
	
}

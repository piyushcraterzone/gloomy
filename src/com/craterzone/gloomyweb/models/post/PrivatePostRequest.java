package com.craterzone.gloomyweb.models.post;

import java.util.Set;

import com.craterzone.gloomyweb.models.post.StoryAdd;

public class PrivatePostRequest extends StoryAdd {

	private Set<String> receiverEmailIds;

	public Set<String> getReceiverEmailIds() {
		return receiverEmailIds;
	}
	public void setReceiverEmailIds(Set<String> receiverEmailIds) {
		this.receiverEmailIds = receiverEmailIds;
	}

}

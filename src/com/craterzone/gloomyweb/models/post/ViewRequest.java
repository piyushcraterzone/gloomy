package com.craterzone.gloomyweb.models.post;


/**
 * 
 * @author Piyush gupta
 * 
 */
public class ViewRequest {

	private String postId;
	private String ipAddress;
	
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
	
}

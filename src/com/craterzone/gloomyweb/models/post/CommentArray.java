package com.craterzone.gloomyweb.models.post;

import java.util.List;

import com.craterzone.gloomyweb.models.post.CommentResponse;

public class CommentArray {
	
	List<CommentResponse> commentList;

	//constructor 
	public CommentArray(List<CommentResponse> commentList) {
		 this.commentList = commentList;
	}

	public List<CommentResponse> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<CommentResponse> commentList) {
		this.commentList = commentList;
	}

}

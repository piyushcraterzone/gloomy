package com.craterzone.gloomyweb.models.post;

/**
 * 
 * @author Piyush Gupta
 *
 */
public class PostShareLikeRequest {

	private String postId;
	private String userId;
	private String type;
	
	public PostShareLikeRequest() {
		
	}
	public PostShareLikeRequest(String postId,String userId) {
		this.postId = postId;
		this.userId = userId;
	}
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}

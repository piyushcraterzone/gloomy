package com.craterzone.gloomyweb.models.post;


public class CommentRequest {
	
	private String postId;
	private String comment;
	private String userId;
	
	public CommentRequest() {
		
	}
	public CommentRequest(String postId,String comment,String userId) {
		this.postId = postId;
		this.userId = userId;
		this.comment = comment;
	}
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}

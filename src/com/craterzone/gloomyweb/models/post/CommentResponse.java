package com.craterzone.gloomyweb.models.post;

import com.craterzone.gloomyweb.models.post.UserThumbInfo;



public class CommentResponse {

	private String message;
	private UserThumbInfo userThumbInfo;
	private long commentDate;
	
	public String getMessage() {
		return message;
	}
	public UserThumbInfo getUserThumbInfo() {
		return userThumbInfo;
	}
	public long getCommentDate() {
		return commentDate;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setUserThumbInfo(UserThumbInfo userThumbInfo) {
		this.userThumbInfo = userThumbInfo;
	}
	public void setCommentDate(long commentDate) {
		this.commentDate = commentDate;
	}
	
	
	
	
}

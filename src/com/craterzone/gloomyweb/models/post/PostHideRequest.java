package com.craterzone.gloomyweb.models.post;

public class PostHideRequest {

	private String userId;
	private String postId;
	
	public PostHideRequest() {
	}
	public String getUserId() {
		return userId;
	}
	public String getPostId() {
		return postId;
	}
	
}

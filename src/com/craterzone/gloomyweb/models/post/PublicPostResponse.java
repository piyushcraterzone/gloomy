package com.craterzone.gloomyweb.models.post;

import java.util.List;
import java.util.Set;

import com.craterzone.gloomyweb.models.post.PostResponse;
import com.craterzone.gloomyweb.models.post.UserThumbInfo;
import com.craterzone.gloomyweb.enums.constants.post.CategoryType;
import com.craterzone.gloomyweb.models.Location;

public class PublicPostResponse extends PostResponse {
	
	private String postId;
	private List<UserThumbInfo> likes;
	private boolean picOfDay;
	private int rank;
	private List<UserThumbInfo> shares;
	private long viewCounter;
	private long comments;
	private UserThumbInfo userThumbInfo;
	private String caption;
	private String subHeading;
	private String hresURL;
	private String lresURL;
	private Location location;
	private CategoryType categoryType;
	private long postDate;
	private String postDetail;
	private Set<String> tags ;

	
	public PublicPostResponse() {
		
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public List<UserThumbInfo> getLikes() {
		return likes;
	}

	public void setLikes(List<UserThumbInfo> likes) {
		this.likes = likes;
	}

	public boolean isPicOfDay() {
		return picOfDay;
	}

	public void setPicOfDay(boolean picOfDay) {
		this.picOfDay = picOfDay;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public List<UserThumbInfo> getShares() {
		return shares;
	}

	public void setShares(List<UserThumbInfo> shares) {
		this.shares = shares;
	}

	public long getViewCounter() {
		return viewCounter;
	}

	public void setViewCounter(long viewCounter) {
		this.viewCounter = viewCounter;
	}

	public long getComments() {
		return comments;
	}

	public void setComments(long comments) {
		this.comments = comments;
	}
	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getSubHeading() {
		return subHeading;
	}

	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}

	public String getHresURL() {
		return hresURL;
	}

	public void setHresURL(String hresURL) {
		this.hresURL = hresURL;
	}

	public String getLresURL() {
		return lresURL;
	}

	public void setLresURL(String lresURL) {
		this.lresURL = lresURL;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public CategoryType getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(CategoryType categoryType) {
		this.categoryType = categoryType;
	}
	public long getPostDate() {
		return postDate;
	}

	public void setPostDate(long postDate) {
		this.postDate = postDate;
	}

	public String getPostDetail() {
		return postDetail;
	}

	public void setPostDetail(String postDetail) {
		this.postDetail = postDetail;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public UserThumbInfo getUserThumbInfo() {
		return userThumbInfo;
	}
	public void setUserThumbInfo(UserThumbInfo userThumbInfo) {
		this.userThumbInfo = userThumbInfo;
	}
}

package com.craterzone.gloomyweb.models.post;

public class PostDeleteRequest {

	private String postId;
	private String userId;
	public String getPostId() {
		return postId;
	}
	public String getUserId() {
		return userId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}

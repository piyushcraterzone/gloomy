package com.craterzone.gloomyweb.models.post;

import java.util.List;

public class PostArray {

	List<PublicPostResponse> postList;

	//constructor 
	public PostArray() {
		
	}
	public List<PublicPostResponse> getPostList() {
		return postList;
	}

	public void setPostList(List<PublicPostResponse> postList) {
		this.postList = postList;
	}
}

package com.craterzone.gloomyweb.models.post;

import com.craterzone.gloomyweb.models.post.StoryAdd;
import com.craterzone.gloomyweb.models.Location;

public class PublicPostRequest extends StoryAdd {

	private boolean pickOfDay = false; // by Default false;
	private int rank;
	private Location location;

	public boolean isPickOfDay() {
		return pickOfDay;
	}

	public void setPickOfDay(boolean pickOfDay) {
		this.pickOfDay = pickOfDay;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
}

package com.craterzone.gloomyweb.models.post;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.craterzone.gloomyweb.enums.constants.post.CategoryType;
import com.craterzone.gloomyweb.models.Image;

public class StoryAdd {

	private String userId ;
	private String caption;
	private String subHeading;
	private String postDetail;
	private CategoryType categoryType = CategoryType.GLOOMY;
	private int colorCode;
	private List<String> tags;
	private Image image;
	private MultipartFile multipartFile;
	private String postId; 
	private String lresURL;
	private List<String> emailId;

	
	public StoryAdd()  {
		
	}
	public StoryAdd(String postId , String userId , String caption , String postDetail , CategoryType categoryType , List<String> tags , String lresURL ) {
		this.postId = postId;
		this.caption = caption;
		this.categoryType = categoryType;
		this.userId = userId;
		this.postDetail = postDetail;
		this.tags = tags;
		this.lresURL = lresURL;
	}
	public String getLresURL() {
		return lresURL;
	}
	public String getPostId() {
		return postId;
	}
	
	public void setPostId(String postId) {
		this.postId = postId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getSubHeading() {
		return subHeading;
	}

	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}

	public String getPostDetail() {
		return postDetail;
	}

	public void setPostDetail(String postDetail) {
		this.postDetail = postDetail;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	public int getColorCode() {
		return colorCode;
	}

	public void setColorCode(int colorCode) {
		this.colorCode = colorCode;
	}

	public CategoryType getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(CategoryType categoryType) {
		this.categoryType = categoryType;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}
	public List<String> getEmailId() {
		return emailId;
	}
	public void setEmailId(List<String> emailId) {
		this.emailId = emailId;
	}

	
}

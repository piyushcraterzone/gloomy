package com.craterzone.gloomyweb.models.post;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Piyush Gupta
 *
 */
@XmlRootElement
public class CommentDelete {

	private long commentDate;
	private String postId;
	private String userId;
	
	public CommentDelete() {
	}
	
	public long getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(long commentDate) {
		this.commentDate = commentDate;
	}

	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
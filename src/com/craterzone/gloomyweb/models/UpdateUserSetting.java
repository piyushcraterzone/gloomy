package com.craterzone.gloomyweb.models;

import com.craterzone.gloomyweb.models.NotificationSettings;

public class UpdateUserSetting {
	private String userId;
	private NotificationSettings notificationSettings;

	public UpdateUserSetting(){
		notificationSettings = new NotificationSettings();
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public NotificationSettings getNotificationSettings() {
		return notificationSettings;
	}

	public void setNotificationSettings(NotificationSettings notificationSettings) {
		this.notificationSettings = notificationSettings;
	}
}

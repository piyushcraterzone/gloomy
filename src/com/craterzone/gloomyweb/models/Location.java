package com.craterzone.gloomyweb.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Location {

	private double lat;
	private double lng;
	
	public Location() {}
	
	public Location(double lat,double lng) {
		this.lat = lat;
		this.lng = lng;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double latitude) {
		this.lat = latitude;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double longitude) {
		this.lng = longitude;
	}
	
	
	
}

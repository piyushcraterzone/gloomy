package com.craterzone.gloomyweb.models;

import com.craterzone.gloomyweb.models.Image;


public class UpdateAvatar {

	private String userId;
	private Image image;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
}

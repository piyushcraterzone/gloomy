package com.craterzone.gloomyweb.models;

import com.craterzone.gloomyweb.models.Location;

public class UpdateLocation {

	private String userId;
	private Location location;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
}

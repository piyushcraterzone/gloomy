package com.craterzone.gloomyweb.models;

/**
 * This Model is Using for Login And Signup both
 * @author Piyush
 *
 */

public class Login {

	private String emailId;
	private String password;

	public Login(String emailId, String password) {
		this.emailId = emailId;
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
}

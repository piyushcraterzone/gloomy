package com.craterzone.gloomyweb.models;

import java.util.Set;

import com.craterzone.gloomyweb.enums.UserStatus;
import com.craterzone.gloomyweb.enums.constants.user.Gender;

public class UserResponse {

	private String userId;
	private String lresURL ;
	private String hresURL;
	private String userDisplayName;
	private Gender gender;
	private UserStatus userStatus;
	private Set<String> tags;
	private NotificationSettings notificationSettings;
	private String token;

	//setter and getter
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public NotificationSettings getNotificationSettings() {
		return notificationSettings;
	}

	public void setNotificationSettings(NotificationSettings notificationSettings) {
		this.notificationSettings = notificationSettings;
	}
	public String getLresURL() {
		return lresURL;
	}
	public void setLresURL(String lresURL) {
		this.lresURL = lresURL;
	}

	public String getHresURL() {
		return hresURL;
	}

	public void setHresURL(String hresURL) {
		this.hresURL = hresURL;
	}

	public String getUserDisplayName() {
		return userDisplayName;
	}

	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

}

package com.craterzone.gloomyweb.models;

import com.craterzone.gloomyweb.enums.constants.user.Gender;



public class SocialLogin {
	
	private String emailId;
	private String name = "Unknown";
	private String cc;
	private Gender gender = Gender.FEMALE.getGender();
	private String socialFBLink = "";
	private String socialGplusLink = "";
	private String socialAvatar;

	public SocialLogin(String emailId,String name,String cc,Gender gender,String socialFBLink,String socialGplusLink,String socialAvatar) {
		this.emailId = emailId;
		this.name = name;
		this.cc = cc;
		this.gender = gender;
		this.socialFBLink = socialFBLink;
		this.socialGplusLink = socialGplusLink;
		this.socialAvatar = socialAvatar;
	}
	public SocialLogin(String emailId,String socialAvatar) {
		this.emailId = emailId;
		this.socialAvatar = socialAvatar;
	}
	public SocialLogin() {
		
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public void setSocialFBLink(String socialFBLink) {
		this.socialFBLink = socialFBLink;
	}
	public void setSocialGplusLink(String socialGplusLink) {
		this.socialGplusLink = socialGplusLink;
	}
	public void setSocialAvatar(String socialAvatar) {
		this.socialAvatar = socialAvatar;
	}
	
	public String getSocialAvatar() {
		return socialAvatar;
	}
	public String getEmailId() {
		return emailId;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCc() {
		return cc;
	}
	
	public String getSocialFBLink() {
		return socialFBLink;
	}
	
	public String getSocialGplusLink() {
		return socialGplusLink;
	}
	
	public Gender getGender() {
		return gender;
	}
	
	
	
}

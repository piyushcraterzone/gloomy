package com.craterzone.gloomyweb.models;

public class ContactUs {

	private String name;
	private String emailId;
	private String subject;
	private String message;
	public String getName() {
		return name;
	}
	public String getEmailId() {
		return emailId;
	}
	public String getSubject() {
		return subject;
	}
	public String getMessage() {
		return message;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

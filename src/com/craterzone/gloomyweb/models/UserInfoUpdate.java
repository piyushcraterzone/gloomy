package com.craterzone.gloomyweb.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserInfoUpdate {

	private String userId;
	private String gender; 
	private String userDisplayName;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserDisplayName() {
		return userDisplayName;
	}
	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
}

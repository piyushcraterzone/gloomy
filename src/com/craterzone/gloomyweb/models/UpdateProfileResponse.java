package com.craterzone.gloomyweb.models;

public class UpdateProfileResponse {

	private String userId;
	private String hresURL;
	private String lresURL;

	public UpdateProfileResponse() {

	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getHresURL() {
		return hresURL;
	}

	public void setHresURL(String hresURL) {
		this.hresURL = hresURL;
	}

	public String getLresURL() {
		return lresURL;
	}

	public void setLresURL(String lresURL) {
		this.lresURL = lresURL;
	}
}

package com.craterzone.gloomyweb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.gloomyweb.models.ContactUs;
import com.craterzone.gloomyweb.models.UpdateUserSetting;

@Controller
public class StaticPageController {

	
	/**
	 * This method will serve request for About us page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/aboutus.htm")
	public ModelAndView aboutPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("aboutus");
		model.addObject("title","About Us");
		model.addObject("tagId","about-us");
		return model;
	}

	/**
	 * This Method will serve the request for Contact page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/contact.htm")
	public ModelAndView contactPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("contact");
		model.addObject("contact", new ContactUs());
		model.addObject("title","Contact Us");
		model.addObject("tagId","contact-us");
		return model;
	}

	@RequestMapping(value="/submitcontact.htm",method=RequestMethod.POST)
	public ModelAndView submitContact() {
		ModelAndView model = new ModelAndView();
		model.setViewName("redirect:contact.htm");
		
		return model;
	}
	
	/**
	 * This method is serving the request for get app page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/getapp.htm")
	public ModelAndView getAppPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("getapp");
		model.addObject("title","Get App");
		model.addObject("tagId","get-app");
		return model;
	}

	/**
	 * This method is serving for terms page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/terms.htm")
	public ModelAndView getTermsPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("terms");
		model.addObject("title","Terms");
		model.addObject("tagId","terms");
		return model;
	}

	/**
	 * This method is serving request for privacy policy page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/privacypolicy.htm")
	public ModelAndView getPrivacyPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("privacypolicy");
		model.addObject("title","Privacy");
		model.addObject("tagId","privacy");
		return model;
	}

	/**
	 * This method is serving request for faq page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/faq.htm")
	public ModelAndView getFaqPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("faq");
		model.addObject("title","Faqs");
		model.addObject("tagId","faq");
		return model;
	}

	/**
	 * this method is for serving the request for how works page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/howwork.htm")
	public ModelAndView getHowWorkPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("howwork");
		model.addObject("title","How Work");
		model.addObject("tagId","how-does-it-works");
		return model;
	}
	/**
	 * This method is for handling request for User Settings page
	 * @param request
	 * @param response
	 * @return
	 */
@RequestMapping("/settings.htm")
public ModelAndView getUserSettingsPage(HttpServletRequest request,
		HttpServletResponse response) {
	ModelAndView model = new ModelAndView("settings");
	model.addObject("settings", new UpdateUserSetting());
	model.addObject("title","Settings");
	model.addObject("tagId","settings");
	return model;
}

/**
 * Request Exception handler
 * @param ex
 * @param request
 * @param response
 * @return
 */
@ExceptionHandler(Exception.class) 
public ModelAndView handleException(Exception ex,HttpServletRequest request,HttpServletResponse response) {
	
	ModelAndView model=new ModelAndView("body/error500");
	model.addObject("Exception", ex);
	model.addObject("PageError", "Error in Request");
	return model;
}


}

package com.craterzone.gloomyweb.controller;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.gloomyweb.controller.util.CommonUtil;
import com.craterzone.gloomyweb.enums.PostType;
import com.craterzone.gloomyweb.enums.WebServiceURL;
import com.craterzone.gloomyweb.enums.constants.post.CategoryType;
import com.craterzone.gloomyweb.models.UserResponse;
import com.craterzone.gloomyweb.models.post.CommentDelete;
import com.craterzone.gloomyweb.models.post.CommentRequest;
import com.craterzone.gloomyweb.models.post.PostArray;
import com.craterzone.gloomyweb.models.post.PostDeleteRequest;
import com.craterzone.gloomyweb.models.post.PostHideRequest;
import com.craterzone.gloomyweb.models.post.PostResponse;
import com.craterzone.gloomyweb.models.post.PostShareLikeRequest;
import com.craterzone.gloomyweb.models.post.PostViewRequest;
import com.craterzone.gloomyweb.models.post.PublicPostResponse;
import com.craterzone.gloomyweb.models.post.SharePrivatePostRequest;
import com.craterzone.gloomyweb.models.post.StoryAdd;
import com.craterzone.gloomyweb.service.PostService;
import com.craterzone.gloomyweb.service.UserService;
import com.craterzone.gloomyweb.utils.common.ConstantsUtil;
import com.craterzone.gloomyweb.utils.common.JsonMapper;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;


/**
 * 
 * @author Piyush gupta
 *
 */
@Controller
public class PostController {
	
	@Autowired
	PostService postService;
	@Autowired
	UserService userService;
	@Autowired
	JsonMapper jsonMapper;
	
	private final Logger _logger = Logger.getLogger(PostController.class.getName());
	
	/**
	 * This method will serve the request for Submit Story page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/submitstory.htm")
	public ModelAndView submitStoryPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView model = new ModelAndView("postStory");
		StoryAdd story = new StoryAdd();
		model.addObject("postRequest", story);
		HttpSession session = request.getSession();
		model.addObject("postRequestJson", jsonMapper.convertJson(story));
		if(session.getAttribute("postType_SES") != null && session.getAttribute("postType_SES").toString().length()>0) {
		model.addObject("postType", session.getAttribute("postType_SES"));
		model.addObject("submitStorySuccess",session.getAttribute("submitStorySuccess_SES"));
		model.addObject("submitStoryError",session.getAttribute("submitStoryError_SES"));
		model.addObject("URL",session.getAttribute("URL_SES"));
		model.addObject("postId",session.getAttribute("postId_SES"));
		model.addObject("edit", session.getAttribute("edit_SES"));
		if(session.getAttribute("submitStoryError_SES") !=null) {
			model.addObject("postRequest", session.getAttribute("postRequest_SES"));
			model.addObject("postRequestJson", session.getAttribute("postRequestJson_SES"));
			
		}
		session.removeAttribute("postType_SES");
		session.removeAttribute("URL_SES");
		session.removeAttribute("submitStoryError_SES");
		session.removeAttribute("submitStorySuccess_SES");
		session.removeAttribute("postId_SES");
		session.removeAttribute("postRequest_SES");
		session.removeAttribute("edit_SES");
		session.removeAttribute("postRequestJson_SES");
		}
		model.addObject("title","Submit Fetaboo");
		model.addObject("tagId","submit-fetaboo");
		return model;
	}
	
	/**
	 * This Method is for handling request for Adding a Post type of public/private
	 * @param storyAdd
	 * @param result
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/submitpost.htm", method = RequestMethod.POST)
	public ModelAndView submitPost(@RequestParam("postType") String type,@ModelAttribute("postRequest") StoryAdd storyAdd,BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.setAttribute("postType_SES", type);
			PostResponse postResponse = postService.putPost(storyAdd,type , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
			if(postResponse != null) {
				session.setAttribute("submitStorySuccess_SES", "Your Fetaboo Shared Successfully");
				session.setAttribute("URL_SES", postResponse.getPostURL());
				session.setAttribute("postId_SES", postResponse.getPostId());
			} else {
				session.setAttribute("submitStoryError_SES", "Sorry Your Fetaboo Could not Shared");
				session.setAttribute("postRequest_SES", storyAdd);
				session.setAttribute("postRequestJson_SES", jsonMapper.convertJson(storyAdd));
			}
	
			return new ModelAndView("redirect:/submitstory.htm");
	}

	/**
	 * this method is serving request for story page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/story.htm",method=RequestMethod.GET)
	public ModelAndView getStoryPage(@RequestParam("r_token") String rowIdToken,  HttpServletRequest request,
			HttpServletResponse response) {
		CommonUtil.getSessionByCokkies(request, response, userService);
			String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		   if (ipAddress == null) {  
			   ipAddress = request.getRemoteAddr();  
		   }
		   
			ModelAndView model = new ModelAndView("story");
			model.addObject("title","Story");
		if (rowIdToken != null && rowIdToken.length() > 0 ) {
			String[] story = rowIdToken.split("\\|");
			PublicPostResponse post = null;
			if(story[1].equals("PRI")) {
				post = postService.getPrivatePost(story[0]);
			} else if(story[1].equals("PUB")) {
				post  = postService.getPublicPost(story[0]);
			}
			
			if (post != null) {
				PostViewRequest postView = new PostViewRequest(post.getPostId(),
						ipAddress);
				postService.viewAdd(jsonMapper.convertJson(postView));
				model.addObject("stories", post);
				model.addObject("storiesJson", jsonMapper.convertJson(post));
				return model;
			}
		}
		model.addObject("stories",null);
		return model;
	}

/**
 * This Method is for handling the request for adding comment on post
 * @param comment
 * @param postId
 * @param userId
 * @param request
 * @param response
 * @return
 */
	@RequestMapping(value="/addcomment.htm",method=RequestMethod.POST)
	public @ResponseBody String addCommentOnPost(@RequestBody String comment,HttpServletRequest request,HttpServletResponse response) {
		CommentRequest commentRequest = jsonMapper.toModel(comment, CommentRequest.class);
		HttpSession session = request.getSession();
		return  postService.commentAdd(jsonMapper.convertJson(commentRequest) , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
	}
	
	/**
	 * This Method is for handling request for Share and like the post
	 * @param type
	 * @param postId
	 * @param userId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/addlikeshare.htm",method=RequestMethod.POST)
	public @ResponseBody String addLikeAndShareOnPost(@RequestBody String likeShare,HttpServletRequest request,HttpServletResponse response) {
		PostShareLikeRequest shareLike = jsonMapper.toModel(likeShare, PostShareLikeRequest.class);
		HttpSession session = request.getSession();
		if(shareLike.getType().equals("like")) {
			return postService.likeAdd(jsonMapper.convertJson(shareLike) , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		} else if(shareLike.getType().equals("share")) {
			return postService.shareAdd(jsonMapper.convertJson(shareLike) , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		}
		return ConstantsUtil.JSON_BLANK_STRING;
		
	}
	/**
	 * This method is for handling the request for searching via category
	 * @param type
	 * @param count
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/searchcategory.htm")
	public @ResponseBody String getPostByCategory(@RequestParam("type") String type,@RequestParam("count") long count,HttpServletRequest request,HttpServletResponse response) {
		 UserResponse user =(UserResponse)request.getSession().getAttribute("usermodel"); 
		 if(user==null) {
			 user=new UserResponse();
		 }
		return postService.getSearchByCategory(CategoryType.getType(type),count , user.getUserId());
	}
	
	@RequestMapping("/public.htm")
	public @ResponseBody String getPublicPost(@RequestParam("count") long count , HttpServletRequest request , HttpServletResponse response) {
		 UserResponse user =(UserResponse)request.getSession().getAttribute("usermodel"); 
		 if(user==null) {
			 user=new UserResponse();
		 }
		return postService.getPublicPost(count , user.getUserId());
	}
	
	/**
	 * This Method is for handling the request for showing particular category post
	 * @param type
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/postcategory.htm")
	public ModelAndView getCategory(@RequestParam("type") String type,HttpServletRequest request,HttpServletResponse response) {
		ModelAndView model = new ModelAndView("postPage");
		model.addObject("title","DashBoard");
		model.addObject("tagId","dashboard");
		model.addObject("categoryType",type);
		 UserResponse user =(UserResponse)request.getSession().getAttribute("usermodel"); 
		 if(user==null) {
			 user=new UserResponse();
		 }
		String responseText = postService.getSearchByCategory(CategoryType.getType(type),0  , user.getUserId());
		if (responseText != null) {
			try {
				PostArray post = jsonMapper.toModel(responseText,PostArray.class);
				model.addObject("stories", post.getPostList());
				model.addObject("storiesJson",jsonMapper.convertJson(post));
				model.addObject("postType", type);
				return model;
			}catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		model.addObject("stories",null );
		return model;
		
	}
	/**
	 * This Method is for handling the request for share private post via email
	 * @param email
	 * @param url
	 * @param postId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/shareprivatly.htm",method=RequestMethod.POST)
	public @ResponseBody String sharePostPrivatly(@RequestBody String jsonData,HttpServletRequest request,HttpServletResponse response) {
		/*if(email != null && email.length() > 0 && url != null && url.length() > 0) {
			String[] emailId = email.split(",");
			Set<String> emailIds = new HashSet<String>();
			for(String id:emailId) {
				emailIds.add(id);
			}*/
		HttpSession session = request.getSession();
			SharePrivatePostRequest sharePost = jsonMapper.toModel(jsonData, SharePrivatePostRequest.class);
			return postService.sharePostPrivatly(jsonMapper.convertJson(sharePost) , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
			
	}
	/**
	 * This method is for getting total count of category type post on network
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/categorycount.htm")
	public @ResponseBody String categoryCount(HttpServletRequest request,HttpServletResponse response) {
		return postService.getCategoryCount();
	}
	/**
	 * This Method is for retrieving comments on post
	 * @param language
	 * @param postId
	 * @param count
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/comment.htm")
	public @ResponseBody String getComment(@RequestParam("postId") String postId,@RequestParam("count") long count,HttpServletRequest request,HttpServletResponse response) {
		
	return postService.getComment(postId, count);	
	}
	/**
	 * This method is for Hiding the the post from user timelines
	 * @param postId
	 * @param userId
	 * @param request
	 * @return
	 */
	@RequestMapping("/hide.htm")
	public @ResponseBody String hidePost(@RequestBody String jsonData,HttpServletRequest request) {
		PostHideRequest hide = jsonMapper.toModel(jsonData, PostHideRequest.class);
		HttpSession session = request.getSession();
		if(hide.getPostId()!=null && hide.getUserId()!=null && hide.getPostId().length()>0 && hide.getUserId().length()>0) {
		return postService.hidePost(jsonMapper.convertJson(hide) , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	/**
	 * This method  is for user subscription for fetaboo newsletter
	 * @param emailId
	 * @param request
	 * @param response
	 */
	@RequestMapping("/subscribe.htm")
	public void subscribe(@RequestParam("emailId") String emailId,HttpServletRequest request,HttpServletResponse response) {
		postService.subscribe(emailId);
	}
	
	
	@RequestMapping("/mostcommented.htm")
	public @ResponseBody String getMostCommentedPost(@RequestParam("count") int count , HttpServletRequest request) {
		return postService.mostPost(WebServiceURL.MOST_COMMENT,count);
		
	}
	@RequestMapping("/mostCommented.htm")
	public ModelAndView getMostCommentedPostForDashboard(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("postPage");
		model.addObject("title","DashBoard");
		model.addObject("tagId","dashboard");
		String responseText =  postService.mostPost(WebServiceURL.MOST_COMMENT,0);
		if (responseText != null && responseText.length()>0) {
			try {
				PostArray post = jsonMapper.toModel(responseText,PostArray.class);
				model.addObject("stories", post.getPostList());
				model.addObject("storiesJson",jsonMapper.convertJson(post));
				return model;
			}catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		model.addObject("stories",null );
		return model;
	}
	@RequestMapping("/mostviewed.htm")
	public @ResponseBody String getMostViewedPost(@RequestParam("count") int count ,  HttpServletRequest request) {
		return postService.mostPost(WebServiceURL.MOST_VIEW,count);
	}
	@RequestMapping("/mostViewed.htm")
	public ModelAndView getMostViewedPostForDashboard(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("postPage");
		model.addObject("title","DashBoard");
		model.addObject("tagId","dashboard");
		String responseText =  postService.mostPost(WebServiceURL.MOST_VIEW,0);
		if (responseText != null && responseText.length()>0) {
			try {
				PostArray post = jsonMapper.toModel(responseText,PostArray.class);
				model.addObject("stories", post.getPostList());
				model.addObject("storiesJson",jsonMapper.convertJson(post));
				return model;
			}catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		model.addObject("stories",null );
		return model;
	}
	@RequestMapping("/mostshared.htm")
	public @ResponseBody String getMostSharedPost(@RequestParam("count") int count,  HttpServletRequest request) {
		return postService.mostPost(WebServiceURL.MOST_SHARE,count);
		
	}/*
	@ResponseMapping("/similarpost.htm")
	public @ResponseBody String getSmilarStories() {
		
	}*/
	@RequestMapping("/populartag.htm")
	public @ResponseBody String getPopularTag() {
		return postService.popularTag();
	}
	
	/**
	 * This controller is used for getting post by tag name
	 */
	@RequestMapping("/postbytag.htm")
	public @ResponseBody String getPostByTagName(@RequestParam("tag") String tag ,@RequestParam("count") int count, HttpServletRequest request) {
		if(tag != null && tag.length() > 0 ) {
			 UserResponse user =(UserResponse)request.getSession().getAttribute("usermodel"); 
			 if(user==null) {
				 user = new UserResponse();
			 }
			PostArray postArray = postService.getPostByTag(tag , count , user.getUserId());
			if(postArray != null) {
			return jsonMapper.convertJson(postArray);
			}
		}
		
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	/**
	 * This controles for adding view on post
	 * @param postId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/view.htm")
	public @ResponseBody String view(@RequestParam("postId") String postId,HttpServletRequest request,HttpServletResponse response) {
		String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		   if (ipAddress == null) {  	
			   ipAddress = request.getRemoteAddr();  
		   }
		PostViewRequest postView = new PostViewRequest(postId, ipAddress);
		return postService.viewAdd(jsonMapper.convertJson(postView));
	}
	
	/**
	 * This controlles the request for deleting the comment
	 * @param postId
	 * @param commentId
	 * @param userId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/commentdelete.htm")
	public @ResponseBody String deleteComment(@RequestBody String jsonData,HttpServletRequest request,HttpServletResponse response) {
		UserResponse user = (UserResponse)request.getSession().getAttribute("usermodel");
			CommentDelete delete = jsonMapper.toModel(jsonData, CommentDelete.class);
			HttpSession session = request.getSession();
			if(delete.getUserId().equals(user.getUserId())) {
		return postService.deleteComment(delete , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	@RequestMapping("/mypost.htm")
	public ModelAndView getMyPosts(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("postPage");
		model.addObject("title","DashBoard");
		model.addObject("tagId","dashboard");
		HttpSession session = request.getSession();
		UserResponse user =  (UserResponse)request.getSession().getAttribute("usermodel");
		if(user!=null && user.getUserId()!=null && user.getUserId().length()>0) {
		String myPostResponse = postService.getMyPublicPost(user.getUserId(), 0 , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		if (myPostResponse != null && myPostResponse.length()>0) {
			try {
				PostArray post = jsonMapper.toModel(myPostResponse,
						PostArray.class);
				model.addObject("stories", post.getPostList());
				model.addObject("storiesJson",jsonMapper.convertJson(post));
				model.addObject("url", "myPost.htm");
				return model;
			} catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		}
		model.addObject("stories", null);
		return model;
	}
	@RequestMapping("/myPost.htm")
	public @ResponseBody String getMyPostsOnScroll(@RequestParam("userId") String userId , @RequestParam("count") long count , @RequestParam("storytype") String storyType ,HttpServletRequest request) {
		HttpSession session = request.getSession();
		if(storyType!=null && storyType.equalsIgnoreCase("PRI")) {
		return postService.getMyPrivatePost(userId, count , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		}
		return postService.getMyPublicPost(userId, count , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
	}
	@RequestMapping("/editpost.htm")
	public @ResponseBody String editPost(@RequestBody String jsonData , HttpServletRequest request ) {
		
		if(jsonData!=null && jsonData.length()>0) {
			HttpSession session = request.getSession();
			PublicPostResponse post = jsonMapper.toModel(jsonData, PublicPostResponse.class);
			StoryAdd storyAdd = new StoryAdd(post.getPostId(), post.getUserThumbInfo().getUserId(), post.getCaption(),post.getPostDetail(), post.getCategoryType(), new ArrayList(post.getTags()),post.getLresURL());
			session.setAttribute("submitStoryError_SES", "");
			session.setAttribute("submitStorySuccess_SES", "");
			session.setAttribute("postType_SES",PostType.PUBLIC.toString() );
			session.setAttribute("postRequest_SES", storyAdd);
			session.setAttribute("postRequestJson_SES", jsonMapper.convertJson(storyAdd));
			session.setAttribute("edit_SES", true);
		return "success";
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	
	@RequestMapping("/updatepost.htm")
	public ModelAndView updatePost(@RequestParam("postType") String type,@ModelAttribute("postRequest") StoryAdd storyAdd,BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		boolean updateResponse = postService.updatePost(storyAdd , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		if(updateResponse) {
			return new ModelAndView("redirect:/dashboard.htm");
		} else {
			request.getSession().setAttribute("submitStoryError_SES", "Sorry Your Fetaboo Could not updated");
			request.getSession().setAttribute("postRequest_SES", storyAdd);
			request.getSession().setAttribute("postRequestJson_SES", jsonMapper.convertJson(storyAdd));
			request.getSession().setAttribute("postType_SES",PostType.PUBLIC.toString() );
			request.getSession().setAttribute("edit_SES", true);
		}

		return new ModelAndView("redirect:/submitstory.htm");
	}
	/**
	 * this method is for deleting the post
	 * @param postId
	 * @return
	 */
	@RequestMapping("deletepost.htm")
	public @ResponseBody String deletePost(@RequestBody String jsonData , HttpServletRequest request) {
			PostDeleteRequest post = jsonMapper.toModel(jsonData, PostDeleteRequest.class);	
			HttpSession session = request.getSession();
			UserResponse user = (UserResponse)request.getSession().getAttribute("usermodel");
			if(post!=null && user !=null && post.getUserId().equals(user.getUserId())) {
				return postService.deletePost(jsonData , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
			}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	
	/**
	 * Request Exception handler
	 * @param ex
	 * @param request
	 * @param response
	 * @return
	 */
	@ExceptionHandler(Exception.class) 
	public ModelAndView handleException(Exception ex,HttpServletRequest request,HttpServletResponse response) {
		_logger.log(Level.SEVERE , " error in serving request " , ex);
		ModelAndView model=new ModelAndView("body/error500");
		model.addObject("Exception", ex);
		model.addObject("PageError", "Error in Request");
		return model;
	}

	
}

package com.craterzone.gloomyweb.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.gloomyweb.controller.util.CommonUtil;
import com.craterzone.gloomyweb.enums.WebServiceCode;
import com.craterzone.gloomyweb.models.Login;
import com.craterzone.gloomyweb.models.ReportUser;
import com.craterzone.gloomyweb.models.UpdateUserSetting;
import com.craterzone.gloomyweb.models.UserResponse;
import com.craterzone.gloomyweb.models.post.PostArray;
import com.craterzone.gloomyweb.service.PostService;
import com.craterzone.gloomyweb.service.UserService;
import com.craterzone.gloomyweb.utils.common.ConstantsUtil;
import com.craterzone.gloomyweb.utils.common.JsonMapper;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.UniformInterfaceException;

@Controller
public class UserController {

	private final Logger _logger = Logger.getLogger(UserController.class
			.getName());

	@Autowired
	UserService userService;

	@Autowired
	PostService postService;

	@Autowired
	JsonMapper jsonMapper;

	/**
	 * This controller method is taking request for user Sign in
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/signup.htm")
	public String signIn(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();
		String userEmail = (request.getParameter("signEmail") != null) ? request
				.getParameter("signEmail") : "";
		String password = (request.getParameter("signPassword") != null) ? request
				.getParameter("signPassword") : "";
		if (userEmail == "" || password == "") {
			model.put("registerationError", "There was an error with your E-Mail/Password combination. Please try again");
			return "redirect:/dashboard.htm";
		}
		Login user = new Login(userEmail, password);
		ClientResponse clientResponse = userService.signUpUser(user);
		if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
			UserResponse userResponse = jsonMapper.toModel(clientResponse.getEntity(String.class), UserResponse.class);
			if (userResponse != null) {
				session.setAttribute("usermodel", userResponse);
				session.setAttribute("userName",userResponse.getUserId());
				/* Making Cookies for User Session */
				Cookie cook = new Cookie(ConstantsUtil.USER_SESSION_SOURCE, ConstantsUtil.WEB_SOURCE_TYPE);
				cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
				response.addCookie(cook);
				cook = new Cookie(ConstantsUtil.USER_SESSION_EMAIL_KEY , userEmail);
				cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
				response.addCookie(cook);
				cook = new Cookie(ConstantsUtil.USER_SESSION_PASSWORD_KEY, password);
				cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
				response.addCookie(cook);
				cook = new Cookie(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION,userResponse.getToken());
				cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
				response.addCookie(cook);
				
				return "redirect:/dashboard.htm";
			}
		} else if(clientResponse.getStatus() == WebServiceCode.CONFLICT.getCode()) {
			model.put("registerationError", "User With Same Email Id is Already Registered");
			return "redirect:/dashboard.htm";
		} 
		model.put("registerationError", "There was an error with your E-Mail/Password combination. Please try again");
		return "redirect:/dashboard.htm.htm" ;
	}

	/**
	 * Dash-board page
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/dashboard.htm")
	public ModelAndView dashboard(HttpServletRequest request,
			HttpServletResponse response) {
		if(!CommonUtil.getSessionByCokkies(request, response , userService)) {
			Cookie requestCookie = new Cookie(ConstantsUtil.COOKIE_NAME, ConstantsUtil.COOKIE_VALUE);
			requestCookie.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(requestCookie);
	}
		ModelAndView model = new ModelAndView("postPage");
		model.addObject("title","DashBoard");
		//model.addObject("tagId","dashboard");
		 UserResponse user =(UserResponse)request.getSession().getAttribute("usermodel"); 
		 if(user==null) {
			 user=new UserResponse();
		 }
		String postResponse = postService.getPublicPost(0  , user.getUserId());
		
		if (postResponse != null && postResponse.length()>0) {
			try {
				PostArray post = jsonMapper.toModel(postResponse,
						PostArray.class);
				model.addObject("stories", post.getPostList());
				model.addObject("storiesJson",jsonMapper.convertJson(post));
				return model;
			} catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		model.addObject("stories", null);
		return model;
	}
/**
	 * This Method is for handling the request for Reporting the User
	 * @param userId
	 * @param reportUserId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/reportuser.htm",method=RequestMethod.POST)
	public @ResponseBody String reportUser(@RequestBody String jsonData,HttpServletRequest request,HttpServletResponse response) {
		ReportUser reportUser = jsonMapper.toModel(jsonData, ReportUser.class);
		HttpSession session = request.getSession();
		if(reportUser.getUserId() !=null && reportUser.getUserId().length() > 0 && reportUser.getAbuseUserId() !=null &&  reportUser.getAbuseUserId().length() > 0 ) {
			return userService.reportUser(reportUser , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	
	/**
	 * This Method is for Blocking the User
	 * @param userId
	 * @param reportUserId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/blockuser.htm",method=RequestMethod.POST)
	public @ResponseBody String blockUser(@RequestBody String jsonData,HttpServletRequest request,HttpServletResponse response) {
		ReportUser blockUser = jsonMapper.toModel(jsonData, ReportUser.class);
		HttpSession session = request.getSession();
		if(blockUser.getUserId() !=null && blockUser.getUserId().length() > 0 && blockUser.getAbuseUserId() !=null &&  blockUser.getAbuseUserId().length() > 0 ) {
			return userService.blockUser(blockUser , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	@RequestMapping("/updatesetting.htm")
	public ModelAndView updateSettings(@ModelAttribute("settings") UpdateUserSetting updateSettings,HttpServletRequest request,HttpServletResponse response){
		ModelAndView model = new ModelAndView("redirect:settings");
		HttpSession session = request.getSession();
		userService.updateUserSettings(updateSettings , session.getAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION).toString());
		return model;
	}
	/**
	 * Request Exception handler
	 * @param ex
	 * @param request
	 * @param response
	 * @return
	 */
	@ExceptionHandler(Exception.class) 
	public ModelAndView handleException(Exception ex,HttpServletRequest request,HttpServletResponse response) {
		_logger.log(Level.SEVERE , " error in serving request " , ex);
		ModelAndView model=new ModelAndView("body/error500");
		model.addObject("Exception", ex);
		model.addObject("PageError", "Error in Request");
		return model;
	}

}

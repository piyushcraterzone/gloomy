package com.craterzone.gloomyweb.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.craterzone.gloomyweb.controller.util.CommonUtil;
import com.craterzone.gloomyweb.models.Login;
import com.craterzone.gloomyweb.models.SocialLogin;
import com.craterzone.gloomyweb.models.UserResponse;
import com.craterzone.gloomyweb.service.PostService;
import com.craterzone.gloomyweb.service.UserService;
import com.craterzone.gloomyweb.utils.common.ConstantsUtil;
import com.craterzone.gloomyweb.utils.common.JsonMapper;
import com.sun.jersey.core.util.Base64;

@Controller
public class Authentication {

	@Autowired
	PostService postService;

	@Autowired
	JsonMapper jsonMapper;
	@Autowired
	UserService userService;
	private final Logger _logger = Logger.getLogger(Authentication.class
			.getName());
	
	@RequestMapping(value = "/index1.htm", method = RequestMethod.GET)
	public void index(ModelMap model,HttpServletRequest request,HttpServletResponse response) throws Exception {
		response.sendRedirect(ConstantsUtil.PROD_URL+"/index.htm");
		return ;
	}
	
	/**
	 * This controller is for controlling the index page 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index.htm", method = RequestMethod.GET)
	public String loginMenu(ModelMap model,HttpServletRequest request,HttpServletResponse response) throws Exception {
		if(CommonUtil.getSessionByCokkies(request, response , userService)) {
			return "redirect:/dashboard.htm";
		}
			Cookie requestCookie = new Cookie(ConstantsUtil.COOKIE_NAME, ConstantsUtil.COOKIE_VALUE);
			requestCookie.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(requestCookie);
			return "index";
	}

	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	public String authentication(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		String userName = request.getParameter("emailId");
		String password = request.getParameter("password");
		if(userName==null||userName.length()<=0||password==null||password.length()<=0){
			return "index";
		}
		Login user = new Login(userName,password);
		/*user.setEmailId(userName);
		user.setPassword(password);*/
		UserResponse userinfo = userService.loginUser(user);
		if(userinfo!=null){
			session.setAttribute("usermodel",userinfo);
			session.setAttribute("userName",userinfo.getUserId());
			Cookie cook = new Cookie(ConstantsUtil.USER_SESSION_SOURCE, ConstantsUtil.WEB_SOURCE_TYPE);
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			cook = new Cookie(ConstantsUtil.USER_SESSION_EMAIL_KEY , new String(Base64.encode(userName)));
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			cook = new Cookie(ConstantsUtil.USER_SESSION_PASSWORD_KEY, new String(Base64.encode(password)));
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			cook = new Cookie(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION,userinfo.getToken());
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			return "redirect:/dashboard.htm";
		}
		model.put("loginError", "Please enter valid Email/Password");
		return "index";
	}

	@RequestMapping(value = "/logout.htm", method = RequestMethod.GET)
	public String logout(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(false);
		if(session !=null){
			session.invalidate();
		}
		Cookie[] cookie = request.getCookies();
		for(Cookie cook:cookie) {
			cook.setMaxAge(0); 
			response.addCookie(cook);
		}
		return "redirect:/dashboard.htm";
	}
	
	@RequestMapping(value="/sociallogin.htm" , method = RequestMethod.POST) 
	//public @ResponseBody String socialLogin(String emailId,String cc,Gender gender,String name,String socialFBLink,String socialGplusLink, String socialAvatar,HttpServletRequest request,HttpServletResponse response) throws Exception {
	public @ResponseBody String socialLogin(@RequestBody String socialLogin,HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		//SocialLogin login = new SocialLogin(emailId, name, cc, gender, socialFBLink, socialGplusLink,socialAvatar);
		SocialLogin login = jsonMapper.toModel(socialLogin, SocialLogin.class);
				UserResponse userInfo = userService.socialLogin(login);
				request.getParameter("emailId");
				request.getAttribute("emailId");
		if(userInfo!=null){
			HttpSession session = request.getSession();
			session.setAttribute("usermodel",userInfo);
			session.setAttribute("userName",userInfo.getUserId());
			
			Cookie cook = new Cookie(ConstantsUtil.USER_SESSION_SOURCE, ConstantsUtil.SOCIAL_SOURCE_TYPE);
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			cook = new Cookie(ConstantsUtil.USER_SESSION_EMAIL_KEY , new String(Base64.encode(login.getEmailId())));
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			cook = new Cookie(ConstantsUtil.SOCIAL_AVATAR,login.getSocialAvatar());
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			cook = new Cookie(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION,userInfo.getToken());
			cook.setMaxAge(ConstantsUtil.COOKIES_SET_MAX_AGE);
			response.addCookie(cook);
			
			return "success";
		} 
		return "error";
		
	}
	/**
	 * Request Exception handler
	 * @param ex
	 * @param request
	 * @param response
	 * @return
	 */
	@ExceptionHandler(Exception.class) 
	public ModelAndView handleException(Exception ex,HttpServletRequest request,HttpServletResponse response) {
		_logger.log(Level.SEVERE , " error in serving request " , ex);
		ModelAndView model=new ModelAndView("body/error500");
		model.addObject("Exception", ex);
		model.addObject("PageError", "Error in Request");
		return model;
	}

}

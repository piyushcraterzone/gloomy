package com.craterzone.gloomyweb.controller.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.craterzone.gloomyweb.models.Login;
import com.craterzone.gloomyweb.models.SocialLogin;
import com.craterzone.gloomyweb.models.UserResponse;
import com.craterzone.gloomyweb.service.UserService;
import com.craterzone.gloomyweb.utils.common.ConstantsUtil;
import com.sun.jersey.core.util.Base64;

public class CommonUtil {

	
	public static boolean getSessionByCokkies(HttpServletRequest request , HttpServletResponse response , UserService userService) {
		Cookie[] cookies = request.getCookies();
		boolean isCookiesFound = false;
		if(cookies != null) {
			String sourceType = "";
			String emailId = "";
			String password = "";
			String socialAvatar = "";
			for(Cookie cook: cookies) {
				if(cook.getName().equals(ConstantsUtil.COOKIE_NAME)) {
					isCookiesFound = true;
					continue;
				}
					if(cook.getName().equals(ConstantsUtil.USER_SESSION_SOURCE)) {
						sourceType = cook.getValue();
						continue;
					}
						if(cook.getName().equals(ConstantsUtil.USER_SESSION_EMAIL_KEY)) {
						emailId = Base64.base64Decode(cook.getValue());
						isCookiesFound = true;
						continue;
						}
						if(cook.getName().equals(ConstantsUtil.USER_SESSION_PASSWORD_KEY)) {
							password = Base64.base64Decode(cook.getValue());
							continue;
						}
						if(cook.getName().equals(ConstantsUtil.SOCIAL_AVATAR)) {
							socialAvatar = cook.getValue();
					}
				}
			if(isCookiesFound) {
			HttpSession session = request.getSession();
			if(sourceType.equals(ConstantsUtil.WEB_SOURCE_TYPE)) {
				Login user = new Login(emailId,password);
				/*user.setEmailId(userName);
				user.setPassword(password);*/
				UserResponse userinfo = userService.loginUser(user);
				if(userinfo!=null){
					session.setAttribute("usermodel",userinfo);
					session.setAttribute("userName",userinfo.getUserId());
					session.setAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION,userinfo.getToken());
				}
			} else if(sourceType.equals(ConstantsUtil.SOCIAL_SOURCE_TYPE)) {
				SocialLogin socialLogin = new SocialLogin(emailId,socialAvatar);
				UserResponse userInfo = userService.socialLogin(socialLogin);
				if(userInfo!=null){
					session.setAttribute("usermodel",userInfo);
					session.setAttribute("userName",userInfo.getUserId());
					session.setAttribute(ConstantsUtil.HEADER_USER_AUTH_TOKEN_CAPTION,userInfo.getToken());
				}
			}
		}
		}
		return isCookiesFound;
	}
}

package com.craterzone.gloomyweb.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.gloomyweb.enums.PostType;
import com.craterzone.gloomyweb.enums.WebServiceURL;
import com.craterzone.gloomyweb.models.Image;
import com.craterzone.gloomyweb.models.Location;
import com.craterzone.gloomyweb.models.post.CommentDelete;
import com.craterzone.gloomyweb.models.post.PostArray;
import com.craterzone.gloomyweb.models.post.PostRequest;
import com.craterzone.gloomyweb.models.post.PostResponse;
import com.craterzone.gloomyweb.models.post.PostUpdateRequest;
import com.craterzone.gloomyweb.models.post.PublicPostResponse;
import com.craterzone.gloomyweb.models.post.StoryAdd;
import com.craterzone.gloomyweb.utils.WebServiceCaller;
import com.craterzone.gloomyweb.utils.common.ConstantsUtil;
import com.craterzone.gloomyweb.utils.common.JsonMapper;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

public class PostService {

	@Autowired
	JsonMapper jsonMapper;

	private final Logger _logger = Logger
			.getLogger(PostService.class.getName());

	public PublicPostResponse getPublicPost(String rowIdToken) {
		ClientResponse clientResponse = WebServiceCaller.GET(
				WebServiceURL.storyGetURL(WebServiceURL.POST, rowIdToken));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			try {
				PublicPostResponse post = jsonMapper.toModel(
						clientResponse.getEntity(String.class),
						PublicPostResponse.class);
				return post;
			} catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		return null;
	}

	/**
	 * this method is for getting Private post
	 * 
	 * @param postId
	 * @return
	 */
	public PublicPostResponse getPrivatePost(String postId) {
		ClientResponse clientResponse = WebServiceCaller
				.GET(WebServiceURL
						.privatePostGetURL(WebServiceURL.POST, postId));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			try {
				PublicPostResponse post = jsonMapper.toModel(
						clientResponse.getEntity(String.class),
						PublicPostResponse.class);
				return post;
			} catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		return null;
	}

	public ClientResponse putPublicPost(String jsonData, String token) {
		return WebServiceCaller.PUT(
				WebServiceURL.publicPostURL(WebServiceURL.POST), jsonData,
				token);
	}

	/**
	 * This Method is for handling request for Adding private and public post
	 * 
	 * @param storyAdd
	 * @param type
	 * @return
	 */
	public PostResponse putPost(StoryAdd storyAdd, String type, String token) {
		try {
			Image image = null;
			if (storyAdd.getMultipartFile() != null
					&& storyAdd.getMultipartFile().getSize() > 0) {
				image = new Image();
				image.setContent(storyAdd.getMultipartFile().getBytes());
				String imageName = storyAdd.getMultipartFile()
						.getOriginalFilename();
				image.setExtension(imageName.substring(imageName
						.lastIndexOf(".") + 1));
			}
			Location location = new Location(0.0, 0.0); // By default atlantic
			Set<String> tags = new HashSet<String>();
			tags.addAll(storyAdd.getTags());
			PostRequest postRequest = new PostRequest(storyAdd.getUserId(),
					storyAdd.getCaption(), storyAdd.getSubHeading(),
					storyAdd.getPostDetail(), storyAdd.getCategoryType(),
					storyAdd.getColorCode(), tags, image);
			postRequest.setLocation(location);
			postRequest.setEmailId(storyAdd.getEmailId());
			String jsonData = jsonMapper.convertJson(postRequest);
			ClientResponse clientResponse = null;
			if (type.equals(PostType.PRIVATE.toString())) {
				clientResponse = WebServiceCaller.PUT(
						WebServiceURL.privatePostURL(WebServiceURL.POST),
						jsonData, token);
			} else {
				clientResponse = WebServiceCaller.PUT(
						WebServiceURL.publicPostURL(WebServiceURL.POST),
						jsonData, token);
			}
			if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
				PostResponse postResponse = jsonMapper.toModel(
						clientResponse.getEntity(String.class),
						PostResponse.class);
				return postResponse;
			} else {
				_logger.log(Level.INFO, "Request Faild with status code : ",
						clientResponse.getStatus());

			}

		} catch (IOException e) {
			_logger.log(Level.INFO, "Error in multi part file", e);

		}
		return null;
	}

	/**
	 * This Method is for handling request for Searching the post by category
	 * type
	 * 
	 * @param categoryType
	 * @param count
	 * @return
	 */
	public String getSearchByCategory(int categoryType, long count, String userId) {
		ClientResponse clientResponse = WebServiceCaller.GET(WebServiceURL
				.publicPostByCategoryGetURL(WebServiceURL.POST, categoryType,
						count, userId));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		}

		return null;
	}

	public String getPublicPost(long count, String userId) {
		ClientResponse clientResponse = WebServiceCaller.GET(
				WebServiceURL.getPublicPost(WebServiceURL.POST, count, userId));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		}

		return ConstantsUtil.JSON_BLANK_STRING;
	}

	/**
	 * This method is for Adding a new comment on post
	 * 
	 * @param jsonData
	 * @return
	 */
	public String commentAdd(String jsonData, String token) {
		ClientResponse clientResponse = WebServiceCaller.PUT(
				WebServiceURL.commentAdd(WebServiceURL.POST), jsonData, token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		} else {
			return ConstantsUtil.JSON_BLANK_STRING;
		}
	}

	/**
	 * This Method is for Adding like on post
	 * 
	 * @param jsonData
	 * @return
	 */
	public String likeAdd(String jsonData, String token) {
		ClientResponse clientResponse = WebServiceCaller.POST(
				WebServiceURL.likeAdd(WebServiceURL.POST), jsonData, token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		} else {
			return "error";
		}
	}

	/**
	 * This Method is for Adding share on post
	 * 
	 * @param jsonData
	 * @return
	 */
	public String shareAdd(String jsonData, String token) {
		ClientResponse clientResponse = WebServiceCaller.POST(
				WebServiceURL.shareAdd(WebServiceURL.POST), jsonData, token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		} else {
			return "error";
		}
	}

	/**
	 * This Method is for Sharing the private post url through email
	 * 
	 * @param jsonData
	 * @return
	 */
	public String sharePostPrivatly(String jsonData, String token) {
		ClientResponse clientResponse = WebServiceCaller.POST(
				WebServiceURL.privatePostURL(WebServiceURL.POST), jsonData,
				token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return "success";
		} else {
			return "error";
		}
	}

	/**
	 * This method is for getting total no. of category wise post
	 * 
	 * @return
	 */
	public String getCategoryCount() {
		ClientResponse clientResponse = WebServiceCaller.GET(
				WebServiceURL.categoryCount(WebServiceURL.POST));
		return clientResponse.getEntity(String.class);
	}

	/**
	 * This method is for getting comments on post
	 */
	public String getComment(String postId, long count) {
		ClientResponse clientResponse = WebServiceCaller.GET(WebServiceURL
				.getCommentOnPost(WebServiceURL.POST, postId, count));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		}
		return clientResponse.getStatus() + "";
	}

	public String viewAdd(String jsonData) {
		ClientResponse clientResponse = WebServiceCaller.POST(
				WebServiceURL.viewAdd(WebServiceURL.POST), jsonData);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		} else {
			return ConstantsUtil.JSON_BLANK_STRING;
		}

	}

	public String hidePost(String jsonData, String token) {
		ClientResponse clientResponse = WebServiceCaller.POST(
				WebServiceURL.hidePost(WebServiceURL.POST), jsonData, token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return "success";
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}

	/**
	 * This function is used for subscribing user on fetaboo network
	 * 
	 * @param emailId
	 */
	public void subscribe(String emailId) {
		WebServiceCaller.GET(
				WebServiceURL.subscribe(WebServiceURL.POST, emailId));
	}

	/**
	 * This Method is for getting most Accessed
	 * 
	 * @param url
	 *            is for Most commented/mostShared/mostViewed
	 * @param count
	 * @return
	 */
	public String mostPost(String url, int count) {
		ClientResponse clientResponse = WebServiceCaller.GET(
				WebServiceURL.mostPosts(WebServiceURL.POST, url, count));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}

	/**
	 * This method is used for getting popular tags
	 * 
	 * @return
	 */
	public String popularTag() {
		ClientResponse clientResponse = WebServiceCaller.GET(
				WebServiceURL.popularTag(WebServiceURL.POST));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}

	/**
	 * This service is servicing the request for getting posts by tag Name
	 * 
	 * @param rowIdToken
	 * @return
	 */

	public PostArray getPostByTag(String tagName, int count, String userId) {
		ClientResponse clientResponse = WebServiceCaller.GET(WebServiceURL
				.postByTag(WebServiceURL.POST, tagName, count, userId));
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			try {
				PostArray post = jsonMapper
						.toModel(clientResponse.getEntity(String.class),
								PostArray.class);
				return post;
			} catch (ClientHandlerException e) {
				_logger.log(Level.INFO, "Error in Client Response", e);
			} catch (UniformInterfaceException e) {
				_logger.log(Level.INFO, "UniformedInterface Error", e);
			}
		}
		return null;
	}

	/**
	 * This method is for deleting the comment on post
	 * 
	 * @param delete
	 * @return
	 */
	public String deleteComment(CommentDelete delete, String token) {
		ClientResponse clientResponse = WebServiceCaller.DELETE(
				WebServiceURL.deleteComment(WebServiceURL.POST),
				jsonMapper.convertJson(delete), token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return "success";
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}

	/**
	 * this method is for getting all public posts for Userid
	 * 
	 * @param userId
	 * @param count
	 * @return
	 */
	public String getMyPublicPost(String userId, long count , String token) {
		ClientResponse clientResponse = WebServiceCaller.GET(WebServiceURL
				.userPublicPost(WebServiceURL.USER, userId, count) , token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}

	/**
	 * this method is for getting private post for user
	 * 
	 * @param userId
	 * @param count
	 * @return
	 */
	public String getMyPrivatePost(String userId, long count , String token) {
		ClientResponse clientResponse = WebServiceCaller.GET(WebServiceURL
				.userPrivatePost(WebServiceURL.USER, userId, count) , token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return clientResponse.getEntity(String.class);
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}

	/**
	 * This service is for getting story for editing by user
	 * 
	 * @param postId
	 * @return
	 */
	public boolean updatePost(StoryAdd storyAdd , String token) {
		PostUpdateRequest update = new PostUpdateRequest(storyAdd.getPostId(),
				storyAdd.getUserId(), storyAdd.getCaption(),
				storyAdd.getPostDetail(), new HashSet(storyAdd.getTags()));
		ClientResponse clientResponse = WebServiceCaller.POST(
				WebServiceURL.updatePost(WebServiceURL.POST),
				jsonMapper.convertJson(update) , token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return true;
		}
		return false;
	}

	/**
	 * this service for deleting the post
	 * 
	 * @param postId
	 * @return
	 */
	public String deletePost(String jsonData , String token) {
		ClientResponse clientResponse = WebServiceCaller.DELETE(
				WebServiceURL.deletePost(WebServiceURL.POST), jsonData , token);
		if (clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return "success";
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}

}

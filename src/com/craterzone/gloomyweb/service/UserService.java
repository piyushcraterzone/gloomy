package com.craterzone.gloomyweb.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.craterzone.gloomyweb.enums.WebServiceURL;
import com.craterzone.gloomyweb.models.Login;
import com.craterzone.gloomyweb.models.ReportUser;
import com.craterzone.gloomyweb.models.SocialLogin;
import com.craterzone.gloomyweb.models.UpdateUserSetting;
import com.craterzone.gloomyweb.models.UserResponse;
import com.craterzone.gloomyweb.utils.WebServiceCaller;
import com.craterzone.gloomyweb.utils.common.ConstantsUtil;
import com.craterzone.gloomyweb.utils.common.JsonMapper;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;

public class UserService {

	@Autowired
	JsonMapper jsonMapper;

	public UserResponse loginUser(Login user) {
		String jsonData = jsonMapper.convertJson(user);
		if (jsonData != null) {
			ClientResponse clientResponse = WebServiceCaller.POST(
					WebServiceURL.loginURL(WebServiceURL.USER), jsonData);
			Status status = Status.fromStatusCode(clientResponse.getStatus());
			switch (status) {
			case UNAUTHORIZED:
				break;
			case BAD_REQUEST:
				break;
			case NOT_FOUND:
				break;
			default:
				String response = clientResponse.getEntity(String.class);
				UserResponse user1 = jsonMapper.toModel(response,
						UserResponse.class);
				return user1;
			}

		}
		return null;
	}

	public ClientResponse signUpUser(Login user) {
		String jsonData = jsonMapper.convertJson(user);
		
			ClientResponse clientResponse = WebServiceCaller.PUT(
					WebServiceURL.signURL(WebServiceURL.USER), jsonData);
			return clientResponse;
			
	}
public String reportUser(ReportUser reportUser , String token) {
		
		ClientResponse clientResponse = WebServiceCaller.POST(WebServiceURL.reportUser(WebServiceURL.USER), jsonMapper.convertJson(reportUser) , token);
		if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return "success";
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	public String blockUser(ReportUser blockUser , String token) {
		ClientResponse clientResponse = WebServiceCaller.POST(WebServiceURL.blockUser(WebServiceURL.USER), jsonMapper.convertJson(blockUser) , token);
		if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return "success";
		}
		return ConstantsUtil.JSON_BLANK_STRING;
	}
	public UserResponse socialLogin(SocialLogin socialLogin) {
		ClientResponse clientResponse = WebServiceCaller.POST(WebServiceURL.loginSocial(WebServiceURL.USER), jsonMapper.convertJson(socialLogin));
		if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return jsonMapper.toModel(clientResponse.getEntity(String.class),UserResponse.class);
		}
		return null;
	}
	public String updateUserSettings(UpdateUserSetting userSettings,  String token) {
		ClientResponse clientResponse = WebServiceCaller.POST(WebServiceURL.updateSettings(WebServiceURL.USER), jsonMapper.convertJson(userSettings) , token);
		if(clientResponse.getStatus() == Status.OK.getStatusCode()) {
			return "success";
		}
		return "error";
	}

}

package com.craterzone.gloomyweb.service;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.craterzone.gloomyweb.utils.common.ConstantsUtil;

public class ResourceReaderService {
	private static final Logger _logger = Logger.getLogger(ResourceReaderService.class.getName());

	public String readErrorMessages(String key,Locale locale) {
		try {
		ResourceBundle bundle = ResourceBundle.getBundle(ConstantsUtil.ERROR_MESSAGE_FILE_NAME,locale);
		return bundle.getString(key);
		} catch(MissingResourceException e) {
			_logger.log(Level.INFO , "Resource not found ", e);
		}
		return "";
	}
	
}

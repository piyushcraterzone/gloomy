package com.craterzone.gloomyweb.utils.common;

/**
 * 
 * @author Piyush
 * This interface is for handling Constant Values in the project
 */
public interface ConstantsUtil {

	public static final String COOKIE_NAME = "GloomyAccessed";
	public static final String USER_SESSION_EMAIL_KEY = "GloomyUserEmail";
	public static final String USER_SESSION_PASSWORD_KEY = "GloomyUserPassword";
	public static final String USER_SESSION_SOURCE = "SessionSource";
	public static final String SOCIAL_SOURCE_TYPE="Social";
	public static final String SOCIAL_AVATAR = "SocialAvatar";
	public static final String WEB_SOURCE_TYPE="Web";
	public static final String COOKIE_VALUE = "Gloomy";
	public static final String DEFAULT_NOTIFICATION_RING_TYPE = "Vibrate";
	public static final String JSON_BLANK_STRING = "";
	public static final String ERROR_MESSAGE_FILE_NAME = "errorMessage";
	public static final String PROD_URL = "http://178.62.182.120:8080";
	public static final String LOCAL_URL = "http://192.168.1.2:8080/gloomy";
	public static final String HEADER_USER_AUTH_TOKEN_CAPTION = "GloomyUseTtoken";
	public static final int COOKIES_SET_MAX_AGE = 3456000;  // 40 days of cookies expiring
}

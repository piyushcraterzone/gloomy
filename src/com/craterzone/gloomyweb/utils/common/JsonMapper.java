package com.craterzone.gloomyweb.utils.common;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class JsonMapper {
	
	private static final Logger _logger = Logger.getLogger(JsonMapper.class.getName()); 
	
	private static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		mapper.configure(org.codehaus.jackson.map.SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
		return mapper;
	}
	public String convertJson(Object obj){
		try {
			return getObjectMapper().writeValueAsString(obj);
		} catch (JsonGenerationException e) {
			_logger.log(Level.SEVERE,"unable to generate the json"+e);
		} catch (JsonMappingException e) {
			_logger.log(Level.SEVERE,"unable to generate the json"+e);
		} catch (IOException e) {
			_logger.log(Level.SEVERE,"unable to generate the json"+e);
		}
		return null;
	}
	
	
	/**
	 * This method convert json to model
	 * @param jsonString : json string need to convert to model
	 * @param classType : Model class type
	 * @return : 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public <T> T toModel(String jsonString, Class<T> classType) {
		if(jsonString != null) {
			try {
				return getObjectMapper().readValue(jsonString, classType);
			} catch (JsonParseException e) {
				_logger.log(Level.SEVERE,"unable to generate the json"+e);
				e.printStackTrace();
			} catch (JsonMappingException e) {
				_logger.log(Level.SEVERE,"unable to generate the json"+e);
			} catch (IOException e) {
				_logger.log(Level.SEVERE,"unable to generate the json"+e);
			}
		}
		return null;
	}
}

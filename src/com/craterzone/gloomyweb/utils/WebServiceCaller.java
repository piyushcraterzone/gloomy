package com.craterzone.gloomyweb.utils;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class WebServiceCaller {
	
	public static ClientResponse GET(String URL , String token) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).header("token", token).get(ClientResponse.class);
	}
	
	public static ClientResponse POST(String URL,String jsonData , String token) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).header("token", token).post(ClientResponse.class,jsonData);
	}
	
	public static ClientResponse PUT(String URL,String jsonData , String token) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).header("token", token).put(ClientResponse.class,jsonData);
	}
	
	public static ClientResponse DELETE(String URL,String jsonData , String token) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).header("token", token).delete(ClientResponse.class,jsonData);
	}
	
	/************************* Over loaded methods for sending request without token **************/
	
	public static ClientResponse GET(String URL) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
	}
	
	public static ClientResponse POST(String URL,String jsonData) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class,jsonData);
	}
	
	public static ClientResponse PUT(String URL,String jsonData) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).put(ClientResponse.class,jsonData);
	}
	
	public static ClientResponse DELETE(String URL,String jsonData ) {
		Client client = Client.create();
        WebResource webResource = client.resource(URL);
       return webResource.type(MediaType.APPLICATION_JSON).delete(ClientResponse.class,jsonData);
	}
	
}

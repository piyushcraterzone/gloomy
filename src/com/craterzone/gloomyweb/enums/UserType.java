package com.craterzone.gloomyweb.enums;

public enum UserType {
	
	GENERAL(0),
	SOCIAL(1);
	
	private int value;

	private UserType(int value) {
		this.value = value;
	}
	public int getLabel(){
		return this.value;
	}
	
}

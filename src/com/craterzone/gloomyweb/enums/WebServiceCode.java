package com.craterzone.gloomyweb.enums;

public enum WebServiceCode {

	NOT_FOUND(404),SUCCESS(200),NO_CONTENT(204),BAD_REQUEST(400),INVALID_SESSION(401),CONFLICT(409),INTERNEL_SERVER(500);
	
	private int code;
	private WebServiceCode(int code) {
		this.code = code;
	}
	public int getCode() {
		return code;
	}
	
	public static int getCode(int code) {
		if(code == SUCCESS.getCode()) {
			return code;
		} else if(code == NO_CONTENT.getCode()) {
			return code;
		} else if(code == BAD_REQUEST.getCode()) {
			return code;
		} else if(code == INVALID_SESSION.getCode()) {
			return code;
		} else if(code == CONFLICT.getCode()) {
			return code;
		} else if(code == INTERNEL_SERVER.getCode()) {
			return code;
		}
		return NOT_FOUND.getCode();
	}
	
}

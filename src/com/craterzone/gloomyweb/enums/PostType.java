package com.craterzone.gloomyweb.enums;

public enum PostType {

	PUBLIC(1),
	PRIVATE(2);
	private int type;
	
	private PostType(int type) {
		this.type=type;
	}
	public int getType() {
		return type;
	}
	
	
}

package com.craterzone.gloomyweb.enums;



public enum UserStatus {

	PUBLIC(1),
	PRIVATE(2);
	
	private int status;
	private UserStatus(int status) {
		this.status = status;
	}
	public int getStatus() {
		return status;
	}
	
	public static int getStatus(UserStatus status) {
		if(status == PRIVATE) {
			return PRIVATE.getStatus();
		}
		return PUBLIC.getStatus();
	}
}

package com.craterzone.gloomyweb.enums;

/**
 * 
 * @author Piyush 
 *
 * This Enum used for getting Support fetaboo multiple languages
 * Default language is English
 */
public enum Language {

	ENGLISH(1,"en"),SPENISH(2,"sp");
	
	private int type;
	private String keyword;
	private Language(int type,String keyword) {
		this.type = type;
		this.keyword = keyword;
	}
	
	public int getType() {
		return type;
	}
	public String getKeyword() {
		return keyword;
	}
	
	public static String getKeyWord(int type) {
		if(type == SPENISH.getType()) {
			return SPENISH.getKeyword();
		}
		return ENGLISH.getKeyword();
	}
	public static int getType(String keyword) {
		if(keyword == null || keyword.length()<=0) {
			return ENGLISH.getType();
		}
		if(SPENISH.getKeyword().equalsIgnoreCase(keyword)) {
			return SPENISH.getType();
		}
		return ENGLISH.getType();
	}
}

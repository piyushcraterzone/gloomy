package com.craterzone.gloomyweb.enums.constants.post;


/**
 * This Is Enum for Type of Post
 * @author Piyush Gupta
 *
 */

public enum CategoryType {

	GLOOMY(1),FETISH(2),TABOO(3);
	
	private int type;
	
	private CategoryType(int type) {
		this.type=type;
	}
	public int getType() {
		return type;
	}

	/**
	 * This Function for getting the Type of Post
	 * By Default post will be Fetaboo Type
	 * @param key
	 * @return
	 */
	public static CategoryType getType(int key) {
		 if(key==FETISH.getType() ){
			 return FETISH;
		}else if( key==TABOO.getType()){
			return TABOO;
		}
		return GLOOMY;
	}
	
	public static int getType(String label) {
		 if(label.equals(FETISH.toString())) {
			 return FETISH.getType();
		} else if( label.equals(TABOO.toString())) {
			return TABOO.getType();
		}
		return GLOOMY.getType();
	}
	
}

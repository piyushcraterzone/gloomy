package com.craterzone.gloomyweb.enums.constants.user;


public enum Gender {
	
	FEMALE(1),
	MALE(2);
	
	private int label;
	
	private Gender(int label) {
		this.label = label;
	}
	
	public int getLabel() {
		return this.label;
	}
	
	public Gender getGender() {
		return FEMALE;
	}
	public static Gender getGender(int gender) {
		if(gender == MALE.getLabel()) {
			return MALE;
		}
		return FEMALE;
	}
}

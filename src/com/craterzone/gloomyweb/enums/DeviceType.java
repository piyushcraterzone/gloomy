package com.craterzone.gloomyweb.enums;

public enum DeviceType {

	IOS(1), ANDROID(2), WINDOWS(3), BLACKBERRY(4);

	private int value;

	private DeviceType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int deviceType) {
		if (deviceType == IOS.getValue() || deviceType == ANDROID.getValue()
				|| deviceType == WINDOWS.getValue()
				|| deviceType == BLACKBERRY.getValue()) {
			return true;
		}
		return false;
	}
}
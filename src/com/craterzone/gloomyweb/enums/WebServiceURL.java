package com.craterzone.gloomyweb.enums;


public enum WebServiceURL {

	USER("user/"),
	POST("post/");
	
	private static String VERSION = "v1/";
	//private static String DEV_SERVER_URL = "http://192.168.1.2:8080/gloomyapi/";
	private static String DEV_SERVER_URL = "http://178.62.182.120:8080/gloomyapi/";
	
	private static String PRIVATE_POST = "private";
	private static String PUBLIC_POST =  "public";
	private static String STORY = "story";
	private static String SEARCH_BY_CATEGORY = "searchcategory";
	private static String LOGIN = "login";
	private static String SOCIAL_LOGIN = "sociallogin";
	private static String SIGNUP = "signup";
	private static String COMMENT = "comment";
	private static String POST_LIKE = "like";
	private static String POST_SHARE = "share";
	private static String REPORT_USER = "report";
	private static String BLOCK_USER = "block";
	private static String COUNT_CATEGORY = "categorycount";
	private static String POST_VIEW = "view";
	private static String POST_HIDE = "hide";
	private static String UPDATE_SETTING = "updatesetting";
	private static String SUBSCRIBE = "subscribe";
	public static String MOST_LIKE = "mostlike";
	public static String MOST_SHARE = "mostshare";
	public static String MOST_VIEW = "mostview";
	public static String MOST_COMMENT ="mostcomment";
	public static String POPULAR_TAG = "populartag";
	public static String SEARCH_BY_TAG = "tag";
	public static String USER_PUBLIC_POST = "public";
	public static String USER_PRIVATE_POST = "private";
	public static String UPDATE_POST ="update";
	public static String DELETE_POST = "deletepost";
	
	public static String PATH_SEPERATOR = "/";
	
	private String URL;
	private WebServiceURL(String URL) {
		this.URL=URL;
	}
	public String getURL() {
		return URL;
	}
	public static String getURL(WebServiceURL URL) {
		return DEV_SERVER_URL + URL.getURL() +VERSION ;
	}
	
	/*---------------------- URLS for POST SErvices-------------------------------------------- */
	
	/** This Is for Adding Private Post 
	 *  */
	public static String privatePostURL(WebServiceURL URL) {
		return getURL(URL) + PRIVATE_POST;
	}
	/** This Is for Adding Private Post 
	 *  */
	public static String publicPostURL(WebServiceURL URL) {
		return getURL(URL) + PUBLIC_POST;
	}
	public static String privatePostGetURL(WebServiceURL URL , String postId) {
		return getURL(URL) + PRIVATE_POST +"?token=" + postId;
	}
	/**
	 * This Url for getting public post 
	 * @param URL
	 * @param count
	 * @return
	 */
	public static String storyGetURL(WebServiceURL URL,String rowIdToken) {
		return getURL(URL) + STORY + "?token="+rowIdToken;
	}
	public static String getPublicPost(WebServiceURL URL,long count  ,String userId) {
		return getURL(URL) +  PUBLIC_POST +"?lastTimeStamp=" + count + "&&userId="+userId;
	}
	public static String publicPostByCategoryGetURL(WebServiceURL URL,int categoryType,long count , String userId) {
		return getURL(URL) + SEARCH_BY_CATEGORY+"?category="+categoryType+"&&lastTimeStamp=" + count + "&&userId="+userId;
	}
	public static String commentAdd(WebServiceURL URL) {
		return getURL(URL) + COMMENT;
	}
	public static String likeAdd(WebServiceURL URL) {
	return getURL(URL) + POST_LIKE;
	}
	public static String shareAdd(WebServiceURL URL) {
		return getURL(URL) + POST_SHARE;
	}
	public static String categoryCount(WebServiceURL URL) {
		return getURL(URL) + COUNT_CATEGORY;
	}
	public static String getCommentOnPost(WebServiceURL URL,String postId,long count) {
		return getURL(URL) + COMMENT + "?postId="+postId +"&&lastTimeStamp="+count;
	}
	public static String viewAdd(WebServiceURL URL) {
		return getURL(URL) + POST_VIEW;
	}
	public static String hidePost(WebServiceURL URL) {
		return getURL(URL) + POST_HIDE;
	}
	public static String mostPosts(WebServiceURL URL , String mostType,int count ) {
		return getURL(URL)   + mostType + "?lastTimeStamp=" + count;
	}
	public static String popularTag(WebServiceURL URL) {
		return getURL(URL) + POPULAR_TAG;
	}
	public static String postByTag(WebServiceURL URL,String tagName , int count , String userId) {
		return getURL(URL) + SEARCH_BY_TAG +"/" + tagName + "?lastTimeStamp="+count + "&&userid="+userId;
	}
	public static String deleteComment(WebServiceURL URL) {
		return getURL(URL) + COMMENT;
	}
	public static String updatePost(WebServiceURL URL) {
		return getURL(URL) + UPDATE_POST ;
	}
	public static String deletePost(WebServiceURL URL ) {
		return getURL(URL) + DELETE_POST;
	}
	
	//kamal
	public static String loginURL(WebServiceURL URL) {
		return getURL(URL) + LOGIN;
	}
	
	public static String signURL(WebServiceURL URL) {
		return getURL(URL) + SIGNUP;
	}
	public static String reportUser(WebServiceURL URL) {
		return getURL(URL) + REPORT_USER;
	}
	public static String blockUser(WebServiceURL URL) {
		return getURL(URL) + BLOCK_USER;
	}
	public static String loginSocial(WebServiceURL URL) {
		return getURL(URL) + SOCIAL_LOGIN;
	}
	public static String updateSettings(WebServiceURL URL) {
		return getURL(URL) + UPDATE_SETTING;
	}
	public static String subscribe(WebServiceURL URL,String emailId) {
		return getURL(URL) + SUBSCRIBE +"?emailId="+emailId;
	}
	public static String userPublicPost(WebServiceURL URL , String userId , long count) {
		return getURL(URL) + USER_PUBLIC_POST + "?userId=" + userId + "&&lastTimeStamp=" + count;
	}
	public static String userPrivatePost(WebServiceURL URL , String userId , long count) {
		return getURL(URL) + USER_PRIVATE_POST + "?userId=" + userId + "&&lastTimeStamp=" + count;
	}
	
}
